package com.iscagroup.schools.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Raed Saeed on 7/15/2019.
 */
public class DateTimeUtil {
    public static String getTimeString(String inDate) {
        String dateString = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
            Date date = simpleDateFormat.parse(inDate);

            SimpleDateFormat finalDateFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
            dateString = finalDateFormat.format(date);
            return dateString;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public static String getDateTimeString(String inDate) {
        String dateString = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.getDefault());
            Date date = simpleDateFormat.parse(inDate);
            SimpleDateFormat finalDateFormat = new SimpleDateFormat("yyyy-MM-dd h:m aa", Locale.getDefault());
            dateString = finalDateFormat.format(date);
            return dateString;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }
}
