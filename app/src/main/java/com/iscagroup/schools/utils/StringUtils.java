package com.iscagroup.schools.utils;

import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import java.util.List;

/**
 * Created by Raed Saeed on 2/20/2019.
 */

public class StringUtils {
    public static String separateString(List<String> names) {
        StringBuilder builder = new StringBuilder();
        String comma = "";
        for (String name : names) {
            builder.append(comma).append(name);
            comma = ",";
        }
        return builder.toString();
    }

    public static SpannableString getSpannableString(String message, int color) {
        SpannableString firstPart = new SpannableString(message);
        firstPart.setSpan(new ForegroundColorSpan(color), 0, message.length(), 0);
        return firstPart;
    }
}
