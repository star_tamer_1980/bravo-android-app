package com.iscagroup.schools.utils;

import com.iscagroup.schools.global.home.ParentHomeActivity;
import com.iscagroup.schools.request.parent.OpenParentRequestFragment;

/**
 * Created by Raed Saeed on 6/26/2019.
 */
public class StringConstants {
    /**
     * The key to {@link com.iscagroup.schools.data.login.LoginResponse}
     * from {@link com.iscagroup.schools.global.login.LoginActivity} to {@link com.iscagroup.schools.global.verification.CodeVerificationActivity}
     */
    public static final String EXTRA_LOGIN_RESPONSE = "com.iscagroup.schools.EXTRA_LOGIN_RESPONSE";


    /**
     * The key to PhoneNumber with plus sign String from {@link com.iscagroup.schools.global.login.LoginActivity}
     * To {@link com.iscagroup.schools.global.verification.CodeVerificationActivity}
     */
    public static final String EXTRA_PHONE_NUMBER = "com.iscagroup.schools.EXTRA_PHONE_NUMBER";


    /**
     * The Key to Selected Country Code Number form from {@link com.iscagroup.schools.global.login.LoginActivity}
     * To {@link com.iscagroup.schools.global.verification.CodeVerificationActivity}
     */
    public static final String EXTRA_CODE_NUMBER = "com.iscagroupo.shools.EXTRA_CODE_NUMBER";


    /**
     * The key to User phone number from {@link com.iscagroup.schools.global.login.LoginActivity}
     * To {@link com.iscagroup.schools.global.verification.CodeVerificationActivity}
     */
    public static final String EXTRA_USER_PHONE_NUMBER = "com.iscagrup.schools.EXTRA_USER_PHONE_NUMBER";

    /**
     * The key to Login data from {@link com.iscagroup.schools.global.workspace.WorkSpaceActivity}
     * To {@link com.iscagroup.schools.global.main.MainActivity}
     */
    public static final String EXTRA_LOGIN_DATA = "com.iscagroup.schools.EXTRA_LOGIN_DATA";

    /**
     * The key to Home data from {@link com.iscagroup.schools.global.main.MainActivity}
     * To {@link ParentHomeActivity}
     */
    public static final String EXTRA_STUDENT_HOME = "com.iscagroup.schools.EXTRA_STUDENT_HOME";

    /**
     * The key to Home data from {@link com.iscagroup.schools.global.main.MainActivity}
     * to {@link com.iscagroup.schools.global.home.TeacherHomeActivity}
     */
    public static final String EXTRA_SCHOOL_HOME = "com.iscagroup.schools.EXTRA_SCHOOL_HOME";
    /**
     * The key of photo list objects that should be showing in ImagePagerAdapter from {@link com.iscagroup.schools.adapter.FileAdapter}
     * to {@link com.iscagroup.schools.global.ImageFullScreenActivity}
     */
    public static final String EXTRA_PHOTO_LIST = "com.iscagroup.schools.EXTRA_PHOTO_LIST";
    /**
     * The key of photo position in photo list that the {@link com.iscagroup.schools.adapter.ImagePagerAdapter} show open at
     * from {@link com.iscagroup.schools.adapter.FileAdapter} to {@link com.iscagroup.schools.global.ImageFullScreenActivity}
     */
    public static final String EXTRA_PAGER_POSITION = "com.iscagroup.school.EXTRA_PAGER_POSITION";
    /**
     * The key of {@link com.iscagroup.schools.data.request.Request}  to send for both {@link com.iscagroup.schools.request.reply.ReplyRequestActivity}
     * and {@link com.iscagroup.schools.request.RequestDetailActivity}
     */
    public static final String EXTRA_REQUEST = "com.iscagroup.schools.EXTRA_REQUEST";
    public static final String EXTRA_NOTE = "com.iscagroup.schools.EXTRA_NOTE";
    /**
     * The key of returned data (Request) from {@link com.iscagroup.schools.request.parent.add.AddParentRequestActivity}
     * to {@link OpenParentRequestFragment}
     */
    public static final String EXTRA_SENDED_REQUEST = "com.iscagroup.schools.EXTRA_SENDED_REQUEST";
    /**
     * The key of request position inside the adapter so the adapter can be notified at this position
     * the position used in {@link com.iscagroup.schools.request.teacher.TeacherOpenRequestFragment} and
     * {@link com.iscagroup.schools.request.school.SchoolOpenRequestFragment} to notify the adapter from
     * onActivityResult call
     */
    public static final String EXTRA_REQUEST_POSITION = "com.iscagroup.schools.EXTRA_REQUEST_POSITION";
    /**
     * The of request type e.g (The replier is Teacher or School manager)
     * if type = 1, the type is Teacher
     * if type = 2, the type is School Manager
     */
    public static final String EXTRA_REQUEST_TYPE = "com.iscagroup.schools.EXTRA_REQUEST_TYPE";
    public static final String EXTRA_CREATED_NOTE = "com.iscagroup.schools.EXTRA_CREATE_NOTE";
    public static final String EXTRA_PLAN = "com.iscagroup.schools.EXTRA_PLAN";
    public static final String EXTRA_DAY = "com.iscagroup.schools.EXTRA_DAY";
    public static final String EXTRA_DRIVER = "com.iscagroup.schools.EXTRA_DRIVER";
}
