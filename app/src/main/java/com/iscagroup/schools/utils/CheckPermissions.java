package com.iscagroup.schools.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

/**
 * Created by Raed Saeed on 6/19/2019.
 */

public class CheckPermissions {
    private static final String TAG = "CheckPermissions";

    public static boolean checkStoragePermissions(AppCompatActivity activityCompat) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activityCompat.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.e("Permission error", "You have permission");
                return true;
            } else {

                Log.e("Permission error", "You have asked for permission");
                ActivityCompat.requestPermissions(activityCompat, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //you dont need to worry about these stuff below api level 23
            Log.e("Permission error", "You already have the permission");
            return true;
        }
    }

    public static boolean checkReadStoragePermissions(AppCompatActivity appCompatActivity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (appCompatActivity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.w(TAG, "checkReadStoragePermissions: permission granted ");
                return true;
            } else {
                Log.w(TAG, "checkReadStoragePermissions: you have asked for permission ");
                ActivityCompat.requestPermissions(appCompatActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            Log.w(TAG, "checkReadStoragePermissions: you already have the permission ");
            return true;
        }
    }

    public static boolean checkReadStoragePermissions(Context appCompatActivity, Fragment fragment) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (appCompatActivity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.w(TAG, "checkReadStoragePermissions: permission granted ");
                return true;
            } else {
                Log.w(TAG, "checkReadStoragePermissions: you have asked for permission ");
                fragment.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            Log.w(TAG, "checkReadStoragePermissions: you already have the permission ");
            return true;
        }
    }

    public static boolean checkStoragePermissions(Context appCompatActivity, Fragment fragment) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (appCompatActivity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.w(TAG, "checkReadStoragePermissions: permission granted ");
                return true;
            } else {
                Log.w(TAG, "checkReadStoragePermissions: you have asked for permission ");
                fragment.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            Log.w(TAG, "checkReadStoragePermissions: you already have the permission ");
            return true;
        }
    }
}
