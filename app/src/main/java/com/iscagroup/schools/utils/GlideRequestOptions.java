package com.iscagroup.schools.utils;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by Raed Saeed on 7/10/2019.
 */
@SuppressWarnings("unused")
public class GlideRequestOptions {

    /**
     * @return {@link RequestOptions} to make image drawable as circular Image
     */
    public static RequestOptions getRoundedRequestOptions() {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CircleCrop(), new RoundedCorners(16));
        return requestOptions;
    }
}
