package com.iscagroup.schools.utils;


import android.content.Context;
import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.ArrayList;
import java.util.List;


public class Validation {
    private static final String TAG = "Validation";

    public static boolean validateNumberWithCode(Context context, List<String> codeList, List<String> numberLis) {
        for (int i = 0; i < codeList.size(); i++) {
            if (!validateNumberWithCode(context, codeList.get(i), numberLis.get(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean validateNumberWithCode(Context context, String code, String number) {
        try {

            if (code.equalsIgnoreCase("EG")) {
                return validateEgyptPhone(number);
            }

            if (number.startsWith("+")) {
                number = number.replace("+", "");
            }

            PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(number, code);

            Phonenumber.PhoneNumber nationalPhone = phoneNumberUtil.getExampleNumber(code);
            Log.w(TAG, "validateNumberWithCode: example code" + nationalPhone.toString());
            Log.w(TAG, "validateNumberWithCode: " + phoneNumber.toString());
            return phoneNumberUtil.isValidNumber(phoneNumber);
        } catch (NumberParseException e) {
            e.printStackTrace();
            Log.w(TAG, "validateNumberWithCode: " + e.toString());
            return false;
        }
    }

    private static boolean validateEgyptPhone(String number) {
        String countryCode = "+20";
        number = number.replace("\\s", "");
        if (number.startsWith(countryCode)) {
            String regexStringCountry = "(\\+2010|\\+2011|\\+2012|\\+2015)\\s?\\d{2}\\s?\\d{6}";
            return number.matches(regexStringCountry);
        } else {
            number = number.replaceFirst("^0+(?!$)", "");
            String regexStr = "(10|11|12|15)\\s?\\d{2}\\s?\\d{6}";
            return number.matches(regexStr);
        }
    }

    public static String filterNumber(String number) {
        return number.replaceFirst("^0+(?!$)", "");
    }

    public static List<String> fullNumberWithCountryCode(Context context, List<String> regionCode, List<String> numbers) {
        List<String> phoneList = new ArrayList<>();
        for (int i = 0; i < regionCode.size(); i++) {
            String phone = fullNumberWithCountryCode(context, regionCode.get(i), numbers.get(i));
            if (phone.length() > 0) {
                phoneList.add(phone);
            }
        }
        return phoneList;
    }

    public static String fullNumberWithCountryCode(Context context, String regionCode, String number) {
        if (validateNumberWithCode(context, regionCode, number)) {
            PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
            int countryCode = phoneNumberUtil.getCountryCodeForRegion(regionCode);
            if (regionCode.equalsIgnoreCase("EG")) {
                number = number.replaceFirst("^0+(?!$)", "");
            }

            if (!number.startsWith("+" + countryCode)) {
                String code = "+" + countryCode;
                return code + number;
            } else {
                return number;
            }
        }

        return "";
    }

    public static String separateNumber(List<String> numberList) {
        StringBuilder builder = new StringBuilder();
        String comma = "";
        for (String number : numberList) {
            builder.append(comma).append(number);
            comma = ",";
        }
        return builder.toString();
    }
}
