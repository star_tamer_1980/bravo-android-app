package com.iscagroup.schools.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Raed Saeed on 6/27/2019.
 */
public class UserSharedPreferences {

    private static final String PREF = "schools_pref";

    private static final String USER_NATIONAL_PHONE_NUMBER = "com.iscagroup.schools.USER_NATIONAL_PHONE_NUMBER";

    private static final String USER_COUNTRY_CODE = "com.iscagroups.schools.USER_COUNTRY_CODE";

    private static final String USER_LOGIN_STATUS = "com.iscagroups.schools.USER_LOGIN_STATUS";

    private static final String USER_NAME = "com.iscagroup.schools.USER_NAME";

    private static final String USER_PHOTO = "com.iscagroup.schools.USER_PHOTO";

    private static final String USER_CLASS_NAME = "com.iscagroup.schools.USER_CLASS_NAME";

    private static final String USER_CLASS_ID = "com.iscagroup.schools.USER_CLASS_ID";

    private static final String USER_ID = "com.iscagroup.schools.USER_ID";

    private static final String USER_STUDENT_ID = "com.iscagroup.schools.USER_STUDENT_ID";

    private static final String USER_SCHOOL_ID = "com.iscagroup.schools.USER_SCHOOL_ID";

    private static final String USER_SCHOOL_LOGO = "com.iscagroup.schools.USER_SCHOOL_LOGO";

    private static final String SCHOOL_NAME = "com.iscagroup.schools.SCHOOL_NAME";

    /**
     * One Signal Player Id or Devices token that is used to send notification throw
     */
    private static final String USER_PLAYER_ID = "com.iscagroup.schools.USER_PLAYER_ID";


    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
    }

    public static void setUserNationalPhoneNumber(Context context, String number) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_NATIONAL_PHONE_NUMBER, number);
        editor.apply();
    }


    public static String getUserNationalPhoneNumber(Context context) {
        return getSharedPreferences(context).getString(USER_NATIONAL_PHONE_NUMBER, "");
    }

    public static void setUserCountryCode(Context context, String countryCode) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_COUNTRY_CODE, countryCode);
        editor.apply();
    }

    public static String getUserCountryCode(Context context) {
        return getSharedPreferences(context).getString(USER_COUNTRY_CODE, "");
    }

    public static void setUserLoginStatus(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(USER_LOGIN_STATUS, status);
        editor.apply();
    }

    public static boolean getUserLoginStatus(Context context) {
        return getSharedPreferences(context).getBoolean(USER_LOGIN_STATUS, false);
    }

    public static void setUserName(Context context, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_NAME, userName);
        editor.apply();
    }

    public static String getUserName(Context context) {
        return getSharedPreferences(context).getString(USER_NAME, "");
    }

    public static void setUserPhoto(Context context, String userPhoto) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_PHOTO, userPhoto);
        editor.apply();
    }

    public static String getUserPhoto(Context context) {
        return getSharedPreferences(context).getString(USER_PHOTO, "");
    }

    public static void setUserClassName(Context context, String userClass) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_CLASS_NAME, userClass);
        editor.apply();
    }

    public static String getUserClassName(Context context) {
        return getSharedPreferences(context).getString(USER_CLASS_NAME, "");
    }

    public static void setUserClassId(Context context, String classId) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_CLASS_ID, classId);
        editor.apply();
    }

    public static String getUserClassId(Context context) {
        return getSharedPreferences(context).getString(USER_CLASS_ID, "");
    }

    /**
     * this for teacher, management and parent
     *
     * @param context to get sharedPreferences
     * @param userId  the id of user
     */
    public static void setUserId(Context context, String userId) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_ID, userId);
        editor.apply();
    }

    public static String getUserId(Context context) {
        return getSharedPreferences(context).getString(USER_ID, "0");
    }

    public static void setUserStudentId(Context context, String studentId) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_STUDENT_ID, studentId);
        editor.apply();
    }

    public static String getUserStudentId(Context context) {
        return getSharedPreferences(context).getString(USER_STUDENT_ID, "0");
    }

    public static void setUserSchoolId(Context context, String schoolId) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_SCHOOL_ID, schoolId);
        editor.apply();
    }

    public static String getUserSchoolId(Context context) {
        return getSharedPreferences(context).getString(USER_SCHOOL_ID, "0");
    }

    public static void setUserSchoolLogo(Context context, String schoolLogo) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_SCHOOL_LOGO, schoolLogo);
        editor.apply();
    }

    public static String getUserSchoolLogo(Context context) {
        return getSharedPreferences(context).getString(USER_SCHOOL_LOGO, "");
    }

    public static void setSchoolName(Context context, String schoolName) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(SCHOOL_NAME, schoolName);
        editor.apply();
    }

    public static String getSchoolName(Context context) {
        return getSharedPreferences(context).getString(SCHOOL_NAME, "");
    }

    public static void setUserPlayerId(Context context, String playerId) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_PLAYER_ID, playerId);
        editor.apply();
    }

    public static String getUserPlayerId(Context context) {
        return getSharedPreferences(context).getString(USER_PLAYER_ID, "");
    }

    public static void clear(Context context) {
        getSharedPreferences(context).edit()
                .remove(USER_LOGIN_STATUS)
                .remove(USER_SCHOOL_LOGO)
                .remove(USER_SCHOOL_ID)
                .remove(USER_STUDENT_ID)
                .remove(USER_ID)
                .remove(USER_CLASS_ID)
                .apply();
    }
}
