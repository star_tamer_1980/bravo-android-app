package com.iscagroup.schools.dailyplan;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.Day;
import com.iscagroup.schools.data.plan.Plan;
import com.iscagroup.schools.dialog.BottomFilePickerDialog;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.utils.StringUtils;
import com.iscagroup.schools.utils.UserSharedPreferences;

import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

/**
 * Created by Raed Saeed on 7/31/2019.
 */
public class AddDailyPlanActivity extends BaseActivity {

    private static final String featureType = "4";
    private Plan plan;
    private EditText etPlanText;
    private PlanViewModel planViewModel;
    private Day day;
    private String teacherId;
    private boolean isEditable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_daily_plan);

        Toolbar toolbar = findViewById(R.id.tb_activity_add_daily_plan_toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        if (intent != null) {
            plan = intent.getParcelableExtra(StringConstants.EXTRA_PLAN);
            day = intent.getParcelableExtra(StringConstants.EXTRA_DAY);
            isEditable = plan.getPlan().length() > 0;
            setToolBarViews(toolbar);
        }

        initViews();
        setPlanView();
    }

    private void setToolBarViews(Toolbar toolbar) {
        TextView tvPrimaryToolbarName = toolbar.findViewById(R.id.tv_layout_secondary_toolbar_primary_name);
        tvPrimaryToolbarName.setText(plan.getSubjectName());

        TextView tvSecondaryToolbarName = toolbar.findViewById(R.id.tv_layout_secondary_toolbar_name);
        tvSecondaryToolbarName.setText(plan.getClassName());
    }

    private void initViews() {
        Button btnSavePlan = findViewById(R.id.btn_activity_add_daily_plan_send);
        btnSavePlan.setOnClickListener(v -> {
            if (isEditable) {
                updatePlan();
            } else {
                addPlan();
            }
        });

        etPlanText = findViewById(R.id.et_activity_add_plan_description);
        etPlanText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                btnSavePlan.setEnabled((s.toString().trim().length() > 0));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ImageView ivAttachFile = findViewById(R.id.iv_activity_add_daily_plan_attach_file);
        ivAttachFile.setOnClickListener(v -> {
            BottomFilePickerDialog filePickerDialog = new BottomFilePickerDialog();
            filePickerDialog.setActivity(true);
            filePickerDialog.setClickListener(view -> {
                switch (view.getId()) {
                    case R.id.iv_dialog_file_picker_pick_image:
                        startImagePicker(5);
                        break;
                    case R.id.iv_dialog_file_picker_pick_document:
                        startDocumentPicker();
                        break;
                }
            });
            filePickerDialog.show(getSupportFragmentManager(), null);
        });

        RecyclerView rvFileList = findViewById(R.id.rv_activity_add_plan_file_list);
        rvFileList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvFileList.setAdapter(getFileAdapter());

        planViewModel = ViewModelProviders.of(this).get(PlanViewModel.class);
        teacherId = UserSharedPreferences.getUserId(this);
    }

    private void setPlanView() {
        if (plan != null) {
            etPlanText.setText(plan.getPlan());
            getFileAdapter().loadAttach(filterModifyFiles(plan.getAttach()));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_menu_delete) {
            compositeDisposable.add(planViewModel.deletePlan(plan.getPlanId())
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .doFinally(() -> {
                        finish();
                        hideProgressDialog();
                    })
                    .doOnSubscribe(disposable -> showProgressDialog(R.string.deleting_your_plan))
                    .subscribe(response -> {
                    }, error -> {
                        hideProgressDialog();
                        showFailureMessage(error.getMessage());
                    }));
        }
        return super.onOptionsItemSelected(item);
    }

    private void addPlan() {
        compositeDisposable.add(planViewModel.addPlan(teacherId, plan.getScheduleId(),
                etPlanText.getText().toString(), day.getNormalDate())
                .subscribeOn(io())
                .observeOn(mainThread())
                .doOnSubscribe(subscription -> showProgressDialog(R.string.updating_your_plan))
                .subscribe(this::sendFiles, error -> {
                    hideProgressBar();
                    showFailureMessage(error.getMessage());
                }));
    }

    private void updatePlan() {
        compositeDisposable.add(planViewModel.updatePlan(plan.getPlanId(), etPlanText.getText().toString())
                .subscribeOn(io())
                .observeOn(mainThread())
                .doOnSubscribe(subscription -> showProgressDialog(R.string.updating_your_plan))
                .subscribe(this::sendFiles, error -> {
                    hideProgressBar();
                    showFailureMessage(error.getMessage());
                }));
    }

    private void sendFiles(JsonObject jsonObject) {
        getAttachmentPresenter().removeFiles(StringUtils.separateString(getFileAdapter().getDeletedFiles()));
        JsonElement jsonElement = jsonObject.get("id");
        getAttachmentPresenter().attachFiles(featureType, String.valueOf(jsonElement.getAsInt()),
                UserSharedPreferences.getUserSchoolId(this),
                getFileAdapter().getList());
    }

    @Override
    public void onUploadDone() {
        hideProgressDialog();
        finish();
    }

    @Override
    public void onUploadFailed() {
        showToast(R.string.connection_interruption);
    }
}
