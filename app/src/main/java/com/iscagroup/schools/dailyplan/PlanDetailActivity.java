package com.iscagroup.schools.dailyplan;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.plan.Plan;
import com.iscagroup.schools.utils.StringConstants;

public class PlanDetailActivity extends BaseActivity {

    private Plan plan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_detail);
        Toolbar toolbar = findViewById(R.id.tb_activity_plan_detail_toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        if (intent != null) {
            plan = intent.getParcelableExtra(StringConstants.EXTRA_PLAN);
            setToolBarViews(toolbar);
        }

        initViews();
    }

    private void setToolBarViews(Toolbar toolbar) {
        TextView tvPrimaryToolbarName = toolbar.findViewById(R.id.tv_layout_secondary_toolbar_primary_name);
        tvPrimaryToolbarName.setText(plan.getSubjectName());

        TextView tvSecondaryToolbarName = toolbar.findViewById(R.id.tv_layout_secondary_toolbar_name);
        tvSecondaryToolbarName.setText(plan.getClassName());
    }

    private void initViews() {
        RecyclerView rvFileList = findViewById(R.id.rv_activity_plan_detail_file_list);
        rvFileList.setLayoutManager(new LinearLayoutManager(this));
        rvFileList.setAdapter(getFileAdapter());

        TextView tvPlanDescription = findViewById(R.id.tv_activity_plan_detail_description);

        if (plan != null) {
            tvPlanDescription.setText(plan.getPlan());
            getFileAdapter().loadAttach(filterDownFiles(plan.getAttach()));
        }
    }
}
