package com.iscagroup.schools.dailyplan;

import com.iscagroup.schools.data.Day;
import com.iscagroup.schools.utils.UserSharedPreferences;

/**
 * Created by Raed Saeed on 8/2/2019.
 */
public class SchoolPlanActivity extends DailyPlanActivity {
    @Override
    protected void fetchDayData(Day day) {
        String schoolId = UserSharedPreferences.getUserSchoolId(this);
        getSchoolDailyPlans(schoolId, day.getNormalDate());
    }
}
