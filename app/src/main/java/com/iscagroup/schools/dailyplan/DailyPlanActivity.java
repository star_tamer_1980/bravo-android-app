package com.iscagroup.schools.dailyplan;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.iscagroup.schools.R;
import com.iscagroup.schools.adapter.DayAdapter;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.Day;
import com.iscagroup.schools.data.plan.Plan;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.widget.CenterRecyclerView;

import java.util.List;

/**
 * Created by Raed Saeed on 7/30/2019.
 */
public abstract class DailyPlanActivity extends BaseActivity implements DailyPlanContract.DailyPlanView, DailyPlanAdapter.OnPlanClickListener {

    private static final String TAG = "DailyPlanActivity";
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    private DailyPlanAdapter planAdapter;
    private DailyPlanPresenterImpl dailyPlanPresenter;
    private DayAdapter dayAdapter;
    private boolean hasData = false;
    private int lastPostion;
    private CenterRecyclerView recyclerView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_plan);

        Toolbar toolbar = findViewById(R.id.activity_daily_plan_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(R.string.add_daily_plan);
        }

        recyclerView = findViewById(R.id.rv_activity_daily_plan_days);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        dayAdapter = new DayAdapter();
        recyclerView.setAdapter(dayAdapter);

        lastPostion = dayAdapter.getTodayIndex();

        recyclerView.smoothScrollToPosition(dayAdapter.getTodayIndex());
        dayAdapter.setOnDayClickListener((position, day) -> {
            lastPostion = position;
            fetchDayData(day);
        });

        initViews();
    }

    private void initViews() {
        swipeRefreshLayout = findViewById(R.id.srl_activity_daily_plan_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(() -> fetchDayData(dayAdapter.getSelectedDay()));

        progressBar = findViewById(R.id.pb_activity_daily_plan_loading);

        RecyclerView rvPlanList = findViewById(R.id.rv_activity_daily_plan_list);
        rvPlanList.setLayoutManager(new LinearLayoutManager(this));
        planAdapter = new DailyPlanAdapter();
        planAdapter.setOnPlanClickListener(this);
        rvPlanList.setAdapter(planAdapter);

        dailyPlanPresenter = new DailyPlanPresenterImpl();
        dailyPlanPresenter.attachLifecycle(getLifecycle());
        dailyPlanPresenter.attachView(this);
    }

    protected abstract void fetchDayData(Day day);

    @Override
    protected void onResume() {
        super.onResume();
        fetchDayData(dayAdapter.getSelectedDay());
    }

    @Override
    public void onPlanClick(int position, Plan plan) {
        Intent intent = new Intent(this, AddDailyPlanActivity.class);
        intent.putExtra(StringConstants.EXTRA_PLAN, plan);
        intent.putExtra(StringConstants.EXTRA_DAY, dayAdapter.getSelectedDay());
        startActivity(intent);
    }

    @Override
    public void getTeacherDailyPlans(String schoolId, String teacherId, String date) {
        dailyPlanPresenter.getTeacherPlan(schoolId, teacherId, date);
    }

    @Override
    public void getParentDailyPlans(String schoolId, String classId, String date) {
        dailyPlanPresenter.getParentPlan(schoolId, classId, date);
    }

    @Override
    public void getSchoolDailyPlans(String schoolId, String date) {
        dailyPlanPresenter.getSchoolPlan(schoolId, date);
    }

    @Override
    public void showDailyPlans(List<Plan> plans) {
        recyclerView.smoothScrollToPosition(lastPostion);
        planAdapter.addPlans(plans);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showProgressBar() {
        if (!hasData) progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void startRefreshing() {
        if (hasData) swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void stopRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }
}
