package com.iscagroup.schools.dailyplan;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BasePresenter;
import com.iscagroup.schools.data.plan.PlanResponse;
import com.iscagroup.schools.model.local.AppDatabase;
import com.iscagroup.schools.model.local.DailyPlanDao;
import com.iscagroup.schools.model.remote.DailyPlanService;

import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

/**
 * Created by Raed Saeed on 7/31/2019.
 */
public class DailyPlanPresenterImpl extends BasePresenter<DailyPlanContract.DailyPlanView> implements DailyPlanContract.DailyPlanPresenter {

    private DailyPlanService dailyPlanService;
    private DailyPlanDao planDao;

    @Override
    protected void onCreate() {
        dailyPlanService = DailyPlanService.getInstance();
        planDao = AppDatabase.getInstance().getPlanDao();
    }

    @Override
    public void getTeacherPlan(String schoolId, String userId, String date) {
        if (getView().isConnected()) {
            getCompositeDisposable().add(dailyPlanService.getTeacherPlans(schoolId, userId, date)
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .doOnSubscribe(subscription -> {
                        getView().showProgressBar();
                        getView().startRefreshing();
                    })
                    .doFinally(() -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                    })
                    .flatMapIterable(PlanResponse::getPlan)
                    .map(plan -> {
                        if (plan.getPlan().length() > 0) {
                            plan.setResourceId(R.layout.item_daily_plan);
                        } else {
                            plan.setResourceId(R.layout.item_empty_daily_plan);
                        }

                        if (!(plan.getPlanDate().length() > 0)) {
                            plan.setPlanDate(date);
                        }
                        return plan;
                    })
                    .toList()
                    .subscribe(plans -> {
                        getView().showDailyPlans(plans);
                        getCompositeDisposable().add(planDao.saveDailyPlans(plans)
                                .subscribeOn(io())
                                .observeOn(mainThread())
                                .subscribe());
                    }, error -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                        error.printStackTrace();
                    }));
        } else {
            getCompositeDisposable().add(planDao.getTeacherPlans(userId, date)
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .doOnSubscribe(subscription -> {
                        getView().showProgressBar();
                        getView().startRefreshing();
                    }).doFinally(() -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                    }).subscribe(plans -> getView().showDailyPlans(plans), error -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                        error.printStackTrace();
                    }));
        }
    }

    @Override
    public void getParentPlan(String schoolId, String classId, String date) {
        if (getView().isConnected()) {
            getCompositeDisposable().add(dailyPlanService.getParentPlan(schoolId, classId, date)
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .doOnSubscribe(subscription -> {
                        getView().showProgressBar();
                        getView().startRefreshing();
                    })
                    .doFinally(() -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                    })
                    .flatMapIterable(PlanResponse::getPlan)
                    .map(plan -> {
                        if (plan.getPlan().length() > 0) {
                            plan.setResourceId(R.layout.item_plan_view);
                        } else {
                            plan.setResourceId(R.layout.item_empty_plan_view);
                        }

                        if (!(plan.getPlanDate().length() > 0)) {
                            plan.setPlanDate(date);
                        }
                        return plan;
                    })
                    .toList()
                    .subscribe(plans -> {
                        getView().showDailyPlans(plans);
                        getCompositeDisposable().add(planDao.saveDailyPlans(plans)
                                .subscribeOn(io())
                                .observeOn(mainThread())
                                .subscribe());
                    }, error -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                        error.printStackTrace();
                    }));
        } else {
            getCompositeDisposable().add(planDao.getParentPlans(classId, date)
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .doOnSubscribe(subscription -> {
                        getView().showProgressBar();
                        getView().startRefreshing();
                    }).doFinally(() -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                    })
                    .toFlowable()
                    .flatMapIterable(planList -> planList)
                    .toList()
                    .subscribe(plans -> getView().showDailyPlans(plans), error -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                        error.printStackTrace();
                    }));
        }
    }

    @Override
    public void getSchoolPlan(String schoolId, String date) {
        if (getView().isConnected()) {
            getCompositeDisposable().add(dailyPlanService.getSchoolPlan(schoolId, date)
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .doOnSubscribe(subscription -> {
                        getView().showProgressBar();
                        getView().startRefreshing();
                    })
                    .doFinally(() -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                    })
                    .flatMapIterable(PlanResponse::getPlan)
                    .map(plan -> {
                        if (plan.getPlan().length() > 0) {
                            plan.setResourceId(R.layout.item_plan_view);
                        } else {
                            plan.setResourceId(R.layout.item_empty_plan_view);
                        }

                        if (!(plan.getPlanDate().length() > 0)) {
                            plan.setPlanDate(date);
                        }

                        plan.setSchoolId(schoolId);
                        return plan;
                    })
                    .toList()
                    .subscribe(plans -> {
                        getView().showDailyPlans(plans);
                        getCompositeDisposable().add(planDao.saveDailyPlans(plans)
                                .subscribeOn(io())
                                .observeOn(mainThread())
                                .subscribe());
                    }, error -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                        error.printStackTrace();
                    }));
        } else {
            getCompositeDisposable().add(planDao.getSchoolPlans(schoolId, date)
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .doOnSubscribe(subscription -> {
                        getView().showProgressBar();
                        getView().startRefreshing();
                    }).doFinally(() -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                    }).subscribe(plans -> getView().showDailyPlans(plans), error -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                        error.printStackTrace();
                    }));
        }
    }
}
