package com.iscagroup.schools.dailyplan;

import com.iscagroup.schools.data.Day;
import com.iscagroup.schools.utils.UserSharedPreferences;

/**
 * Created by Raed Saeed on 8/1/2019.
 */
public class ParentDailyPlanActivity extends DailyPlanActivity {
    @Override
    protected void fetchDayData(Day day) {
        String schoolId = UserSharedPreferences.getUserSchoolId(this);
        String classId = UserSharedPreferences.getUserClassId(this);
        getParentDailyPlans(schoolId, classId, day.getNormalDate());
    }
}
