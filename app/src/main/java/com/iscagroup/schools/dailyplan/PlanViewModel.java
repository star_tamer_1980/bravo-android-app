package com.iscagroup.schools.dailyplan;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.google.gson.JsonObject;
import com.iscagroup.schools.model.remote.DailyPlanService;

import io.reactivex.Observable;

/**
 * Created by Raed Saeed on 8/1/2019.
 */
public class PlanViewModel extends AndroidViewModel {
    private DailyPlanService planService;

    public PlanViewModel(@NonNull Application application) {
        super(application);
        planService = DailyPlanService.getInstance();
    }

    Observable<JsonObject> addPlan(String teacherId, String scheduleId, String plan, String planDate) {
        return planService.addPlan(teacherId, scheduleId, plan, planDate);
    }

    Observable<JsonObject> updatePlan(String planId, String plan) {
        return planService.updatePlan(planId, plan);
    }

    Observable<JsonObject> deletePlan(String planId) {
        return planService.delete(planId);
    }
}
