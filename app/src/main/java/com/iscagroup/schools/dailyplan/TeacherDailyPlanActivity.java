package com.iscagroup.schools.dailyplan;

import com.iscagroup.schools.data.Day;
import com.iscagroup.schools.utils.UserSharedPreferences;

public class TeacherDailyPlanActivity extends DailyPlanActivity {
    @Override
    protected void fetchDayData(Day day) {
        String schoolId = UserSharedPreferences.getUserSchoolId(this);
        String userId = UserSharedPreferences.getUserId(this);
        getTeacherDailyPlans(schoolId, userId, day.getNormalDate());
    }
}
