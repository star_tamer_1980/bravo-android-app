package com.iscagroup.schools.dailyplan;

import com.iscagroup.schools.base.BaseContract;
import com.iscagroup.schools.data.plan.Plan;

import java.util.List;

/**
 * Created by Raed Saeed on 7/31/2019.
 */
public interface DailyPlanContract {
    interface DailyPlanView extends BaseContract.BaseView {
        void getTeacherDailyPlans(String schoolId, String teacherId, String date);

        void getParentDailyPlans(String schoolId, String classId, String date);

        void getSchoolDailyPlans(String schoolId, String date);

        void showDailyPlans(List<Plan> plans);
    }

    interface DailyPlanPresenter {
        void getTeacherPlan(String schoolId, String userId, String date);

        void getParentPlan(String schoolId, String classId, String date);

        void getSchoolPlan(String schoolId, String date);
    }
}
