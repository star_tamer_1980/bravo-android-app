package com.iscagroup.schools.dailyplan;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.AbstractViewHolder;
import com.iscagroup.schools.base.TypeFactory;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.plan.Plan;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.widget.ExpandableTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 7/31/2019.
 */
public class DailyPlanAdapter extends RecyclerView.Adapter<AbstractViewHolder<BaseData>> {
    private List<BaseData> items;

    private TypeFactory typeFactory = new TypeFactory() {
        @Override
        public int type(BaseData object) {
            return object.getResourceId();
        }

        @Override
        public AbstractViewHolder createViewHolder(View view, int viewType) {
            if (viewType == R.layout.item_daily_plan) {
                return new ActivePlanViewHolder(view);
            } else if (viewType == R.layout.item_plan_view) {
                return new ViewPlanViewHolder(view);
            } else if (viewType == R.layout.item_empty_plan_view) {
                return new ViewPlanEmptyViewHolder(view);
            }
            return new EmptyPlanViewHolder(view);
        }
    };

    private OnPlanClickListener onPlanClickListener;

    DailyPlanAdapter() {

    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public AbstractViewHolder<BaseData> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return typeFactory.createViewHolder(view, viewType);

    }

    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder<BaseData> holder, int position) {
        BaseData item = items.get(position);
        if (item != null) {
            holder.bind(item);
        }
    }

    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return typeFactory.type(items.get(position));
    }

    public void load(List<BaseData> dataList) {
        this.items = dataList;
        if (items != null) {
            this.notifyDataSetChanged();
        }
    }

    void addPlans(List<Plan> plans) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.clear();
        items.addAll(plans);
        notifyDataSetChanged();
    }

    void setOnPlanClickListener(OnPlanClickListener onPlanClickListener) {
        this.onPlanClickListener = onPlanClickListener;
    }

    public interface OnPlanClickListener {
        void onPlanClick(int position, Plan plan);
    }

    class ActivePlanViewHolder extends AbstractViewHolder<Plan> {
        private CardView cvPlanCard;
        private TextView tvPlanSubject;
        private TextView tvPlanClass;
        private ExpandableTextView expandablePlan;

        ActivePlanViewHolder(View itemView) {
            super(itemView);
            cvPlanCard = itemView.findViewById(R.id.cv_item_plan_card);
            cvPlanCard.setOnClickListener(v -> onPlanClickListener.onPlanClick(getAdapterPosition(), (Plan) items.get(getAdapterPosition())));
            tvPlanSubject = itemView.findViewById(R.id.tv_item_daily_plan_subject_name);
            tvPlanClass = itemView.findViewById(R.id.tv_item_daily_plan_class_name);
            expandablePlan = itemView.findViewById(R.id.etv_text);
        }

        @Override
        public void bind(Plan element) {
            tvPlanSubject.setText(element.getSubjectName());
            tvPlanClass.setText(element.getClassName());
            expandablePlan.setText(element.getPlan());
        }
    }

    class EmptyPlanViewHolder extends AbstractViewHolder<Plan> {
        private CardView cvPlanCard;
        private TextView tvPlanSubject;
        private TextView tvPlanClass;

        EmptyPlanViewHolder(@NonNull View itemView) {
            super(itemView);
            cvPlanCard = itemView.findViewById(R.id.cv_item_empty_plan_card);
            cvPlanCard.setOnClickListener(v -> onPlanClickListener.onPlanClick(getAdapterPosition(), (Plan) items.get(getAdapterPosition())));
            tvPlanSubject = itemView.findViewById(R.id.tv_item_empty_daily_plan_subject_name);
            tvPlanClass = itemView.findViewById(R.id.tv_item_empty_daily_plan_class_name);
        }

        @Override
        public void bind(Plan element) {
            tvPlanSubject.setText(element.getSubjectName());
            tvPlanClass.setText(element.getClassName());
        }
    }

    class ViewPlanViewHolder extends AbstractViewHolder<Plan> implements View.OnClickListener {
        private CardView cvPlanCard;
        private TextView tvPlanSubject;
        private TextView tvPlanClass;
        private ExpandableTextView expandablePlan;

        ViewPlanViewHolder(@NonNull View itemView) {
            super(itemView);
            cvPlanCard = itemView.findViewById(R.id.cv_item_plan_view_view_card);
            cvPlanCard.setOnClickListener(this);

            tvPlanSubject = itemView.findViewById(R.id.tv_item_plan_view_subject_name);
            tvPlanClass = itemView.findViewById(R.id.tv_item_plan_view_class_name);
            expandablePlan = itemView.findViewById(R.id.etv_text);
        }

        @Override
        public void bind(Plan element) {
            tvPlanSubject.setText(element.getSubjectName());
            tvPlanClass.setText(element.getClassName());
            expandablePlan.setText(element.getPlan());
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), PlanDetailActivity.class);
            intent.putExtra(StringConstants.EXTRA_PLAN, items.get(getAdapterPosition()));
            v.getContext().startActivity(intent);
        }
    }

    class ViewPlanEmptyViewHolder extends AbstractViewHolder<Plan> {
        private CardView cvPlanCard;
        private TextView tvPlanSubject;
        private TextView tvPlanClass;

        ViewPlanEmptyViewHolder(@NonNull View itemView) {
            super(itemView);
            cvPlanCard = itemView.findViewById(R.id.cv_item_empty_plan_view_card);
            tvPlanSubject = itemView.findViewById(R.id.tv_item_empty_plan_view_subject_name);
            tvPlanClass = itemView.findViewById(R.id.tv_item_empty_plan_view_class_name);
        }

        @Override
        public void bind(Plan element) {
            tvPlanSubject.setText(element.getSubjectName());
            tvPlanClass.setText(element.getClassName());
        }
    }
}