package com.iscagroup.schools.dialog;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseBottomDialogFragment;
import com.iscagroup.schools.base.BaseDialogFragment;
import com.iscagroup.schools.utils.CheckPermissions;

/**
 * Created by Raed Saeed on 7/31/2019.
 */
public class BottomFilePickerDialog extends BaseBottomDialogFragment implements View.OnClickListener {

    public static final int DOCUMENT_REQUEST_CODE = 1;
    @SuppressWarnings("unused")
    private static final String TAG = "FilePickerDialog";
    private Context context;

    private View tempClickView;

    private boolean isActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_file_picker, null, false);

        ImageView ivFilePickerDialogDocument = view.findViewById(R.id.iv_dialog_file_picker_pick_document);
        ivFilePickerDialogDocument.setOnClickListener(this);

        ImageView ivFilePickerDialogImage = view.findViewById(R.id.iv_dialog_file_picker_pick_image);
        ivFilePickerDialogImage.setOnClickListener(this);
        return view;
    }

    @Override
    public void setClickListener(BaseDialogFragment.DialogClickListener clickListener) {
        super.setClickListener(clickListener);
    }

    public void setActivity(boolean activity) {
        isActivity = activity;
    }

    @Override
    public void onClick(View v) {
        tempClickView = v;
        switch (v.getId()) {
            case R.id.iv_dialog_file_picker_pick_image:
            case R.id.iv_dialog_file_picker_pick_document:
                if (isActivity) {
                    if (CheckPermissions.checkReadStoragePermissions(getActivity(), this)) {
                        getClickListener().onDialogClick(v);
                        dismiss();
                    }
                } else {
                    if (CheckPermissions.checkReadStoragePermissions(context, this)) {
                        getClickListener().onDialogClick(v);
                        dismiss();
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (tempClickView != null) {
                getClickListener().onDialogClick(tempClickView);
            }
        }
    }
}
