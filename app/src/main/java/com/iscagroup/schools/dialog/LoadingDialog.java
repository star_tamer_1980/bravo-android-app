package com.iscagroup.schools.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.iscagroup.schools.R;


/**
 * Created by Raed Saeed on 6/18/2019.
 */

public class LoadingDialog extends DialogFragment {
    private static final String TAG = "LoadingDialog";

    private static final String MESSAGE = "loading_message";
    private Context context;

    public static LoadingDialog getInstance(String message) {
        LoadingDialog loadingDialog = new LoadingDialog();
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE, message);
        loadingDialog.setArguments(bundle);
        return loadingDialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_loading, null, false);
        builder.setView(view);

        TextView textView = view.findViewById(R.id.tv_dialog_loading);
        if (getArguments() != null) {
            String tag = getArguments().getString(MESSAGE);
            if (tag != null) {
                textView.setText(tag);
            }
        } else {
            textView.setText(R.string.processing);
        }

        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        return alertDialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }
}
