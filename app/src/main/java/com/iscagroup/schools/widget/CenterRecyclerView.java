package com.iscagroup.schools.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;


/**
 * Created by Raed Saeed on 7/30/2019
 * For Future Implementation :(
 */

public class CenterRecyclerView extends RecyclerView {
    private static final String TAG = "CustomRecyclerView";

    // this the the default width of screen elements;
    private static final int defaultWidth = 7;

    private LayoutManager layoutManager;
    // get the width of screen and divide it by 7 the apply for the items in recyclerView

    private OnViewDragListener onDragListener;
    /**
     * A scroll listener to check the items inside recyclerView
     * and modify the items inside the adapter
     */
    private OnScrollListener onScrollListener = new OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);     // an integer referes to the first position for scrolling
            int position = ((computeHorizontalScrollOffset()) / (getMeasuredWidth() / 7));


            switch (newState) {
                case RecyclerView.SCROLL_STATE_IDLE:
                    Log.w(TAG, "onScrollStateChanged: called : state idle position is " + position);
//                    onDragListener.onStateIdle(position);
                    break;
                case SCROLL_STATE_DRAGGING:
                    Log.w(TAG, "onScrollStateChanged: called : state dragging");
//                    onDragListener.onStateDragging();
                    break;
            }
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
        }
    };

    public CenterRecyclerView(@NonNull Context context) {
        super(context);
    }

    public CenterRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CenterRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * call the super method of Recycler view and set the layout manager to it
     *
     * @param layout for initiate the layout manager
     */
    @Override
    public void setLayoutManager(@Nullable LayoutManager layout) {
        super.setLayoutManager(layout);
        this.layoutManager = layout;
        addScrollListener();
    }

    private void addScrollListener() {
        super.addOnScrollListener(onScrollListener);
    }

    @Override
    public void addOnScrollListener(@NonNull OnScrollListener listener) {
        super.addOnScrollListener(onScrollListener);
    }

    @Override
    public void removeOnScrollListener(@NonNull OnScrollListener listener) {
        super.removeOnScrollListener(onScrollListener);
    }

    @Override
    public void setAdapter(@Nullable Adapter adapter) {
        super.setAdapter(adapter);
    }

    @Override
    public void smoothScrollToPosition(int position) {
        final RecyclerView.SmoothScroller smoothScroller = new CenterSmoothScroller(getContext());
        smoothScroller.setTargetPosition(position);
        post(() -> layoutManager.startSmoothScroll(smoothScroller));
    }


    public void setDate(int offset) {
        smoothScrollToPosition(offset);
    }


    public void setOnDragListener(OnViewDragListener onDragListener) {
        this.onDragListener = onDragListener;
    }

    public interface OnViewDragListener {
        void onStateDragging();

        void onStateIdle(int position);
    }

    /**
     * A static class to for scrolling to a specific position
     */
    private static class CenterSmoothScroller extends LinearSmoothScroller {
        CenterSmoothScroller(Context context) {
            super(context);
        }

        @Override
        public int calculateDtToFit(int viewStart, int viewEnd, int boxStart, int boxEnd, int snapPreference) {
            return (boxStart + (boxEnd - boxStart) / 2) - (viewStart + (viewEnd - viewStart) / 2);
        }
    }
}
