package com.iscagroup.schools.data.request;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Raed Saeed on 7/4/2019.
 */

@SuppressWarnings("unused")
public class AttachTypeConverter {
    @TypeConverter
    public String convertAttachListToString(List<Attach> attachList) {
        if (attachList == null) {
            return (null);
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<Attach>>() {
        }.getType();
        return gson.toJson(attachList, type);
    }

    @TypeConverter
    public List<Attach> convertAttachStringToList(String attachString) {
        if (attachString == null) {
            return (null);
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<Attach>>() {
        }.getType();
        return gson.fromJson(attachString, type);
    }
}
