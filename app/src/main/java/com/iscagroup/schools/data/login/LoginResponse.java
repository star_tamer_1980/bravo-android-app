
package com.iscagroup.schools.data.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class LoginResponse implements Parcelable {

    public static final Creator<LoginResponse> CREATOR = new Creator<LoginResponse>() {
        @Override
        public LoginResponse createFromParcel(Parcel in) {
            return new LoginResponse(in);
        }

        @Override
        public LoginResponse[] newArray(int size) {
            return new LoginResponse[size];
        }
    };
    @SerializedName("msg")
    private String mMsg;
    @SerializedName("Result")
    private List<Result> mResult;
    @SerializedName("status")
    private Boolean mStatus;

    private LoginResponse(Parcel in) {
        mMsg = in.readString();
        mResult = in.createTypedArrayList(Result.CREATOR);
        byte tmpMStatus = in.readByte();
        mStatus = tmpMStatus == 0 ? null : tmpMStatus == 1;
    }

    public String getMsg() {
        return mMsg;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public List<Result> getResult() {
        return mResult;
    }

    public void setResult(List<Result> result) {
        mResult = result;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMsg);
        dest.writeTypedList(mResult);
        dest.writeByte((byte) (mStatus == null ? 0 : mStatus ? 1 : 2));
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "mMsg='" + mMsg + '\'' +
                ", mResult=" + mResult +
                ", mStatus=" + mStatus +
                '}';
    }
}
