package com.iscagroup.schools.data.driver;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Raed Saeed on 8/21/2019.
 */

public class DriverResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("Result")
    @Expose
    private List<Driver> drivers = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }

    @NonNull
    @Override
    public String toString() {
        return "DriverResponse{" +
                "status=" + status +
                ", msg='" + msg + '\'' +
                ", drivers=" + drivers +
                '}';
    }
}
