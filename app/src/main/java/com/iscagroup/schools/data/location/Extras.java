package com.iscagroup.schools.data.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by Raed Saeed on 8/19/2019.
 */

@SuppressWarnings("unused")
public class Extras {

    @SerializedName("empty")
    @Expose
    private Boolean empty;
    @SerializedName("emptyParcel")
    @Expose
    private Boolean emptyParcel;
    @SerializedName("parcelled")
    @Expose
    private Boolean parcelled;

    public Extras() {

    }

    public Boolean getEmpty() {
        return empty;
    }

    public void setEmpty(Boolean empty) {
        this.empty = empty;
    }

    public Boolean getEmptyParcel() {
        return emptyParcel;
    }

    public void setEmptyParcel(Boolean emptyParcel) {
        this.emptyParcel = emptyParcel;
    }

    public Boolean getParcelled() {
        return parcelled;
    }

    public void setParcelled(Boolean parcelled) {
        this.parcelled = parcelled;
    }
}
