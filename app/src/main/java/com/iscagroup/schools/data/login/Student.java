
package com.iscagroup.schools.data.login;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

import java.util.List;

@SuppressWarnings("unused")
@Entity(tableName = "students")
public class Student extends BaseData implements Parcelable {

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };
    @ColumnInfo(name = "active")
    @SerializedName("active")
    private String active;
    @ColumnInfo(name = "birth_date")
    @SerializedName("birthdate")
    private String mBirthdate;
    @ColumnInfo(name = "class_id")
    @SerializedName("class_id")
    private String mClassId;
    @ColumnInfo(name = "class_name")
    @SerializedName("class_name")
    private String mClassName;
    @ColumnInfo(name = "emergency_number")
    @SerializedName("emergency_number")
    private String mEmergencyNumber;
    @ColumnInfo(name = "emergency_number2")
    @SerializedName("emergency_number2")
    private String mEmergencyNumber2;
    @ColumnInfo(name = "first_name")
    @SerializedName("first_name")
    private String mFirstName;
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private String mId;
    @ColumnInfo(name = "last_name")
    @SerializedName("last_name")
    private String mLastName;
    @ColumnInfo(name = "school_id")
    @SerializedName("school_id")
    private String mSchoolId;
    @ColumnInfo(name = "school_logo")
    @SerializedName("school_logo")
    private String mSchoolLogo;
    @ColumnInfo(name = "school_name")
    @SerializedName("school_name")
    private String mSchoolName;

    @SerializedName("student_photo")
    private String studentPhoto;

    private List<HomeModel> homeList;

    private Student(Parcel in) {
        super(in);
        active = in.readString();
        mBirthdate = in.readString();
        mClassId = in.readString();
        mClassName = in.readString();
        mEmergencyNumber = in.readString();
        mEmergencyNumber2 = in.readString();
        mFirstName = in.readString();
        mId = in.readString();
        mLastName = in.readString();
        mSchoolId = in.readString();
        mSchoolLogo = in.readString();
        mSchoolName = in.readString();
        studentPhoto = in.readString();
        homeList = in.createTypedArrayList(HomeModel.CREATOR);
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getBirthdate() {
        return mBirthdate;
    }

    public void setBirthdate(String birthdate) {
        mBirthdate = birthdate;
    }

    public String getClassId() {
        return mClassId;
    }

    public void setClassId(String classId) {
        mClassId = classId;
    }

    public String getClassName() {
        return mClassName;
    }

    public void setClassName(String className) {
        mClassName = className;
    }

    public String getEmergencyNumber() {
        return mEmergencyNumber;
    }

    public void setEmergencyNumber(String emergencyNumber) {
        mEmergencyNumber = emergencyNumber;
    }

    public String getEmergencyNumber2() {
        return mEmergencyNumber2;
    }

    public void setEmergencyNumber2(String emergencyNumber2) {
        mEmergencyNumber2 = emergencyNumber2;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getSchoolId() {
        return mSchoolId;
    }

    public void setSchoolId(String schoolId) {
        mSchoolId = schoolId;
    }

    public String getSchoolLogo() {
        return mSchoolLogo;
    }

    public void setSchoolLogo(String schoolLogo) {
        mSchoolLogo = schoolLogo;
    }

    public String getSchoolName() {
        return mSchoolName;
    }

    public void setSchoolName(String schoolName) {
        mSchoolName = schoolName;
    }

    public String getStudentPhoto() {
        return studentPhoto;
    }

    public void setStudentPhoto(String studentPhoto) {
        this.studentPhoto = studentPhoto;
    }

    public List<HomeModel> getHomeList() {
        return homeList;
    }

    public void setHomeList(List<HomeModel> homeList) {
        this.homeList = homeList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(active);
        dest.writeString(mBirthdate);
        dest.writeString(mClassId);
        dest.writeString(mClassName);
        dest.writeString(mEmergencyNumber);
        dest.writeString(mEmergencyNumber2);
        dest.writeString(mFirstName);
        dest.writeString(mId);
        dest.writeString(mLastName);
        dest.writeString(mSchoolId);
        dest.writeString(mSchoolLogo);
        dest.writeString(mSchoolName);
        dest.writeString(studentPhoto);
        dest.writeTypedList(homeList);
    }

    @NonNull
    @Override
    public String toString() {
        return "Student{" +
                "active='" + active + '\'' +
                ", mBirthdate='" + mBirthdate + '\'' +
                ", mClassId='" + mClassId + '\'' +
                ", mClassName='" + mClassName + '\'' +
                ", mEmergencyNumber='" + mEmergencyNumber + '\'' +
                ", mEmergencyNumber2='" + mEmergencyNumber2 + '\'' +
                ", mFirstName='" + mFirstName + '\'' +
                ", mId='" + mId + '\'' +
                ", mLastName='" + mLastName + '\'' +
                ", mSchoolId='" + mSchoolId + '\'' +
                ", mSchoolLogo='" + mSchoolLogo + '\'' +
                ", mSchoolName='" + mSchoolName + '\'' +
                ", studentPhoto='" + studentPhoto + '\'' +
                ", homeList=" + homeList +
                '}';
    }
}

