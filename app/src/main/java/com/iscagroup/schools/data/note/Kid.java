package com.iscagroup.schools.data.note;

/**
 * Created by Raed Saeed on 7/24/2019.
 */

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

public class Kid extends BaseData {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("student_name")
    @Expose
    private String studentName;
    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    @NonNull
    @Override
    public String toString() {
        return "Kid{" +
                "id='" + id + '\'' +
                ", studentName='" + studentName + '\'' +
                ", classId='" + classId + '\'' +
                ", schoolId='" + schoolId + '\'' +
                '}';
    }
}