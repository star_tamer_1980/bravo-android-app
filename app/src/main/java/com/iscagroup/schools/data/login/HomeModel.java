
package com.iscagroup.schools.data.login;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

@SuppressWarnings("unused")
@Entity(tableName = "home")
public class HomeModel extends BaseData implements Parcelable {

    public static final Creator<HomeModel> CREATOR = new Creator<HomeModel>() {
        @Override
        public HomeModel createFromParcel(Parcel in) {
            return new HomeModel(in);
        }

        @Override
        public HomeModel[] newArray(int size) {
            return new HomeModel[size];
        }
    };
    @ColumnInfo(name = "active")
    @SerializedName("active")
    private String mActive;
    @ColumnInfo(name = "feature")
    @SerializedName("feature")
    private String mFeature;
    @ColumnInfo(name = "icon_path")
    @SerializedName("icon_path")
    private String mIconPath;
    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private String mId;
    @ColumnInfo(name = "sort")
    @SerializedName("sort")
    private String mSort;

    private HomeModel(Parcel in) {
        super(in);
        mActive = in.readString();
        mFeature = in.readString();
        mIconPath = in.readString();
        mId = in.readString();
        mSort = in.readString();
    }

    public String getActive() {
        return mActive;
    }

    public void setActive(String active) {
        mActive = active;
    }

    public String getFeature() {
        return mFeature;
    }

    public void setFeature(String feature) {
        mFeature = feature;
    }

    public String getIconPath() {
        return mIconPath;
    }

    public void setIconPath(String iconPath) {
        mIconPath = iconPath;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getSort() {
        return mSort;
    }

    public void setSort(String sort) {
        mSort = sort;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mActive);
        dest.writeString(mFeature);
        dest.writeString(mIconPath);
        dest.writeString(mId);
        dest.writeString(mSort);
    }

    @NonNull
    @Override
    public String toString() {
        return "HomeModel{" +
                "mActive='" + mActive + '\'' +
                ", mFeature='" + mFeature + '\'' +
                ", mIconPath='" + mIconPath + '\'' +
                ", mId='" + mId + '\'' +
                ", mSort='" + mSort + '\'' +
                '}';
    }
}
