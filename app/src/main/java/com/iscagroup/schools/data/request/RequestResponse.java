
package com.iscagroup.schools.data.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class RequestResponse {

    @SerializedName("msg")
    private String mMsg;
    @SerializedName("Result")
    private List<Request> mRequest;
    @SerializedName("status")
    private Boolean mStatus;

    public String getMsg() {
        return mMsg;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public List<Request> getResult() {
        return mRequest;
    }

    public void setResult(List<Request> request) {
        mRequest = request;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

}
