package com.iscagroup.schools.data.plan;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.request.Attach;
import com.iscagroup.schools.data.request.AttachTypeConverter;

import java.util.List;
import java.util.Objects;

/**
 * Created by Raed Saeed on 7/31/2019.
 */
@SuppressWarnings("unused")
@Entity(tableName = "plan")
@TypeConverters(AttachTypeConverter.class)
public class Plan extends BaseData implements Parcelable {
    public static final Creator<Plan> CREATOR = new Creator<Plan>() {
        @Override
        public Plan createFromParcel(Parcel in) {
            return new Plan(in);
        }

        @Override
        public Plan[] newArray(int size) {
            return new Plan[size];
        }
    };
    @PrimaryKey
    @NonNull
    @SerializedName("schedule_id")
    @Expose
    private String scheduleId;
    @SerializedName("day_name")
    @Expose
    private String dayName;
    @SerializedName("subject_id")
    @Expose
    private String subjectId;
    @SerializedName("subject_name")
    @Expose
    private String subjectName;
    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("class_name")
    @Expose
    private String className;
    @SerializedName("teacher_user_id")
    @Expose
    private String teacherUserId;
    @SerializedName("teacher_name")
    @Expose
    private String teacherName;
    @SerializedName("plan_id")
    @Expose
    private String planId;
    @SerializedName("plan_date")
    @Expose
    private String planDate;
    @SerializedName("plan")
    @Expose
    private String plan;
    @SerializedName("attach")
    @Expose
    private List<Attach> attach;
    private String schoolId;


    @Ignore
    private Plan(Parcel in) {
        super(in);
        scheduleId = Objects.requireNonNull(in.readString());
        dayName = in.readString();
        subjectId = in.readString();
        subjectName = in.readString();
        classId = in.readString();
        className = in.readString();
        teacherUserId = in.readString();
        teacherName = in.readString();
        planId = Objects.requireNonNull(in.readString());
        planDate = in.readString();
        plan = in.readString();
        attach = in.createTypedArrayList(Attach.CREATOR);
        schoolId = in.readString();
    }

    public Plan(@NonNull String scheduleId, String dayName, String subjectId, String subjectName,
                String classId, String className, String teacherUserId,
                String teacherName, @NonNull String planId, String planDate,
                String plan, List<Attach> attach, String schoolId) {
        this.scheduleId = scheduleId;
        this.dayName = dayName;
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.classId = classId;
        this.className = className;
        this.teacherUserId = teacherUserId;
        this.teacherName = teacherName;
        this.planId = planId;
        this.planDate = planDate;
        this.plan = plan;
        this.attach = attach;
        this.schoolId = schoolId;
    }

    @NonNull
    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(@NonNull String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTeacherUserId() {
        return teacherUserId;
    }

    public void setTeacherUserId(String teacherUserId) {
        this.teacherUserId = teacherUserId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public List<Attach> getAttach() {
        return attach;
    }

    public void setAttach(List<Attach> attach) {
        this.attach = attach;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(scheduleId);
        dest.writeString(dayName);
        dest.writeString(subjectId);
        dest.writeString(subjectName);
        dest.writeString(classId);
        dest.writeString(className);
        dest.writeString(teacherUserId);
        dest.writeString(teacherName);
        dest.writeString(planId);
        dest.writeString(planDate);
        dest.writeString(plan);
        dest.writeTypedList(attach);
        dest.writeString(schoolId);
    }

    @NonNull
    @Override
    public String toString() {
        return "Plan{" +
                "scheduleId='" + scheduleId + '\'' +
                ", dayName='" + dayName + '\'' +
                ", subjectId='" + subjectId + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", classId='" + classId + '\'' +
                ", className='" + className + '\'' +
                ", teacherUserId='" + teacherUserId + '\'' +
                ", teacherName='" + teacherName + '\'' +
                ", planId='" + planId + '\'' +
                ", planDate='" + planDate + '\'' +
                ", plan='" + plan + '\'' +
                ", attach=" + attach +
                ", schoolId='" + schoolId + '\'' +
                '}';
    }
}
