package com.iscagroup.schools.data.note;

/**
 * Created by Raed Saeed on 7/21/2019.
 */

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

public class ClassModel extends BaseData {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("grade_id")
    @Expose
    private String gradeId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    @NonNull
    @Override
    public String toString() {
        return "ClassModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", gradeId='" + gradeId + '\'' +
                ", schoolId='" + schoolId + '\'' +
                '}';
    }
}