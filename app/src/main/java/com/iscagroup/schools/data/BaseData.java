package com.iscagroup.schools.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Raed Saeed on 6/27/2019.
 * This is the base class of every model and must every model inherit from it
 * because it contain the resource id (xml) for every model that should be showing on screen.
 */
public class BaseData implements Parcelable {
    private int resourceId;

    public static final Creator<BaseData> CREATOR = new Creator<BaseData>() {
        @Override
        public BaseData createFromParcel(Parcel in) {
            return new BaseData(in);
        }

        @Override
        public BaseData[] newArray(int size) {
            return new BaseData[size];
        }
    };

    protected BaseData() {

    }

    protected BaseData(Parcel in) {
        resourceId = in.readInt();
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(resourceId);
    }
}
