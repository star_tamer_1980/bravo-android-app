
package com.iscagroup.schools.data.login;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

import java.util.List;

@SuppressWarnings("unused")
@Entity(tableName = "schools")
public class School extends BaseData implements Parcelable {

    public static final Creator<School> CREATOR = new Creator<School>() {
        @Override
        public School createFromParcel(Parcel in) {
            return new School(in);
        }

        @Override
        public School[] newArray(int size) {
            return new School[size];
        }
    };
    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private String mId;
    @ColumnInfo(name = "active")
    @SerializedName("active")
    private String mActive;
    @ColumnInfo(name = "added_by")
    @SerializedName("added_by")
    private String mAddedBy;
    @ColumnInfo(name = "added_on")
    @SerializedName("added_on")
    private String mAddedOn;
    @ColumnInfo(name = "email")
    @SerializedName("email")
    private String mEmail;
    @ColumnInfo(name = "facebook")
    @SerializedName("facebook")
    private String mFacebook;
    @ColumnInfo(name = "logo")
    @SerializedName("logo")
    private String mLogo;
    @ColumnInfo(name = "student_name")
    @SerializedName("name")
    private String mName;
    @ColumnInfo(name = "student_phone_one")
    @SerializedName("phone1")
    private String mPhone1;
    @ColumnInfo(name = "student_phone_two")
    @SerializedName("phone2")
    private String mPhone2;
    @ColumnInfo(name = "teacher_news_permission")
    @SerializedName("teacher_add_news")
    private String mTeacherAddNews;
    @ColumnInfo(name = "user_id")
    @SerializedName("user_id")
    private String mUserId;

    private List<HomeModel> homeList;

    private School(Parcel in) {
        super(in);
        mActive = in.readString();
        mAddedBy = in.readString();
        mAddedOn = in.readString();
        mEmail = in.readString();
        mFacebook = in.readString();
        mId = in.readString();
        mLogo = in.readString();
        mName = in.readString();
        mPhone1 = in.readString();
        mPhone2 = in.readString();
        mTeacherAddNews = in.readString();
        mUserId = in.readString();
        homeList = in.createTypedArrayList(HomeModel.CREATOR);
    }

    public String getActive() {
        return mActive;
    }

    public void setActive(String active) {
        mActive = active;
    }

    public String getAddedBy() {
        return mAddedBy;
    }

    public void setAddedBy(String addedBy) {
        mAddedBy = addedBy;
    }

    public String getAddedOn() {
        return mAddedOn;
    }

    public void setAddedOn(String addedOn) {
        mAddedOn = addedOn;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFacebook() {
        return mFacebook;
    }

    public void setFacebook(String facebook) {
        mFacebook = facebook;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getLogo() {
        return mLogo;
    }

    public void setLogo(String logo) {
        mLogo = logo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhone1() {
        return mPhone1;
    }

    public void setPhone1(String phone1) {
        mPhone1 = phone1;
    }

    public String getPhone2() {
        return mPhone2;
    }

    public void setPhone2(String phone2) {
        mPhone2 = phone2;
    }

    public String getTeacherAddNews() {
        return mTeacherAddNews;
    }

    public void setTeacherAddNews(String teacherAddNews) {
        mTeacherAddNews = teacherAddNews;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public List<HomeModel> getHomeList() {
        return homeList;
    }

    public void setHomeList(List<HomeModel> list) {
        this.homeList = list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mActive);
        dest.writeString(mAddedBy);
        dest.writeString(mAddedOn);
        dest.writeString(mEmail);
        dest.writeString(mFacebook);
        dest.writeString(mId);
        dest.writeString(mLogo);
        dest.writeString(mName);
        dest.writeString(mPhone1);
        dest.writeString(mPhone2);
        dest.writeString(mTeacherAddNews);
        dest.writeString(mUserId);
        dest.writeTypedList(homeList);
    }

    @NonNull
    @Override
    public String toString() {
        return "School{" +
                "mId='" + mId + '\'' +
                ", mActive='" + mActive + '\'' +
                ", mAddedBy='" + mAddedBy + '\'' +
                ", mAddedOn='" + mAddedOn + '\'' +
                ", mEmail='" + mEmail + '\'' +
                ", mFacebook='" + mFacebook + '\'' +
                ", mLogo='" + mLogo + '\'' +
                ", mName='" + mName + '\'' +
                ", mPhone1='" + mPhone1 + '\'' +
                ", mPhone2='" + mPhone2 + '\'' +
                ", mTeacherAddNews='" + mTeacherAddNews + '\'' +
                ", mUserId='" + mUserId + '\'' +
                '}';
    }
}
