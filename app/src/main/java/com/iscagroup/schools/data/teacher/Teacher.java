
package com.iscagroup.schools.data.teacher;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

@SuppressWarnings("unused")
public class Teacher extends BaseData implements Parcelable {

    public static final Creator<Teacher> CREATOR = new Creator<Teacher>() {
        @Override
        public Teacher createFromParcel(Parcel in) {
            return new Teacher(in);
        }

        @Override
        public Teacher[] newArray(int size) {
            return new Teacher[size];
        }
    };
    @SerializedName("name")
    private String mName;
    @SerializedName("teacher_user_id")
    private String mTeacherUserId;

    private Teacher(Parcel in) {
        super(in);
        mName = in.readString();
        mTeacherUserId = in.readString();
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getTeacherUserId() {
        return mTeacherUserId;
    }

    public void setTeacherUserId(String teacherUserId) {
        mTeacherUserId = teacherUserId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mName);
        dest.writeString(mTeacherUserId);
    }
}
