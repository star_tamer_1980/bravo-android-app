package com.iscagroup.schools.data.note;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.request.Attach;
import com.iscagroup.schools.data.request.AttachTypeConverter;

import java.util.List;
import java.util.Objects;


/**
 * Created by Raed Saeed on 7/23/2019.
 */
@Entity(tableName = "notes")
@TypeConverters(AttachTypeConverter.class)
public class Note extends BaseData implements Parcelable {

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    @SerializedName("id")
    @Expose
    @PrimaryKey
    @NonNull
    private String id = "";
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("note_type")
    @Expose
    private String noteType;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("stage_id")
    @Expose
    private String stageId;
    @SerializedName("grade_id")
    @Expose
    private String gradeId;
    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("added_by")
    @Expose
    private String addedBy;
    @SerializedName("added_on")
    @Expose
    private String addedOn;
    @SerializedName("note_to")
    @Expose
    private String noteTo;
    @SerializedName("attach")
    @Expose
    private List<Attach> attach = null;

    private String teacherId = "";

    @Ignore
    private Note(Parcel in) {
        super(in);
        id = Objects.requireNonNull(in.readString());
        categoryId = in.readString();
        categoryName = in.readString();
        note = in.readString();
        noteType = in.readString();
        studentId = in.readString();
        stageId = in.readString();
        gradeId = in.readString();
        classId = in.readString();
        schoolId = in.readString();
        teacherId = in.readString();
        addedBy = in.readString();
        addedOn = in.readString();
        noteTo = in.readString();
        attach = in.createTypedArrayList(Attach.CREATOR);
    }

    public Note(@NonNull String id, String categoryId, String categoryName, String note, String noteType,
                String studentId, String stageId, String gradeId, String classId, String schoolId,
                String teacherId, String addedBy, String addedOn, String noteTo, List<Attach> attach) {
        this.id = id;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.note = note;
        this.noteType = noteType;
        this.studentId = studentId;
        this.stageId = stageId;
        this.gradeId = gradeId;
        this.classId = classId;
        this.schoolId = schoolId;
        this.teacherId = teacherId;
        this.addedBy = addedBy;
        this.addedOn = addedOn;
        this.noteTo = noteTo;
        this.attach = attach;
    }

    @Ignore
    public Note() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(String addedOn) {
        this.addedOn = addedOn;
    }

    public String getNoteTo() {
        return noteTo;
    }

    public void setNoteTo(String noteTo) {
        this.noteTo = noteTo;
    }

    public List<Attach> getAttach() {
        return attach;
    }

    public void setAttach(List<Attach> attach) {
        this.attach = attach;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(id);
        dest.writeString(categoryId);
        dest.writeString(categoryName);
        dest.writeString(note);
        dest.writeString(noteType);
        dest.writeString(studentId);
        dest.writeString(stageId);
        dest.writeString(gradeId);
        dest.writeString(classId);
        dest.writeString(schoolId);
        dest.writeString(teacherId);
        dest.writeString(addedBy);
        dest.writeString(addedOn);
        dest.writeString(noteTo);
        dest.writeTypedList(attach);
    }

    @NonNull
    @Override
    public String toString() {
        return "Note{" +
                "id='" + id + '\'' +
                ", categoryId='" + categoryId + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", note='" + note + '\'' +
                ", noteType='" + noteType + '\'' +
                ", studentId='" + studentId + '\'' +
                ", stageId='" + stageId + '\'' +
                ", gradeId='" + gradeId + '\'' +
                ", classId='" + classId + '\'' +
                ", schoolId='" + schoolId + '\'' +
                ", addedBy='" + addedBy + '\'' +
                ", addedOn='" + addedOn + '\'' +
                ", noteTo='" + noteTo + '\'' +
                ", attach=" + attach +
                ", resource id = " + getResourceId() +
                '}';
    }
}