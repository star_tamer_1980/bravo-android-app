package com.iscagroup.schools.data.login;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Raed Saeed on 6/26/2019.
 */
@SuppressWarnings("unused")
public class LoginResponseTypConverter {
    @TypeConverter
    public String convertHomeListToString(List<HomeModel> homeModels) {
        if (homeModels == null) {
            return (null);
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<HomeModel>>() {
        }.getType();
        return gson.toJson(homeModels, type);
    }

    @TypeConverter
    public List<HomeModel> convertHomeJsonToList(String home) {
        if (home == null) {
            return (null);
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<HomeModel>>() {
        }.getType();
        return gson.fromJson(home, type);
    }

    @TypeConverter
    public String convertStudentListToString(List<Student> students) {
        if (students == null) {
            return (null);
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<Student>>() {
        }.getType();
        return gson.toJson(students, type);
    }

    @TypeConverter
    public List<Student> convertStudentStringToList(String studentString) {
        if (studentString == null) {
            return (null);
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<Student>>() {
        }.getType();
        return gson.fromJson(studentString, type);
    }

    @TypeConverter
    public String convertSchoolListToString(List<School> schools) {
        if (schools == null) {
            return (null);
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<School>>() {
        }.getType();
        return gson.toJson(schools, type);
    }

    @TypeConverter
    public List<School> convertSchoolStringToList(String schoolString) {
        if (schoolString == null) {
            return (null);
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<School>>() {
        }.getType();
        return gson.fromJson(schoolString, type);
    }
}
