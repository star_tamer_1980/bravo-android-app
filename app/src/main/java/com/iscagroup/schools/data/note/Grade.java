package com.iscagroup.schools.data.note;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

import java.util.List;

/**
 * Created by Raed Saeed on 7/21/2019.
 */
public class Grade extends BaseData {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("grade")
    @Expose
    private String grade;
    @SerializedName("stage_id")
    @Expose
    private String stageId;
    @SerializedName("classes")
    @Expose
    private List<ClassModel> classModels = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public List<ClassModel> getClassModels() {
        return classModels;
    }

    public void setClassModels(List<ClassModel> classModels) {
        this.classModels = classModels;
    }

    @NonNull
    @Override
    public String toString() {
        return "Grade{" +
                "id='" + id + '\'' +
                ", grade='" + grade + '\'' +
                ", stageId='" + stageId + '\'' +
                ", classModels=" + classModels +
                '}';
    }
}
