
package com.iscagroup.schools.data.teacher;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class TeacherResponse {

    @SerializedName("msg")
    private String mMsg;
    @SerializedName("Result")
    private List<Teacher> mTeacher;
    @SerializedName("status")
    private Boolean mStatus;

    public String getMsg() {
        return mMsg;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public List<Teacher> getTeachers() {
        return mTeacher;
    }

    public void setTeachers(List<Teacher> result) {
        mTeacher = result;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

}
