package com.iscagroup.schools.data.note;

/**
 * Created by Raed Saeed on 7/21/2019.
 */

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

import java.util.List;

public class Stage extends BaseData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("stage")
    @Expose
    private String stage;
    @SerializedName("grades")
    @Expose
    private List<Grade> grades = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public List<Grade> getGrades() {
        return grades;
    }

    public void setGrades(List<Grade> grades) {
        this.grades = grades;
    }


    @NonNull
    @Override
    public String toString() {
        return "Stage{" +
                "id='" + id + '\'' +
                ", stage='" + stage + '\'' +
                ", grades=" + grades +
                '}';
    }
}