package com.iscagroup.schools.data.note;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Raed Saeed on 7/24/2019.
 */
public class NoteClassResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("classes")
    @Expose
    private List<NoteTeacherClass> classes = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<NoteTeacherClass> getClasses() {
        return classes;
    }

    public void setClasses(List<NoteTeacherClass> classes) {
        this.classes = classes;
    }

    @NonNull
    @Override
    public String toString() {
        return "NoteClassResponse{" +
                "status=" + status +
                ", msg='" + msg + '\'' +
                ", classes=" + classes +
                '}';
    }
}