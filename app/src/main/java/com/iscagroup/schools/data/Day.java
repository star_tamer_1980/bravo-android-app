package com.iscagroup.schools.data;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import org.joda.time.DateTime;

import java.util.Locale;

/**
 * Created by Raed Saeed on 3/3/2019.
 */

public class Day extends BaseData implements Parcelable {
    public static final Creator<Day> CREATOR = new Creator<Day>() {
        @Override
        public Day createFromParcel(Parcel in) {
            return new Day(in);
        }

        @Override
        public Day[] newArray(int size) {
            return new Day[size];
        }
    };

    private DateTime date;
    private String dayString;
    private String weekString;
    private String monthString;
    private String yearString;
    private String dateString;
    private boolean selected;

    public Day(DateTime date) {
        this.date = date;
        dayString = date.toString("E", Locale.getDefault());
        weekString = date.toString("EEE", Locale.getDefault()).toUpperCase();
        monthString = date.toString("d MMM", Locale.getDefault());
        yearString = date.toString("YYYY", Locale.getDefault());
        dateString = date.toString("yyyy-MM-dd", Locale.getDefault());
    }

    private Day(Parcel in) {
        super(in);
        selected = in.readByte() != 0;
        dayString = in.readString();
        weekString = in.readString();
        monthString = in.readString();
        yearString = in.readString();
        dateString = in.readString();
    }

    public String getDay() {
        return dayString;
    }

    public String getWeekDay() {
        return weekString;
    }

    public String getMonth() {
        return monthString;
    }

    public String getYear() {
        return yearString;
    }

    private DateTime getDate() {
        return date.withTime(0, 0, 0, 0);
    }

    public String getNormalDate() {
        return dateString;
    }

    public boolean isToday() {
        DateTime today = new DateTime().withTime(0, 0, 0, 0);
        return getDate().getMillis() == today.getMillis();
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @NonNull
    @Override
    public String toString() {
        return "Day{" +
                "date=" + date +
                ", dayString='" + dayString + '\'' +
                ", weekString='" + weekString + '\'' +
                ", monthString='" + monthString + '\'' +
                ", yearString='" + yearString + '\'' +
                ", dateString='" + dateString + '\'' +
                ", selected=" + selected +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeByte((byte) (selected ? 1 : 0));
        dest.writeString(dayString);
        dest.writeString(weekString);
        dest.writeString(monthString);
        dest.writeString(yearString);
        dest.writeString(dateString);
    }
}