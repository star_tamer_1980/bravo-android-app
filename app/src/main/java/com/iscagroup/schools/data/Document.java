package com.iscagroup.schools.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raed Saeed on 2/19/2019.
 */

public class Document extends BaseData implements Parcelable {

    public static final Creator<Document> CREATOR = new Creator<Document>() {
        @Override
        public Document createFromParcel(Parcel in) {
            return new Document(in);
        }

        @Override
        public Document[] newArray(int size) {
            return new Document[size];
        }
    };
    @SerializedName("fileExt")
    @Expose
    private String fileExt;
    @SerializedName("isImage")
    @Expose
    private boolean isImage;
    @SerializedName("imageName")
    @Expose
    private String name;
    @SerializedName("path")
    @Expose
    private String path;
    private boolean isUploaded;
    private int resourceId;

    public Document() {

    }

    public Document(String path, String fileName, boolean isImage) {
        this.isImage = isImage;
        this.name = fileName;
        this.path = path;
    }

    private Document(Parcel in) {
        super(in);
        fileExt = in.readString();
        isImage = in.readByte() != 0;
        name = in.readString();
        path = in.readString();
        isUploaded = in.readByte() != 0;
        resourceId = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(fileExt);
        dest.writeByte((byte) (isImage ? 1 : 0));
        dest.writeString(name);
        dest.writeString(path);
        dest.writeByte((byte) (isUploaded ? 1 : 0));
        dest.writeInt(resourceId);
    }

    public String getFileExt() {
        return fileExt;
    }

    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }

    public boolean isImage() {
        return isImage;
    }

    public void setImage(boolean image) {
        isImage = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setUploaded(boolean uploaded) {
        isUploaded = uploaded;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourId) {
        this.resourceId = resourId;
    }
}

