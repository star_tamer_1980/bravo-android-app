package com.iscagroup.schools.data.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raed Saeed on 8/19/2019.
 */

@SuppressWarnings("unused")
public class LocationResponse {
    @SerializedName("accuracy")
    @Expose
    private Double accuracy;
    @SerializedName("altitude")
    @Expose
    private Double altitude;
    @SerializedName("bearing")
    @Expose
    private Long bearing;
    @SerializedName("complete")
    @Expose
    private Boolean complete;
    @SerializedName("elapsedRealtimeNanos")
    @Expose
    private Long elapsedRealtimeNanos;
    @SerializedName("extras")
    @Expose
    private Extras extras;
    @SerializedName("fromMockProvider")
    @Expose
    private Boolean fromMockProvider;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("speed")
    @Expose
    private Long speed;
    @SerializedName("time")
    @Expose
    private Long time;

    public LocationResponse() {

    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Long getBearing() {
        return bearing;
    }

    public void setBearing(Long bearing) {
        this.bearing = bearing;
    }

    public Boolean getComplete() {
        return complete;
    }

    public void setComplete(Boolean complete) {
        this.complete = complete;
    }

    public Long getElapsedRealtimeNanos() {
        return elapsedRealtimeNanos;
    }

    public void setElapsedRealtimeNanos(Long elapsedRealtimeNanos) {
        this.elapsedRealtimeNanos = elapsedRealtimeNanos;
    }

    public Extras getExtras() {
        return extras;
    }

    public void setExtras(Extras extras) {
        this.extras = extras;
    }

    public Boolean getFromMockProvider() {
        return fromMockProvider;
    }

    public void setFromMockProvider(Boolean fromMockProvider) {
        this.fromMockProvider = fromMockProvider;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

}