package com.iscagroup.schools.data.driver;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

/**
 * Created by Raed Saeed on 8/21/2019.
 */


public class Driver extends BaseData implements Parcelable {
    public static final Creator<Driver> CREATOR = new Creator<Driver>() {
        @Override
        public Driver createFromParcel(Parcel in) {
            return new Driver(in);
        }

        @Override
        public Driver[] newArray(int size) {
            return new Driver[size];
        }
    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("c_code")
    @Expose
    private String cCode;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("bus_no")
    @Expose
    private String busNo;
    @SerializedName("bus_route")
    @Expose
    private String busRoute;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("firebase_id")
    @Expose
    private String firebaseId;

    private Driver(Parcel in) {
        super(in);
        id = in.readString();
        driverName = in.readString();
        cCode = in.readString();
        mobile = in.readString();
        busNo = in.readString();
        busRoute = in.readString();
        schoolId = in.readString();
        active = in.readString();
        firebaseId = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCCode() {
        return cCode;
    }

    public void setCCode(String cCode) {
        this.cCode = cCode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getBusNo() {
        return busNo;
    }

    public void setBusNo(String busNo) {
        this.busNo = busNo;
    }

    public String getBusRoute() {
        return busRoute;
    }

    public void setBusRoute(String busRoute) {
        this.busRoute = busRoute;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getFirebaseId() {
        return firebaseId;
    }

    public void setFirebaseId(String firebaseId) {
        this.firebaseId = firebaseId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(id);
        dest.writeString(driverName);
        dest.writeString(cCode);
        dest.writeString(mobile);
        dest.writeString(busNo);
        dest.writeString(busRoute);
        dest.writeString(schoolId);
        dest.writeString(active);
        dest.writeString(firebaseId);
    }

    @NonNull
    @Override
    public String toString() {
        return "Driver{" +
                "id='" + id + '\'' +
                ", driverName='" + driverName + '\'' +
                ", cCode='" + cCode + '\'' +
                ", mobile='" + mobile + '\'' +
                ", busNo='" + busNo + '\'' +
                ", busRoute='" + busRoute + '\'' +
                ", schoolId='" + schoolId + '\'' +
                ", active='" + active + '\'' +
                '}';
    }
}