
package com.iscagroup.schools.data.request;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

import java.util.List;
import java.util.Objects;

@SuppressWarnings("unused")
@Entity(tableName = "requests_table")
@TypeConverters(AttachTypeConverter.class)
public class Request extends BaseData implements Parcelable {

    public static final Creator<Request> CREATOR = new Creator<Request>() {
        @Override
        public Request createFromParcel(Parcel in) {
            return new Request(in);
        }

        @Override
        public Request[] newArray(int size) {
            return new Request[size];
        }
    };
    @SerializedName("id")
    @Expose
    @PrimaryKey
    @NonNull
    private String id = "";
    @SerializedName("request")
    @Expose
    private String request;
    @SerializedName("req_status")
    @Expose
    private String reqStatus;
    @SerializedName("reply")
    @Expose
    private String reply;
    @SerializedName("request_from")
    @Expose
    private String requestFrom;
    @SerializedName("teacher_user_id")
    @Expose
    private String teacherUserId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("create_date")
    @Expose
    private String createDate;
    @SerializedName("reply_date")
    @Expose
    private String replyDate;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("class_name")
    @Expose
    private String className;
    @SerializedName("student_photo")
    @Expose
    private String studentPhoto;
    @SerializedName("teacher_name")
    @Expose
    private String teacherName;
    @SerializedName("attach")
    @Expose
    private List<Attach> attach;

    @Ignore
    public Request() {

    }

    public Request(@NonNull String id, String request, String reqStatus, String reply,
                   String requestFrom, String teacherUserId, String schoolId, String createDate,
                   String replyDate, String studentId, String classId, String name,
                   String className, String studentPhoto, String teacherName, List<Attach> attach) {
        this.id = id;
        this.request = request;
        this.reqStatus = reqStatus;
        this.reply = reply;
        this.requestFrom = requestFrom;
        this.teacherUserId = teacherUserId;
        this.schoolId = schoolId;
        this.createDate = createDate;
        this.replyDate = replyDate;
        this.studentId = studentId;
        this.classId = classId;
        this.name = name;
        this.className = className;
        this.teacherName = teacherName;
        this.studentPhoto = studentPhoto;
        this.attach = attach;
    }

    protected Request(Parcel in) {
        super(in);
        id = Objects.requireNonNull(in.readString());
        request = in.readString();
        reqStatus = in.readString();
        reply = in.readString();
        requestFrom = in.readString();
        teacherUserId = in.readString();
        schoolId = in.readString();
        createDate = in.readString();
        replyDate = in.readString();
        studentId = in.readString();
        classId = in.readString();
        name = in.readString();
        className = in.readString();
        studentPhoto = in.readString();
        teacherName = in.readString();
        attach = in.createTypedArrayList(Attach.CREATOR);
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getReqStatus() {
        return reqStatus;
    }

    public void setReqStatus(String reqStatus) {
        this.reqStatus = reqStatus;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getRequestFrom() {
        return requestFrom;
    }

    public void setRequestFrom(String requestFrom) {
        this.requestFrom = requestFrom;
    }

    public String getTeacherUserId() {
        return teacherUserId;
    }

    public void setTeacherUserId(String teacherUserId) {
        this.teacherUserId = teacherUserId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getReplyDate() {
        return replyDate;
    }

    public void setReplyDate(String replyDate) {
        this.replyDate = replyDate;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getStudentPhoto() {
        return studentPhoto;
    }

    public void setStudentPhoto(String studentPhoto) {
        this.studentPhoto = studentPhoto;
    }

    public List<Attach> getAttach() {
        return attach;
    }

    public void setAttach(List<Attach> attach) {
        this.attach = attach;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(id);
        dest.writeString(request);
        dest.writeString(reqStatus);
        dest.writeString(reply);
        dest.writeString(requestFrom);
        dest.writeString(teacherUserId);
        dest.writeString(schoolId);
        dest.writeString(createDate);
        dest.writeString(replyDate);
        dest.writeString(studentId);
        dest.writeString(classId);
        dest.writeString(name);
        dest.writeString(className);
        dest.writeString(studentPhoto);
        dest.writeString(teacherName);
        dest.writeTypedList(attach);
    }

    @NonNull
    @Override
    public String toString() {
        return "Request{" +
                "id='" + id + '\'' +
                ", request='" + request + '\'' +
                ", reqStatus='" + reqStatus + '\'' +
                ", reply='" + reply + '\'' +
                ", requestFrom='" + requestFrom + '\'' +
                ", teacherUserId='" + teacherUserId + '\'' +
                ", schoolId='" + schoolId + '\'' +
                ", createDate='" + createDate + '\'' +
                ", replyDate='" + replyDate + '\'' +
                ", studentId='" + studentId + '\'' +
                ", classId='" + classId + '\'' +
                ", name='" + name + '\'' +
                ", className='" + className + '\'' +
                ", teacherName='" + teacherName + '\'' +
                ", studentPhoto ='" + studentPhoto + '\'' +
                ", attach=" + attach +
                '}';
    }
}
