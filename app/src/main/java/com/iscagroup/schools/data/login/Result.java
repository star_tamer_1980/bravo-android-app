
package com.iscagroup.schools.data.login;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

import java.util.List;

@SuppressWarnings("unused")
@Entity(tableName = "login_result")
@TypeConverters(LoginResponseTypConverter.class)
public class Result extends BaseData implements Parcelable {

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private long resultId;

    @SerializedName("id")
    private String userId;

    @SerializedName("c_code")
    private String userCountryCode;

    @SerializedName("active")
    private String mActive;

    @SerializedName("added_on")
    private String mAddedOn;

    @SerializedName("home")
    private List<HomeModel> mHome;

    @SerializedName("mobile")
    private String userMobile;

    @SerializedName("name")
    private String userName;

    @SerializedName("students")
    private List<Student> mStudents;

    @SerializedName("user_type")
    private String mUserType;

    @SerializedName("schools")
    private List<School> mSchools;

    public Result(@NonNull String userId, String userCountryCode, String mActive, String mAddedOn, List<HomeModel> mHome, String userMobile, String userName, List<Student> mStudents, String mUserType, List<School> mSchools) {
        this.userId = userId;
        this.userCountryCode = userCountryCode;
        this.mActive = mActive;
        this.mAddedOn = mAddedOn;
        this.mHome = mHome;
        this.userMobile = userMobile;
        this.userName = userName;
        this.mStudents = mStudents;
        this.mUserType = mUserType;
        this.mSchools = mSchools;
    }

    private Result(Parcel in) {
        super(in);
        userId = in.readString();
        userCountryCode = in.readString();
        mActive = in.readString();
        mAddedOn = in.readString();
        mHome = in.createTypedArrayList(HomeModel.CREATOR);
        userMobile = in.readString();
        userName = in.readString();
        mStudents = in.createTypedArrayList(Student.CREATOR);
        mUserType = in.readString();
        mSchools = in.createTypedArrayList(School.CREATOR);
    }

    public long getResultId() {
        return resultId;
    }

    public void setResultId(long resultId) {
        this.resultId = resultId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getActive() {
        return mActive;
    }

    public void setActive(String active) {
        mActive = active;
    }

    public String getAddedOn() {
        return mAddedOn;
    }

    public void setAddedOn(String addedOn) {
        mAddedOn = addedOn;
    }

    public String getUserCountryCode() {
        return userCountryCode;
    }

    public void setUserCountryCode(String cCode) {
        userCountryCode = cCode;
    }

    public List<HomeModel> getHome() {
        return mHome;
    }

    public void setHome(List<HomeModel> home) {
        mHome = home;
    }

    public String getId() {
        return userId;
    }

    public void setId(String id) {
        userId = id;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String mobile) {
        userMobile = mobile;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String name) {
        userName = name;
    }

    public List<Student> getStudents() {
        return mStudents;
    }

    public void setStudents(List<Student> students) {
        mStudents = students;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

    public List<School> getSchools() {
        return mSchools;
    }

    public void setSchools(List<School> mSchools) {
        this.mSchools = mSchools;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(userId);
        dest.writeString(userCountryCode);
        dest.writeString(mActive);
        dest.writeString(mAddedOn);
        dest.writeTypedList(mHome);
        dest.writeString(userMobile);
        dest.writeString(userName);
        dest.writeTypedList(mStudents);
        dest.writeString(mUserType);
        dest.writeTypedList(mSchools);
    }

    @NonNull
    @Override
    public String toString() {
        return "Result{" +
                "userId='" + userId + '\'' +
                ", userCountryCode='" + userCountryCode + '\'' +
                ", mActive='" + mActive + '\'' +
                ", mAddedOn='" + mAddedOn + '\'' +
                ", mHome=" + mHome +
                ", userMobile='" + userMobile + '\'' +
                ", userName='" + userName + '\'' +
                ", mStudents=" + mStudents +
                ", mUserType='" + mUserType + '\'' +
                ", mSchools=" + mSchools +
                '}';
    }
}
