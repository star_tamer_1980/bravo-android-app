
package com.iscagroup.schools.data.request;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

@SuppressWarnings("unused")
public class Attach extends BaseData implements Parcelable {

    public static final Creator<Attach> CREATOR = new Creator<Attach>() {
        @Override
        public Attach createFromParcel(Parcel in) {
            return new Attach(in);
        }

        @Override
        public Attach[] newArray(int size) {
            return new Attach[size];
        }
    };
    @SerializedName("added_on")
    private String mAddedOn;
    @SerializedName("feature_id")
    private String mFeatureId;
    @SerializedName("feature_type_id")
    private String mFeatureTypeId;
    @SerializedName("file_name")
    private String mFileName;
    @SerializedName("file_path")
    private String mFilePath;
    @SerializedName("id")
    private String mId;
    @SerializedName("school_id")
    private String mSchoolId;
    @SerializedName("extension")
    @Expose
    private String extension;
    @SerializedName("parent_request")
    @Expose
    private String parentRequest;
    private boolean isImage;

    public Attach() {

    }

    public Attach(String mFileName, String mFilePath, boolean isImage) {
        this.mFilePath = mFilePath;
        this.mFileName = mFileName;
        this.isImage = isImage;
    }

    private Attach(Parcel in) {
        super(in);
        mAddedOn = in.readString();
        mFeatureId = in.readString();
        mFeatureTypeId = in.readString();
        mFileName = in.readString();
        mFilePath = in.readString();
        mId = in.readString();
        mSchoolId = in.readString();
        isImage = in.readByte() != 0;
        extension = in.readString();
        parentRequest = in.readString();
    }

    public String getAddedOn() {
        return mAddedOn;
    }

    public void setAddedOn(String addedOn) {
        mAddedOn = addedOn;
    }

    public String getFeatureId() {
        return mFeatureId;
    }

    public void setFeatureId(String featureId) {
        mFeatureId = featureId;
    }

    public String getFeatureTypeId() {
        return mFeatureTypeId;
    }

    public void setFeatureTypeId(String featureTypeId) {
        mFeatureTypeId = featureTypeId;
    }

    public String getFileName() {
        return mFileName;
    }

    public void setFileName(String fileName) {
        mFileName = fileName;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getSchoolId() {
        return mSchoolId;
    }

    public void setSchoolId(String schoolId) {
        mSchoolId = schoolId;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getParentRequest() {
        return parentRequest;
    }

    public void setParentRequest(String parentRequest) {
        this.parentRequest = parentRequest;
    }

    public boolean isImage() {
        return isImage;
    }

    public void setImage(boolean image) {
        isImage = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mAddedOn);
        dest.writeString(mFeatureId);
        dest.writeString(mFeatureTypeId);
        dest.writeString(mFileName);
        dest.writeString(mFilePath);
        dest.writeString(mId);
        dest.writeString(mSchoolId);
        dest.writeByte((byte) (isImage ? 1 : 0));
        dest.writeString(extension);
        dest.writeString(parentRequest);
    }

    @NonNull
    @Override
    public String toString() {
        return "Attach{" +
                "mAddedOn='" + mAddedOn + '\'' +
                ", mFeatureId='" + mFeatureId + '\'' +
                ", mFeatureTypeId='" + mFeatureTypeId + '\'' +
                ", mFileName='" + mFileName + '\'' +
                ", mFilePath='" + mFilePath + '\'' +
                ", mId='" + mId + '\'' +
                ", mSchoolId='" + mSchoolId + '\'' +
                ", extension='" + extension + '\'' +
                ", parentRequest='" + parentRequest + '\'' +
                ", isImage=" + isImage +
                '}';
    }
}
