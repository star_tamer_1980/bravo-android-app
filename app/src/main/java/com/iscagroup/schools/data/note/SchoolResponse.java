package com.iscagroup.schools.data.note;

/**
 * Created by Raed Saeed on 7/21/2019.
 */

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SchoolResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("stages")
    @Expose
    private List<Stage> stages = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Stage> getStages() {
        return stages;
    }

    public void setStages(List<Stage> stages) {
        this.stages = stages;
    }

    @NonNull
    @Override
    public String toString() {
        return "SchoolResponse{" +
                "status=" + status +
                ", msg='" + msg + '\'' +
                ", stages=" + stages +
                '}';
    }
}