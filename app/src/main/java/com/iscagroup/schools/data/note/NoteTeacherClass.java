package com.iscagroup.schools.data.note;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iscagroup.schools.data.BaseData;

/**
 * Created by Raed Saeed on 7/24/2019.
 */
public class NoteTeacherClass extends BaseData {
    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("class_name")
    @Expose
    private String className;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @NonNull
    @Override
    public String toString() {
        return "NoteTeacherClass{" +
                "classId='" + classId + '\'' +
                ", className='" + className + '\'' +
                '}';
    }
}