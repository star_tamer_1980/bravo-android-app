package com.iscagroup.schools.request.reply;

import com.iscagroup.schools.base.BaseContract;

/**
 * Created by Raed Saeed on 7/9/2019.
 */
public interface ReplyRequestContract {
    interface ReplyRequestView extends BaseContract.BaseView {
        void onSuccessfulReply();
    }

    interface ReplyRequestPresenter {
        void reply(String requestId, String reply);

        void replySchool(String requestId, String reply);
    }
}
