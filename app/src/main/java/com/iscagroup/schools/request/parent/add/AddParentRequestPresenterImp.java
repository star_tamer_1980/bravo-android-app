package com.iscagroup.schools.request.parent.add;

import com.iscagroup.schools.base.BasePresenter;
import com.iscagroup.schools.model.remote.RequestService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Raed Saeed on 7/7/2019.
 */
public class AddParentRequestPresenterImp extends BasePresenter<AddParentRequestContract.AddParentRequestView> implements AddParentRequestContract.AddParentRequestPresenter {
    @SuppressWarnings("unused")
    private static final String TAG = "AddParentRequestPresent";
    private RequestService requestService;

    AddParentRequestPresenterImp() {

    }

    @Override
    protected void onCreate() {
        requestService = RequestService.getInstance();
    }

    @Override
    public void getTeachers(String classId) {
        if (getView().isConnected()) {
            getCompositeDisposable().add(requestService.getTeachers(classId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(teacherResponse -> {
                        getView().showTeachersData(teacherResponse.getTeachers());
                    }, error -> getView().showFailureMessage(error.getMessage())));
        }
    }

    @Override
    public void addRequest(String studentId, String classId, String teacherId, String schoolId, String requestFor, String request) {
        if (getView().isConnected()) {
            getView().showProgressBar();
            getCompositeDisposable().add(requestService.addRequest(studentId, classId, teacherId, schoolId, requestFor, request)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(addResponse -> getView().onSuccessfulAddRequest(addResponse), error -> {
                        getView().hideProgressBar();
                        getView().onFailureAddRequest(error.getMessage());
                    }));
        }
    }
}
