package com.iscagroup.schools.request.school;

import android.os.Bundle;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseFragment;
import com.iscagroup.schools.request.teacher.TeacherRequestListActivity;

import java.util.ArrayList;
import java.util.List;

public class SchoolRequestListActivity extends TeacherRequestListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected List<BaseFragment> getFragments() {
        List<BaseFragment> fragments = new ArrayList<>();
        SchoolOpenRequestFragment openFrag = new SchoolOpenRequestFragment();
        openFrag.setName(getString(R.string.open));
        fragments.add(openFrag);

        SchoolClosedRequestFragment closedFrag = new SchoolClosedRequestFragment();
        closedFrag.setName(getString(R.string.closed));
        fragments.add(closedFrag);
        return fragments;
    }
}
