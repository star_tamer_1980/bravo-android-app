package com.iscagroup.schools.request;

import com.iscagroup.schools.base.BaseContract;
import com.iscagroup.schools.data.request.Request;

import java.util.List;

/**
 * Created by Raed Saeed on 7/4/2019.
 */
public interface RequestContract {
    interface RequestView extends BaseContract.BaseView {
        void getRequests();

        void showRequestList(List<Request> requestList);
    }

    interface RequestPresenter {
        void getRequests(String studentId, String status, String itemCount, String limit);

        void getTeacherRequests(String userId, String schoolId, String status, String itemCount, String limit);

        void getSchoolRequests(String schoolId, String status, String itemCount, String limit);
    }
}
