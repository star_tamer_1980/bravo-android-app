package com.iscagroup.schools.request.school;

import android.content.Intent;

import com.iscagroup.schools.data.request.Request;
import com.iscagroup.schools.request.reply.ReplyRequestActivity;
import com.iscagroup.schools.request.teacher.TeacherOpenRequestFragment;
import com.iscagroup.schools.utils.StringConstants;

/**
 * Created by Raed Saeed on 7/14/2019.
 */
public class SchoolOpenRequestFragment extends TeacherOpenRequestFragment {
    @Override
    public void getRequests() {
        // set isRefreshed to true to enable setRefreshing method
        isRefreshed = true;

        // set isLastPage to false to enable loadMoreItems method
        isLastPage = false;

        requestPresenterImp.getSchoolRequests(schoolId, REQUEST_STATUS, "0", "10");
    }

    @Override
    protected void loadMoreItems() {
        loadMore = true;
        requestPresenterImp.getSchoolRequests(schoolId, REQUEST_STATUS, String.valueOf(requestAdapter.getItemCount()), "10");
    }

    @Override
    public void onRequestClick(int position, Request request) {
        Intent intent = new Intent(getContext(), ReplyRequestActivity.class);
        intent.putExtra(StringConstants.EXTRA_REQUEST_POSITION, position);
        intent.putExtra(StringConstants.EXTRA_REQUEST, request);
        intent.putExtra(StringConstants.EXTRA_REQUEST_TYPE, 2);
        startActivityForResult(intent, REPLY_PARENT_REQUEST);
    }
}
