package com.iscagroup.schools.request;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.iscagroup.schools.R;
import com.iscagroup.schools.adapter.FileAdapter;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.request.Attach;
import com.iscagroup.schools.data.request.Request;
import com.iscagroup.schools.utils.DateTimeUtil;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.utils.UserSharedPreferences;

import java.util.ArrayList;
import java.util.List;

public class RequestDetailActivity extends BaseActivity {

    private Request request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_detail);

        Toolbar toolbar = findViewById(R.id.tb_activity_request_detail_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        Intent intent = getIntent();
        if (intent != null) {
            request = intent.getParcelableExtra(StringConstants.EXTRA_REQUEST);
        }

        initToolbarViews(toolbar);
        initViews();
    }

    private void initToolbarViews(Toolbar toolbar) {
        String studentName = request.getName();
        TextView tvStudentName = toolbar.findViewById(R.id.tv_toolbar_layout_primary_name);
        tvStudentName.setText(studentName);

        String studentClass = request.getClassName();
        TextView tvStudentClass = toolbar.findViewById(R.id.tv_toolbar_secondary_name);
        tvStudentClass.setText(studentClass);

        ImageView ivStudentAvatar = toolbar.findViewById(R.id.civ_toolbar_layout_avatar);
        String kidAvatarPath = request.getStudentPhoto();
        Glide.with(toolbar)
                .load(kidAvatarPath)
                .into(ivStudentAvatar);
    }

    private void initViews() {
        TextView tvRequestDescription = findViewById(R.id.tv_activity_request_detail_body);
        tvRequestDescription.setText(request.getRequest());

        TextView tvRequestCreateDate = findViewById(R.id.tv_activity_request_detail_create_date);
        tvRequestCreateDate.setText(DateTimeUtil.getDateTimeString(request.getCreateDate()));

        RecyclerView rvRequestFiles = findViewById(R.id.rv_activity_request_detail_request_file_list);
        LinearLayoutManager requestLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        rvRequestFiles.setLayoutManager(requestLayoutManager);
        FileAdapter requestFileAdapter = new FileAdapter();
        rvRequestFiles.setAdapter(requestFileAdapter);
        requestFileAdapter.loadAttach(getRequestFiles());

        TextView tvRequestCloser = findViewById(R.id.tv_activity_request_detail_sender_name);
        tvRequestCloser.setText(request.getTeacherName());

        TextView tvRequestReply = findViewById(R.id.tv_activity_request_detail_reply_text);
        tvRequestReply.setText(request.getReply());
        tvRequestReply.setVisibility(request.getReply().trim().length() > 0 ? View.VISIBLE : View.GONE);

        TextView tvReplyCreateDate = findViewById(R.id.tv_activity_request_detail_reply_date);
        tvReplyCreateDate.setText(DateTimeUtil.getDateTimeString(request.getReplyDate()));

        ImageView ivSenderAvatar = findViewById(R.id.iv_activity_request_detail_sender_avatar);
        Glide.with(this)
                .load(UserSharedPreferences.getUserSchoolLogo(this))
                .into(ivSenderAvatar);

        RecyclerView rvReplyFiles = findViewById(R.id.rv_activity_request_detail_reply_file_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        rvReplyFiles.setLayoutManager(layoutManager);
        FileAdapter replyFileAdapter = new FileAdapter();
        rvReplyFiles.setAdapter(replyFileAdapter);
        replyFileAdapter.loadAttach(getReplyFiles());
    }

    private List<Attach> getRequestFiles() {
        List<Attach> requestFiles = new ArrayList<>();
        if (request.getAttach() != null && request.getAttach().size() != 0) {
            for (Attach attach : request.getAttach()) {
                if (attach.getParentRequest() == null || attach.getParentRequest().equalsIgnoreCase("1")) {
                    attach.setResourceId(R.layout.item_downloadable_file);
                    requestFiles.add(attach);
                }
            }
        }
        return requestFiles;
    }

    private List<Attach> getReplyFiles() {
        List<Attach> replyAttach = new ArrayList<>();
        if (request.getAttach() != null && request.getAttach().size() != 0) {
            for (Attach attach : request.getAttach()) {
                if (attach.getParentRequest() != null && attach.getParentRequest().equalsIgnoreCase("0")) {
                    attach.setResourceId(R.layout.item_downloadable_file);
                    replyAttach.add(attach);
                }
            }
        }
        return replyAttach;
    }
}
