package com.iscagroup.schools.request;


import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BasePresenter;
import com.iscagroup.schools.data.request.RequestResponse;
import com.iscagroup.schools.model.local.AppDatabase;
import com.iscagroup.schools.model.local.RequestDao;
import com.iscagroup.schools.model.remote.RequestService;

import java.net.UnknownHostException;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Raed Saeed on 7/4/2019.
 */
public class RequestPresenterImp extends BasePresenter<RequestContract.RequestView> implements RequestContract.RequestPresenter {
    @SuppressWarnings("unused")
    private static final String TAG = "RequestPresenterImp";

    private RequestService requestService;
    private RequestDao requestDao;

    public RequestPresenterImp() {

    }

    @Override
    protected void onCreate() {
        requestService = RequestService.getInstance();
        requestDao = AppDatabase.getInstance().getRequestDao();
    }


    @Override
    public void getRequests(String studentId, String status, String itemCount, String limit) {
        getView().showProgressBar();
        getView().startRefreshing();

        if (getView().isConnected()) {
            getCompositeDisposable().add(requestService.getParentRequest(studentId, status, itemCount, limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .retry(throwable -> throwable instanceof UnknownHostException)
                    .flatMapIterable(RequestResponse::getResult)
                    .map(item -> {
                        if (item.getReqStatus().equalsIgnoreCase("1")) {
                            item.setResourceId(R.layout.item_closed_request);
                            return item;
                        }
                        item.setResourceId(R.layout.item_parent_open_request);
                        return item;
                    })
                    .toList()
                    .subscribe(requestResponse -> {

                        getView().stopRefreshing();
                        getView().showRequestList(requestResponse);

                        getCompositeDisposable().add(requestDao.saveRequests(requestResponse)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(insertResponse -> {
                                }, error -> {
                                    getView().showFailureMessage(error.getMessage());
                                }));

                    }, error -> {
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                    }));
        } else {
            getCompositeDisposable().add(requestDao.getRequests(studentId, status, Integer.valueOf(itemCount))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .toFlowable()
                    .flatMapIterable(requestList -> requestList)
                    .map(request -> {
                        if (request.getReqStatus().equalsIgnoreCase("1")) {
                            request.setResourceId(R.layout.item_closed_request);
                            return request;
                        }
                        request.setResourceId(R.layout.item_parent_open_request);
                        return request;
                    })
                    .toList()
                    .subscribe(requestList -> {
                                getView().stopRefreshing();
                                getView().showRequestList(requestList);
                            },
                            error -> {
                                getView().stopRefreshing();
                                getView().showFailureMessage(error.getMessage());
                            }));
        }
    }

    @Override
    public void getTeacherRequests(String userId, String schoolId, String status, String itemCount, String limit) {
        getView().showProgressBar();
        getView().startRefreshing();

        if (getView().isConnected()) {
            getCompositeDisposable().add(requestService.getTeacherRequest(userId, schoolId, status, itemCount, limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .retry(throwable -> throwable instanceof UnknownHostException)
                    .flatMapIterable(RequestResponse::getResult)
                    .map(item -> {
                        if (item.getReqStatus().equalsIgnoreCase("1")) {
                            item.setResourceId(R.layout.item_closed_request);
                            return item;
                        }
                        item.setResourceId(R.layout.item_teacher_request);
                        return item;
                    })
                    .toList()
                    .subscribe(requestResponse -> {

                        getView().stopRefreshing();
                        getView().showRequestList(requestResponse);

                        getCompositeDisposable().add(requestDao.saveRequests(requestResponse)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(insertResponse -> {
                                }, error -> {
                                    getView().showFailureMessage(error.getMessage());
                                }));

                    }, error -> {
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                    }));
        } else {
            getCompositeDisposable().add(requestDao.getTeacherRequest(userId, schoolId, status, Integer.valueOf(itemCount))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .toFlowable()
                    .flatMapIterable(requestList -> requestList)
                    .map(request -> {
                        if (request.getReqStatus().equalsIgnoreCase("1")) {
                            request.setResourceId(R.layout.item_closed_request);
                            return request;
                        }
                        request.setResourceId(R.layout.item_teacher_request);
                        return request;
                    })
                    .toList()
                    .subscribe(requestList -> {
                                getView().stopRefreshing();
                                getView().showRequestList(requestList);
                            },
                            error -> {
                                getView().stopRefreshing();
                                getView().showFailureMessage(error.getMessage());
                            }));
        }
    }

    @Override
    public void getSchoolRequests(String schoolId, String status, String itemCount, String limit) {
        getView().showProgressBar();
        getView().startRefreshing();

        if (getView().isConnected()) {
            getCompositeDisposable().add(requestService.getSchoolRequest(schoolId, status, itemCount, limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .retry(throwable -> throwable instanceof UnknownHostException)
                    .flatMapIterable(RequestResponse::getResult)
                    .map(item -> {
                        if (item.getReqStatus().equalsIgnoreCase("1")) {
                            item.setResourceId(R.layout.item_closed_request);
                            return item;
                        }
                        item.setResourceId(R.layout.item_school_request);
                        return item;
                    })
                    .toList()
                    .subscribe(requestResponse -> {

                        getView().stopRefreshing();
                        getView().showRequestList(requestResponse);

                        getCompositeDisposable().add(requestDao.saveRequests(requestResponse)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(insertResponse -> {
                                }, error -> {
                                    getView().showFailureMessage(error.getMessage());
                                }));

                    }, error -> {
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                    }));
        } else {
            getCompositeDisposable().add(requestDao.getSchoolRequest(schoolId, status, Integer.valueOf(itemCount))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .toFlowable()
                    .flatMapIterable(requestList -> requestList)
                    .map(request -> {
                        if (request.getReqStatus().equalsIgnoreCase("1")) {
                            request.setResourceId(R.layout.item_closed_request);
                            return request;
                        }
                        request.setResourceId(R.layout.item_school_request);
                        return request;
                    })
                    .toList()
                    .subscribe(requestList -> {
                                getView().stopRefreshing();
                                getView().showRequestList(requestList);
                            },
                            error -> {
                                getView().stopRefreshing();
                                getView().showFailureMessage(error.getMessage());
                            }));
        }
    }
}
