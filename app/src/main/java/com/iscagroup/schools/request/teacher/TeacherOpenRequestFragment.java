package com.iscagroup.schools.request.teacher;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseFragment;
import com.iscagroup.schools.data.request.Request;
import com.iscagroup.schools.request.RequestAdapter;
import com.iscagroup.schools.request.RequestContract;
import com.iscagroup.schools.request.RequestPresenterImp;
import com.iscagroup.schools.request.reply.ReplyRequestActivity;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.utils.UserSharedPreferences;

import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Raed Saeed on 7/4/2019.
 */
public class TeacherOpenRequestFragment extends BaseFragment implements RequestContract.RequestView, RequestAdapter.OnRequestClickListener {
    /**
     * The request status 1 for closed request and 0 for open request
     */
    protected static final String REQUEST_STATUS = "0";
    @SuppressWarnings("unused")
    private static final String TAG = "TeacherOpenRequest";
    protected static final int REPLY_PARENT_REQUEST = 383;
    protected RequestAdapter requestAdapter;
    protected RequestPresenterImp requestPresenterImp;
    /**
     * isLastPage boolean variable to check if the page has more items or not
     * if true we don't call loadMoreItems method
     * if false we call loadMoreItems method
     */
    protected boolean isLastPage = false;
    /**
     * isRefreshed boolean variable to check if the getRequest called or not for first time
     * getRequest called from onResume and onRefresh which is used to clear the adapter
     */
    protected boolean isRefreshed = false;
    /**
     * loadMore boolean variable to check loadMore or not
     * if loadMore is true the progress bar should be showing
     * if loadMore is false the progress bar should be not showing
     */
    protected boolean loadMore = false;
    protected String schoolId;
    private SwipeRefreshLayout refreshLayout;
    private ProgressBar pbRequestLoading;
    /**
     * loaded variable to check if the request loaded or not, means on successful response
     * wether data exist or not the loaded variable will be true to use in setRefreshing
     * if true setRefreshing will be called
     * if false setRefreshing won't be called
     */
    private boolean loaded = false;
    private String userId;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request, container, false);
        initViews(view);
        initVars();
        return view;
    }

    private void initViews(View view) {
        FloatingActionButton fabAddRequest = view.findViewById(R.id.fab_fragment_request_add_request);
        fabAddRequest.hide();

        requestAdapter = new RequestAdapter();
        requestAdapter.setOnRequestClickListener(this);

        RecyclerView rvRequestList = view.findViewById(R.id.rv_fragment_request_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvRequestList.setLayoutManager(layoutManager);
        rvRequestList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = layoutManager.getItemCount();
                int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                boolean endIsReached = lastVisibleItemPosition + 3 > totalItemCount;
                if (!isLastPage && !loadMore && endIsReached && totalItemCount > 0) {
                    loadMoreItems();
                }
            }
        });
        rvRequestList.setAdapter(requestAdapter);


        refreshLayout = view.findViewById(R.id.srl_fragment_request_refresh_layout);
        refreshLayout.setOnRefreshListener(this::getRequests);

        pbRequestLoading = view.findViewById(R.id.pb_fragment_request_loading);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!loaded) {
            getRequests();
        }
    }

    @Override
    public void startRefreshing() {
        if (loaded && !loadMore) refreshLayout.setRefreshing(true);
    }

    @Override
    public void stopRefreshing() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void hideProgressBar() {
        pbRequestLoading.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showProgressBar() {
        pbRequestLoading.setVisibility(View.VISIBLE);
    }

    private void initVars() {
        requestPresenterImp = new RequestPresenterImp();
        requestPresenterImp.attachView(this);
        requestPresenterImp.attachLifecycle(getLifecycle());
        getLifecycleRegistry().handleLifecycleEvent(Lifecycle.Event.ON_CREATE);

        userId = UserSharedPreferences.getUserId(getContext());
//        userId = "4";
        schoolId = UserSharedPreferences.getUserSchoolId(getContext());
    }

    @Override
    public void getRequests() {
        // set isRefreshed to true to enable setRefreshing method
        isRefreshed = true;

        // set isLastPage to false to enable loadMoreItems method
        isLastPage = false;

        requestPresenterImp.getTeacherRequests(userId, schoolId, REQUEST_STATUS, "0", "10");
    }

    protected void loadMoreItems() {
        loadMore = true;
        requestPresenterImp.getTeacherRequests(userId, schoolId, REQUEST_STATUS, String.valueOf(requestAdapter.getItemCount()), "10");
    }

    @Override
    public void showRequestList(List<Request> requestList) {
        loaded = true;
        loadMore = false;

        if (isRefreshed) {
            requestAdapter.clearList();
            isRefreshed = false;
        }

        if (requestList.size() < 10) {
            isLastPage = true;
            hideProgressBar();
        }
        requestAdapter.addList(requestList);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REPLY_PARENT_REQUEST && resultCode == RESULT_OK) {
            if (data != null) {
                int positionToRemove = data.getIntExtra(StringConstants.EXTRA_REQUEST_POSITION, -1);
                if (positionToRemove != -1) {
                    requestAdapter.removeItem(positionToRemove);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestClick(int position, Request request) {
        Intent intent = new Intent(getContext(), ReplyRequestActivity.class);
        intent.putExtra(StringConstants.EXTRA_REQUEST_POSITION, position);
        intent.putExtra(StringConstants.EXTRA_REQUEST, request);
        intent.putExtra(StringConstants.EXTRA_REQUEST_TYPE, 1);
        startActivityForResult(intent, REPLY_PARENT_REQUEST);
    }
}
