package com.iscagroup.schools.request.reply;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.iscagroup.schools.R;
import com.iscagroup.schools.adapter.FileAdapter;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.request.Attach;
import com.iscagroup.schools.data.request.Request;
import com.iscagroup.schools.dialog.BottomFilePickerDialog;
import com.iscagroup.schools.utils.DateTimeUtil;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.utils.UserSharedPreferences;

import java.util.ArrayList;
import java.util.List;

import static com.iscagroup.schools.utils.StringConstants.EXTRA_REQUEST_POSITION;

public class ReplyRequestActivity extends BaseActivity implements ReplyRequestContract.ReplyRequestView {
    @SuppressWarnings("unused")
    private static final String TAG = "ReplyRequestActivity";

    private static final String featureType = "1";

    private TextView tvRequestDescription;

    private EditText etRequestReply;

    private Request request;

    private FileAdapter receivedFileAdapter;

    private ReplyRequestPresenterImp replyRequestPresenterImp;

    /**
     * The received request position to reply
     * if the request replied, we notify the adapter to remove the request from list according to it's position
     */
    private int requestPosition;

    private int requestType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply_request);

        Toolbar toolbar = findViewById(R.id.tb_activity_reply_request_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        Intent intent = getIntent();
        if (intent != null) {
            request = intent.getParcelableExtra(StringConstants.EXTRA_REQUEST);
            requestPosition = intent.getIntExtra(EXTRA_REQUEST_POSITION, requestPosition);
            requestType = intent.getIntExtra(StringConstants.EXTRA_REQUEST_TYPE, 1);
        }

        initToolBarViews(toolbar);
        initViews();
        initVars();
        populateUi(request);
    }

    private void initToolBarViews(Toolbar toolbar) {
        String studentName = request.getName();
        TextView tvStudentName = toolbar.findViewById(R.id.tv_toolbar_layout_primary_name);
        tvStudentName.setText(studentName);

        String studentClass = request.getClassName();
        TextView tvStudentClass = toolbar.findViewById(R.id.tv_toolbar_secondary_name);
        tvStudentClass.setText(studentClass);

        ImageView ivStudentAvatar = toolbar.findViewById(R.id.civ_toolbar_layout_avatar);
        String kidAvatarPath = request.getStudentPhoto();
        Glide.with(toolbar)
                .load(kidAvatarPath)
                .into(ivStudentAvatar);
    }

    private void initVars() {
        replyRequestPresenterImp = new ReplyRequestPresenterImp();
        replyRequestPresenterImp.attachLifecycle(getLifecycle());
        replyRequestPresenterImp.attachView(this);
    }

    private void initViews() {
        tvRequestDescription = findViewById(R.id.tv_activity_reply_request_body);

        receivedFileAdapter = new FileAdapter();
        RecyclerView rvReceivedAttachmentList = findViewById(R.id.rv_activity_reply_request_received_attachment_list);
        rvReceivedAttachmentList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvReceivedAttachmentList.setAdapter(receivedFileAdapter);

        RecyclerView rvSentAttachmentList = findViewById(R.id.rv_reply_request_file_list);
        rvSentAttachmentList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvSentAttachmentList.setAdapter(getFileAdapter());

        ImageView ivReplyWithAttach = findViewById(R.id.iv_activity_reply_request_attach_file);
        ivReplyWithAttach.setOnClickListener(v -> {
            BottomFilePickerDialog filePickerDialog = new BottomFilePickerDialog();
            filePickerDialog.setClickListener(view -> {
                switch (view.getId()) {
                    case R.id.iv_dialog_file_picker_pick_image:
                    case R.id.tv_dialog_file_picker_image_label:
                        startImagePicker(10);
                        break;

                    case R.id.iv_dialog_file_picker_pick_document:
                    case R.id.tv_dialog_file_picker_document_label:
                        startDocumentPicker();
                        break;
                }
            });
            filePickerDialog.show(getSupportFragmentManager(), null);
        });

        Button btnReplyRequest = findViewById(R.id.btn_activity_reply_request_reply);
        btnReplyRequest.setOnClickListener(v -> {
            if (requestType == 1) {
                replyRequestPresenterImp.reply(request.getId(), etRequestReply.getText().toString().trim());
            } else {
                replyRequestPresenterImp.replySchool(request.getId(), etRequestReply.getText().toString().trim());
            }
        });

        etRequestReply = findViewById(R.id.et_activity_reply_request_description);
        etRequestReply.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    btnReplyRequest.setEnabled(true);
                } else {
                    btnReplyRequest.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void populateUi(Request request) {
        tvRequestDescription.setText(request.getRequest());

        TextView tvRequestCreateDate = findViewById(R.id.tv_activity_reply_request_created_date);
        tvRequestCreateDate.setText(DateTimeUtil.getTimeString(request.getCreateDate()));
        receivedFileAdapter.loadAttach(addAttachResource(request.getAttach()));
    }

    private List<Attach> addAttachResource(List<Attach> attachList) {
        List<Attach> modifiedList = new ArrayList<>();
        for (Attach attach : attachList) {
            attach.setResourceId(R.layout.item_downloadable_file);
            modifiedList.add(attach);
        }
        return modifiedList;
    }

    @Override
    public void showProgressBar() {
        showProgressDialog(getString(R.string.submitting_your_request));
    }

    @Override
    public void hideProgressBar() {
        hideProgressDialog();
    }

    @Override
    public void onSuccessfulReply() {
        if (getFileAdapter().getItemCount() != 0) {
            String schoolId = UserSharedPreferences.getUserSchoolId(this);
            getAttachmentPresenter().attachFiles(featureType, request.getId(), schoolId, getFileAdapter().getList());
        } else {
            setActivityResult();
        }
    }

    @Override
    public void onUploadDone() {
        setActivityResult();
    }

    private void setActivityResult() {
        hideProgressBar();
        Intent intent = new Intent();
        intent.putExtra(EXTRA_REQUEST_POSITION, requestPosition);
        setResult(RESULT_OK, intent);
        finishActivityWithTransition();
    }
}
