package com.iscagroup.schools.request.parent.add;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.iscagroup.schools.R;
import com.iscagroup.schools.adapter.CustomSpinnerAdapter;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.request.Attach;
import com.iscagroup.schools.data.request.Request;
import com.iscagroup.schools.data.teacher.Teacher;
import com.iscagroup.schools.dialog.BottomFilePickerDialog;
import com.iscagroup.schools.global.AttachmentContract;
import com.iscagroup.schools.global.AttachmentPresenter;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.utils.UserSharedPreferences;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AddParentRequestActivity extends BaseActivity implements AddParentRequestContract.AddParentRequestView, AttachmentContract.AttachmentView {
    @SuppressWarnings("unused")
    private static final String TAG = "AddParentRequest";
    private static final String featureType = "1";
    private AddParentRequestPresenterImp addParentRequestPresenterImp;
    private CustomSpinnerAdapter spinnerAdapter;

    private EditText etRequestBody;
    private AppCompatSpinner appCompatSpinner;

    // Request Body (Description)
    private String requestBody;

    // The current student id
    private String studentId;

    // The current student class id
    private String classId;

    // The current student school id
    private String schoolId;

    // Teacher id that receive the request
    private String teacherId;

    // The type of receiver (Teacher = 1 or Management = 2)
    private String requestFor = "1";
    // Attachment upload presenter
    private AttachmentPresenter attachmentPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_parent_request);

        Toolbar toolbar = findViewById(R.id.tb_activity_add_parent_request_toolbar);
        setSupportActionBar(toolbar);
        initToolBarViews(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        ImageView imageView = findViewById(R.id.iv_activity_add_parent_request_attach_file);
        imageView.setOnClickListener(v -> {
            BottomFilePickerDialog filePickerDialog = new BottomFilePickerDialog();
            filePickerDialog.setActivity(true);
            filePickerDialog.setClickListener(view -> {
                switch (view.getId()) {
                    case R.id.iv_dialog_file_picker_pick_image:
                        startImagePicker(5);
                        break;
                    case R.id.iv_dialog_file_picker_pick_document:
                        startDocumentPicker();
                        break;
                }
            });
            filePickerDialog.show(getSupportFragmentManager(), null);
        });


        RecyclerView rvFilesToUploadList = findViewById(R.id.rv_activity_parent_request_file_list);
        rvFilesToUploadList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvFilesToUploadList.setAdapter(getFileAdapter());

        initVars();
        initViews();
        initTeacherSelector();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getTeachers();
    }

    private void initViews() {
        RadioGroup radioGroup = findViewById(R.id.rg_activity_add_parent_request_management_group);
        radioGroup.check(R.id.rb_teacher);
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rb_teacher) {
                appCompatSpinner.setEnabled(true);
                requestFor = "1";
            } else {
                appCompatSpinner.setSelection(0);
                appCompatSpinner.setEnabled(false);
                requestFor = "2";
                teacherId = "";
            }
        });

        Button btnAddRequest = findViewById(R.id.btn_activity_add_parent_request_send);
        btnAddRequest.setOnClickListener(v -> addParentRequest());

        etRequestBody = findViewById(R.id.et_activity_parent_request_description);
        etRequestBody.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    btnAddRequest.setEnabled(true);
                } else {
                    btnAddRequest.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initToolBarViews(Toolbar toolbar) {
        String studentName = UserSharedPreferences.getUserName(this);
        TextView tvStudentName = toolbar.findViewById(R.id.tv_toolbar_layout_primary_name);
        tvStudentName.setText(studentName);

        String studentClass = UserSharedPreferences.getUserClassName(this);
        TextView tvStudentClass = toolbar.findViewById(R.id.tv_toolbar_secondary_name);
        tvStudentClass.setText(studentClass);

        ImageView ivStudentAvatar = toolbar.findViewById(R.id.civ_toolbar_layout_avatar);
        String kidAvatarPath = UserSharedPreferences.getUserPhoto(this);
        Glide.with(toolbar)
                .load(kidAvatarPath)
                .into(ivStudentAvatar);
    }

    private void initTeacherSelector() {
        appCompatSpinner = findViewById(R.id.spinner_activity_add_parent_request_teachers);
        spinnerAdapter = new CustomSpinnerAdapter();
        appCompatSpinner.setAdapter(spinnerAdapter);
        appCompatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    Teacher teacher = (Teacher) spinnerAdapter.getItem(position - 1);
                    teacherId = teacher.getTeacherUserId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initVars() {
        attachmentPresenter = new AttachmentPresenter();
        attachmentPresenter.attachLifecycle(getLifecycle());
        attachmentPresenter.attachView(this);

        addParentRequestPresenterImp = new AddParentRequestPresenterImp();
        addParentRequestPresenterImp.attachLifecycle(getLifecycle());
        addParentRequestPresenterImp.attachView(this);

        studentId = UserSharedPreferences.getUserStudentId(this);
        classId = UserSharedPreferences.getUserClassId(this);
        schoolId = UserSharedPreferences.getUserSchoolId(this);
    }

    @Override
    public void addParentRequest() {
        requestBody = etRequestBody.getText().toString().trim();

        if (!TextUtils.isEmpty(requestBody)) {
            addParentRequestPresenterImp.addRequest(studentId, classId, teacherId, schoolId, requestFor, requestBody);
        }
    }

    /**
     * Get available teacher for this class when the activity is created
     */
    @Override
    public void getTeachers() {
        addParentRequestPresenterImp.getTeachers(classId);
    }

    @Override
    public void showTeachersData(List<Teacher> teachers) {
        spinnerAdapter.setTeachers(teachers);
    }

    @Override
    public void onSuccessfulAddRequest(JsonObject jsonObject) {
        Log.w(TAG, "onSuccessfulAddRequest: " + jsonObject.toString());
        JsonElement element = jsonObject.get("id");
        int id = element.getAsInt();
        attachmentPresenter.attachFiles(featureType, String.valueOf(id), schoolId, getFileAdapter().getList(), true);
    }

    @Override
    public void hideProgressBar() {
        hideProgressDialog();
    }

    @Override
    public void showProgressBar() {
        showProgressDialog(getString(R.string.submitting_your_request));
    }

    /**
     * Create new Parent Request
     *
     * @return {@link Request} the created new Parent Request to OpenParentRequestFragment
     */
    private Request buildRequest() {
        Request request = new Request();
        request.setId("0");
        request.setStudentId(studentId);
        request.setClassId(classId);
        request.setTeacherUserId(teacherId);
        request.setSchoolId(schoolId);
        request.setRequest(requestBody);
        request.setReply("");
        request.setReplyDate("");
        request.setResourceId(R.layout.item_parent_open_request);
        request.setName(UserSharedPreferences.getUserName(this));
        request.setStudentPhoto(UserSharedPreferences.getUserPhoto(this));
        request.setClassName(UserSharedPreferences.getUserClassName(this));
        request.setAttach(buildAttachList(getFileAdapter().getList()));
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        request.setCreateDate(dateFormat.format(date));
        return request;
    }

    private List<Attach> buildAttachList(List<BaseData> list) {
        List<Attach> attaches = new ArrayList<>();
        for (BaseData baseData : list) {
            Attach attach = (Attach) baseData;
            attach.setParentRequest("1");
            attaches.add(attach);
        }
        return attaches;
    }

    @Override
    public void onFailureAddRequest(String message) {
        showFailureMessage(message);
    }

    @Override
    public void onUploadDone() {
        hideProgressBar();
        Intent intent = new Intent();
        intent.putExtra(StringConstants.EXTRA_SENDED_REQUEST, buildRequest());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onUploadFailed() {
        hideProgressBar();
        showToast(R.string.upload_failed);
    }
}
