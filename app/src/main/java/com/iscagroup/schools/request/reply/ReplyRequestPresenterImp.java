package com.iscagroup.schools.request.reply;

import com.iscagroup.schools.base.BasePresenter;
import com.iscagroup.schools.model.local.AppDatabase;
import com.iscagroup.schools.model.local.RequestDao;
import com.iscagroup.schools.model.remote.RequestService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Raed Saeed on 7/9/2019.
 */
public class ReplyRequestPresenterImp extends BasePresenter<ReplyRequestContract.ReplyRequestView> implements ReplyRequestContract.ReplyRequestPresenter {
    @SuppressWarnings("unused")
    private static final String TAG = "ReplyRequestPresenterIm";

    private RequestService requestService;
    private RequestDao requestDao;

    @Override
    protected void onCreate() {
        requestService = RequestService.getInstance();
        requestDao = AppDatabase.getInstance().getRequestDao();
    }

    @Override
    public void reply(String requestId, String reply) {
        if (getView().isConnected()) {
            getView().showProgressBar();
            getCompositeDisposable().add(requestService.replyRequestTeacher(requestId, reply)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(jsonResponse -> {
                                getCompositeDisposable().add(requestDao.deleteRequest(requestId)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(() -> {
                                        }, Throwable::printStackTrace));
                                getView().onSuccessfulReply();

                            }
                            , error -> {
                                getView().hideProgressBar();
                                error.printStackTrace();
                                getView().showFailureMessage(error.getMessage());
                            }));
        } else {
            getView().showFailureMessage("");
        }
    }

    @Override
    public void replySchool(String requestId, String reply) {
        if (getView().isConnected()) {
            getView().showProgressBar();
            getCompositeDisposable().add(requestService.replyRequestSchool(requestId, reply)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(jsonResponse -> {
                                getCompositeDisposable().add(requestDao.deleteRequest(requestId)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(() -> {
                                        }, Throwable::printStackTrace));
                                getView().onSuccessfulReply();

                            }
                            , error -> {
                                getView().hideProgressBar();
                                error.printStackTrace();
                                getView().showFailureMessage(error.getMessage());
                            }));
        } else {
            getView().showFailureMessage("");
        }
    }
}
