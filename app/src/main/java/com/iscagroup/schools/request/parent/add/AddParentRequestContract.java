package com.iscagroup.schools.request.parent.add;

import com.google.gson.JsonObject;
import com.iscagroup.schools.base.BaseContract;
import com.iscagroup.schools.data.teacher.Teacher;

import java.util.List;

/**
 * Created by Raed Saeed on 7/7/2019.
 */
public interface AddParentRequestContract {
    interface AddParentRequestView extends BaseContract.BaseView {
        void addParentRequest();

        void getTeachers();

        void showTeachersData(List<Teacher> teachers);

        void onSuccessfulAddRequest(JsonObject jsonObject);

        void onFailureAddRequest(String message);
    }

    interface AddParentRequestPresenter {
        void getTeachers(String classId);

        void addRequest(String studentId, String classId, String teacherId, String schoolId, String requestFor, String request);
    }
}
