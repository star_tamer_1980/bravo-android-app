package com.iscagroup.schools.request;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.AbstractViewHolder;
import com.iscagroup.schools.base.TypeFactory;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.request.Attach;
import com.iscagroup.schools.data.request.Request;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.utils.UserSharedPreferences;
import com.iscagroup.schools.widget.ExpandableTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 7/4/2019.
 */
public class RequestAdapter extends RecyclerView.Adapter<AbstractViewHolder<BaseData>> {
    private List<Request> items;

    private OnRequestClickListener onRequestClickListener;

    private TypeFactory typeFactory = new TypeFactory() {
        @Override
        public int type(BaseData object) {
            return object.getResourceId();
        }

        @Override
        public AbstractViewHolder createViewHolder(View view, int viewType) {
            switch (viewType) {
                case R.layout.item_parent_open_request:
                    return new OpenParentRequestViewHolder(view);
                case R.layout.item_teacher_request:
                    return new OpenTeacherRequestViewHolder(view);
                case R.layout.item_school_request:
                    return new OpenSchoolRequestViewHolder(view);
                default:
                    return new ClosedParentRequestViewHolder(view);
            }
        }
    };

    public RequestAdapter() {

    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public AbstractViewHolder<BaseData> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return typeFactory.createViewHolder(view, viewType);

    }

    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder<BaseData> holder, int position) {
        BaseData item = items.get(position);
        if (item != null) {
            holder.bind(item);
        }
    }

    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return typeFactory.type(items.get(position));
    }

    public void addList(List<Request> dataList) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.addAll(dataList);
        notifyDataSetChanged();
    }

    public void clearList() {
        if (items != null) {
            items.clear();
            notifyDataSetChanged();
        }
    }

    public void addItem(Request request) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.add(0, request);
        notifyItemInserted(0);
    }

    void removeItem(Request request) {
        if (items != null) {
            int position = items.indexOf(request);
            items.remove(request);
            notifyItemRemoved(position);
        }
    }

    public void removeItem(int positionToRemove) {
        if (items != null && positionToRemove < items.size() && positionToRemove != -1) {
            Log.w(RequestAdapter.class.getSimpleName(), "removeItem: removing item" + positionToRemove);
            items.remove(positionToRemove);
            notifyItemRemoved(positionToRemove);
        }
    }

    public void setOnRequestClickListener(OnRequestClickListener onRequestClickListener) {
        this.onRequestClickListener = onRequestClickListener;
    }

    public interface OnRequestClickListener {
        void onRequestClick(int position, Request request);
    }

    class OpenParentRequestViewHolder extends AbstractViewHolder<Request> {
        private CardView cvOpenRequestItemCard;
        private TextView tvKidName;
        private TextView tvKidClass;
        private ImageView ivKidImage;
        private ExpandableTextView tvDescription;
        private ImageView ivAttachment;
        private TextView tvCreateDate;

        private String userPhoto;
        private String userClass;

        OpenParentRequestViewHolder(View itemView) {
            super(itemView);
            cvOpenRequestItemCard = itemView.findViewById(R.id.cv_item_parent_open_request_card);
            cvOpenRequestItemCard.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), RequestDetailActivity.class);
                intent.putExtra(StringConstants.EXTRA_REQUEST, items.get(getAdapterPosition()));
                v.getContext().startActivity(intent);
            });
            tvKidName = itemView.findViewById(R.id.tv_item_parent_open_request_kid_name);
            tvKidClass = itemView.findViewById(R.id.tv_item_parent_open_request_kid_class);
            ivKidImage = itemView.findViewById(R.id.civ_item_parent_open_request_avatar);
            tvDescription = itemView.findViewById(R.id.tv_item_parent_open_request_description);
            ivAttachment = itemView.findViewById(R.id.iv_item_parent_open_request_has_attachment);
            tvCreateDate = itemView.findViewById(R.id.tv_item_parent_open_request_date);

            userPhoto = UserSharedPreferences.getUserPhoto(itemView.getContext());
            userClass = UserSharedPreferences.getUserClassName(itemView.getContext());
        }

        @Override
        public void bind(Request element) {
            tvKidName.setText(element.getName());
            tvKidClass.setText(userClass);

            tvDescription.setText(element.getRequest());

            Glide.with(itemView)
                    .load(userPhoto)
                    .into(ivKidImage);

            if (element.getAttach() != null && element.getAttach().size() > 0) {
                ivAttachment.setVisibility(View.VISIBLE);
            } else {
                ivAttachment.setVisibility(View.INVISIBLE);
            }

            tvCreateDate.setText(element.getCreateDate());
        }
    }

    class ClosedParentRequestViewHolder extends AbstractViewHolder<Request> {
        private CardView cvClosedRequestCard;
        private TextView tvKidName;
        private TextView tvKidClass;
        private ImageView ivKidImage;
        private ExpandableTextView tvDescription;
        private ImageView ivAttachment;
        private TextView tvCreateDate;
        private TextView tvSenderName;
        private ImageView ivSenderAvatar;
        private ExpandableTextView tvReply;
        private ImageView ivReplyAttachment;
        private TextView tvReplyDate;

        private String userClass;
        private String userSchoolLogo;

        ClosedParentRequestViewHolder(@NonNull View itemView) {
            super(itemView);
            cvClosedRequestCard = itemView.findViewById(R.id.cv_item_closed_request_card);
            cvClosedRequestCard.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), RequestDetailActivity.class);
                intent.putExtra(StringConstants.EXTRA_REQUEST, items.get(getAdapterPosition()));
                v.getContext().startActivity(intent);
            });

            tvKidName = itemView.findViewById(R.id.tv_item_closed_request_kid_name);
            tvKidClass = itemView.findViewById(R.id.tv_item_closed_request_kid_class);
            ivKidImage = itemView.findViewById(R.id.civ_item_closed_request_avatar);
            tvDescription = itemView.findViewById(R.id.tv_item_closed_request_description);
            ivAttachment = itemView.findViewById(R.id.iv_item_closed_request_has_attachment);
            tvCreateDate = itemView.findViewById(R.id.tv_item_closed_request_date);
            tvSenderName = itemView.findViewById(R.id.tv_item_closed_request_sender_name);
            ivSenderAvatar = itemView.findViewById(R.id.civ_item_closed_request_sender_avatar);
            tvReply = itemView.findViewById(R.id.tv_item_closed_request_reply);
            ivReplyAttachment = itemView.findViewById(R.id.iv_item_closed_request_reply_has_attachment);
            tvReplyDate = itemView.findViewById(R.id.tv_item_closed_request_reply_date);

            userClass = UserSharedPreferences.getUserClassName(itemView.getContext());
            userSchoolLogo = UserSharedPreferences.getUserSchoolLogo(itemView.getContext());
        }

        @Override
        public void bind(Request element) {
            tvKidName.setText(element.getName());
            tvKidClass.setText(userClass);

            tvDescription.setText(element.getRequest());

            Glide.with(itemView)
                    .load(element.getStudentPhoto())
                    .into(ivKidImage);

            // Todo fix this crash at offline mode
            if (getRequestFiles(element).size() > 0) {
                ivAttachment.setVisibility(View.VISIBLE);
            } else {
                ivAttachment.setVisibility(View.INVISIBLE);
            }

            // Todo fix this crash at offline mode
            if (getReplyFiles(element).size() > 0) {
                ivReplyAttachment.setVisibility(View.VISIBLE);
            } else {
                ivReplyAttachment.setVisibility(View.INVISIBLE);
            }

            tvCreateDate.setText(element.getCreateDate());
            tvSenderName.setText(element.getTeacherName());

            Glide.with(itemView)
                    .load(userSchoolLogo)
                    .into(ivSenderAvatar);

            tvReply.setText(element.getReply());
            tvReplyDate.setText(element.getReplyDate());
        }

        private List<Attach> getRequestFiles(Request request) {
            List<Attach> requestFiles = new ArrayList<>();
            if (request.getAttach() != null && request.getAttach().size() != 0) {
                for (Attach attach : request.getAttach()) {
                    if (attach.getParentRequest().equalsIgnoreCase("1")) {
                        requestFiles.add(attach);
                    }
                }
            }
            return requestFiles;
        }

        private List<Attach> getReplyFiles(Request request) {
            List<Attach> replyAttach = new ArrayList<>();
            if (request.getAttach() != null && request.getAttach().size() != 0) {
                for (Attach attach : request.getAttach()) {
                    if (attach.getParentRequest().equalsIgnoreCase("0")) {
                        replyAttach.add(attach);
                    }
                }
            }
            return replyAttach;
        }
    }

    class OpenTeacherRequestViewHolder extends AbstractViewHolder<Request> {
        private CardView cvItemRequestCard;
        private TextView tvKidName;
        private TextView tvKidClass;
        private ImageView ivKidAvatar;
        private ExpandableTextView tvRequestDescription;
        private ImageView ivRequestAttachment;
        private TextView tvRequestDate;

        OpenTeacherRequestViewHolder(@NonNull View itemView) {
            super(itemView);
            cvItemRequestCard = itemView.findViewById(R.id.cv_item_teacher_request_card);
            cvItemRequestCard.setOnClickListener(v -> {
                Request request = items.get(getAdapterPosition());
                onRequestClickListener.onRequestClick(getAdapterPosition(), request);
            });

            tvKidName = itemView.findViewById(R.id.tv_item_teacher_request_name);
            tvKidClass = itemView.findViewById(R.id.tv_item_teacher_request_class);
            ivKidAvatar = itemView.findViewById(R.id.iv_item_teacher_request_kid_avatar);
            tvRequestDescription = itemView.findViewById(R.id.tv_item_teacher_request_description);
            ivRequestAttachment = itemView.findViewById(R.id.iv_item_teacher_request_has_attachment);
            tvRequestDate = itemView.findViewById(R.id.tv_item_teacher_request_date);
        }

        @Override
        public void bind(Request element) {
            tvKidName.setText(element.getName());
            tvKidClass.setText(element.getClassName());

            Glide.with(itemView)
                    .load(element.getStudentPhoto())
                    .into(ivKidAvatar);

            tvRequestDescription.setText(element.getRequest());

            if (element.getAttach() != null && element.getAttach().size() > 0) {
                ivRequestAttachment.setVisibility(View.VISIBLE);
            } else {
                ivRequestAttachment.setVisibility(View.INVISIBLE);
            }

            tvRequestDate.setText(element.getCreateDate());
        }
    }

    class OpenSchoolRequestViewHolder extends AbstractViewHolder<Request> {
        private CardView cvItemRequestCard;
        private TextView tvKidName;
        private TextView tvKidClass;
        private ImageView ivKidAvatar;
        private ExpandableTextView tvRequestDescription;
        private ImageView ivRequestAttachment;
        private TextView tvRequestDate;

        OpenSchoolRequestViewHolder(@NonNull View itemView) {
            super(itemView);
            cvItemRequestCard = itemView.findViewById(R.id.cv_item_school_request_card);
            cvItemRequestCard.setOnClickListener(v -> {
                Request request = items.get(getAdapterPosition());
                onRequestClickListener.onRequestClick(getAdapterPosition(), request);
            });

            tvKidName = itemView.findViewById(R.id.tv_item_school_request_name);
            tvKidClass = itemView.findViewById(R.id.tv_item_school_request_class);
            ivKidAvatar = itemView.findViewById(R.id.iv_item_school_request_kid_avatar);
            tvRequestDescription = itemView.findViewById(R.id.tv_item_school_request_description);
            ivRequestAttachment = itemView.findViewById(R.id.iv_item_school_request_has_attachment);
            tvRequestDate = itemView.findViewById(R.id.tv_item_school_request_date);
        }

        @Override
        public void bind(Request element) {
            tvKidName.setText(element.getName());
            tvKidClass.setText(element.getClassName());

            Glide.with(itemView)
                    .load(element.getStudentPhoto())
                    .into(ivKidAvatar);

            tvRequestDescription.setText(element.getRequest());

            if (element.getAttach() != null && element.getAttach().size() > 0) {
                ivRequestAttachment.setVisibility(View.VISIBLE);
            } else {
                ivRequestAttachment.setVisibility(View.INVISIBLE);
            }

            tvRequestDate.setText(element.getCreateDate());
        }
    }
}