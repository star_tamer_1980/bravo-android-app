package com.iscagroup.schools.request.teacher;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.iscagroup.schools.R;
import com.iscagroup.schools.adapter.FragmentAdapter;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

public class TeacherRequestListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_management_request);

        Toolbar toolbar = findViewById(R.id.tb_activity_management_request);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.parent_request);
        }

        initViews();
    }

    private void initViews() {
        ViewPager viewPager = findViewById(R.id.vp_activity_management_request);

        FragmentAdapter fragmentAdapter = new FragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(fragmentAdapter);

        TabLayout tabLayout = findViewById(R.id.tl_activity_management_request_tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        fragmentAdapter.setFragments(getFragments());
    }

    protected List<BaseFragment> getFragments() {
        List<BaseFragment> fragments = new ArrayList<>();
        TeacherOpenRequestFragment openFrag = new TeacherOpenRequestFragment();
        openFrag.setName(getString(R.string.open));
        fragments.add(openFrag);

        TeacherClosedRequestFragment closedFrag = new TeacherClosedRequestFragment();
        fragments.add(closedFrag);
        closedFrag.setName(getString(R.string.closed));
        return fragments;
    }
}
