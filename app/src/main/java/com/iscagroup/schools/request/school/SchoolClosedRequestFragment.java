package com.iscagroup.schools.request.school;

import com.iscagroup.schools.request.teacher.TeacherClosedRequestFragment;

/**
 * Created by Raed Saeed on 7/14/2019.
 */
public class SchoolClosedRequestFragment extends TeacherClosedRequestFragment {
    @Override
    public void getRequests() {
        // set isRefreshed to true to enable setRefreshing method
        isRefreshed = true;

        // set isLastPage to false to enable loadMoreItems method
        isLastPage = false;

        requestPresenterImp.getSchoolRequests(schoolId, REQUEST_STATUS, "0", "10");
    }

    @Override
    protected void loadMoreItems() {
        loadMore = true;
        requestPresenterImp.getSchoolRequests(schoolId, REQUEST_STATUS, String.valueOf(requestAdapter.getItemCount()), "10");
    }
}
