package com.iscagroup.schools.request.parent;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.iscagroup.schools.R;
import com.iscagroup.schools.adapter.FragmentAdapter;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

public class ParentRequestListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_request);

        Toolbar toolbar = findViewById(R.id.tb_activity_parent_request);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.parent_request);
        }

        initViews();
    }

    private void initViews() {
        ViewPager viewPager = findViewById(R.id.vp_activity_parent_request);

        FragmentAdapter fragmentAdapter = new FragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(fragmentAdapter);

        TabLayout tabLayout = findViewById(R.id.tl_activity_parent_request_tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        fragmentAdapter.setFragments(getFragments());
    }

    private List<BaseFragment> getFragments() {
        List<BaseFragment> fragments = new ArrayList<>();
        OpenParentRequestFragment openFrag = new OpenParentRequestFragment();
        openFrag.setName(getString(R.string.open));
        fragments.add(openFrag);

        ClosedParentRequestFragment closedFrag = new ClosedParentRequestFragment();
        closedFrag.setName(getString(R.string.closed));
        fragments.add(closedFrag);
        return fragments;
    }
}
