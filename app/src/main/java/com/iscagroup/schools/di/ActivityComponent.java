package com.iscagroup.schools.di;

import android.app.Activity;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by Raed Saeed on 8/21/2019.
 */

@Component(modules = {RetrofitModule.class})
public interface ActivityComponent {
    void injectActivity(Activity activity);

    Retrofit getRetrofit();
}
