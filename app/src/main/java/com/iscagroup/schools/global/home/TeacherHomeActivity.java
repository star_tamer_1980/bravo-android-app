package com.iscagroup.schools.global.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.iscagroup.schools.R;
import com.iscagroup.schools.data.login.HomeModel;
import com.iscagroup.schools.data.login.Result;
import com.iscagroup.schools.data.login.School;
import com.iscagroup.schools.utils.StringConstants;

import java.util.ArrayList;
import java.util.List;

public class TeacherHomeActivity extends BaseNavActivity {

    private School school;
    private Result loginResult;
    private NavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = menuItem -> {
        switch (menuItem.getItemId()) {
            case R.id.action_settings:
                closeDrawer();
                return true;
            case R.id.action_menu_logout:
                closeDrawer();
                logout();
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout flContainer = findViewById(R.id.fl_activity_nav_drawer_container);
        View activityView = getLayoutInflater().inflate(R.layout.activity_teacher_home, flContainer);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.home);
        }


        Intent intent = getIntent();
        if (intent != null) {
            school = intent.getParcelableExtra(StringConstants.EXTRA_SCHOOL_HOME);
            loginResult = intent.getParcelableExtra(StringConstants.EXTRA_LOGIN_RESPONSE);
        }

        setUpNavHeader();
        setUpNavMenu();

        HomeAdapter homeAdapter = new HomeAdapter();

        RecyclerView rvParentHomeList = activityView.findViewById(R.id.rv_activity_teacher_home_list);
        rvParentHomeList.setLayoutManager(new GridLayoutManager(this, 2));
        rvParentHomeList.setAdapter(homeAdapter);

        homeAdapter.setHomeList(setHomeModelViewType(school.getHomeList()));
    }

    protected List<HomeModel> setHomeModelViewType(List<HomeModel> homes) {
        List<HomeModel> homeModels = new ArrayList<>();
        for (HomeModel model : homes) {
            model.setResourceId(2);
            homeModels.add(model);
        }
        return homeModels;
    }

    protected void setUpNavHeader() {
        View headerView = getLayoutInflater().inflate(R.layout.nav_header_parent, null, false);

        ImageView ivKidImage = headerView.findViewById(R.id.civ_nav_header_photo);
        TextView tvSchoolName = headerView.findViewById(R.id.tv_nav_header_parent_school_name);
        TextView tvUserName = headerView.findViewById(R.id.tv_nav_header_parent_user_name);

        ConstraintLayout clBirthDateContainer = headerView.findViewById(R.id.cl_nav_header_parent_birthday);
        clBirthDateContainer.setVisibility(View.GONE);

        setNavigationViewHeader(headerView);

        if (school != null && loginResult != null) {
            Glide.with(headerView)
                    .load(school.getLogo())
                    .into(ivKidImage);


            tvSchoolName.setText(school.getName());
            tvUserName.setText(loginResult.getUserName());
        }
    }

    protected void setUpNavMenu() {
        setNavigationViewMenu(R.menu.menu_parent_nav_drawer);
        setNavigationViewClickListener(navigationItemSelectedListener);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivityWithTransition();
    }
}
