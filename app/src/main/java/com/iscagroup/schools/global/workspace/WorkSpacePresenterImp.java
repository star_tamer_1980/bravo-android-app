package com.iscagroup.schools.global.workspace;

import com.iscagroup.schools.base.BasePresenter;
import com.iscagroup.schools.model.local.AppDatabase;
import com.iscagroup.schools.model.remote.LoginService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Raed Saeed on 6/27/2019.
 */
public class WorkSpacePresenterImp extends BasePresenter<WorkSpaceContract.WorkSpaceView> implements WorkSpaceContract.WorkSpacePresenter {
    @SuppressWarnings("unused")
    private static final String TAG = "WorkSpacePresenter";

    private LoginService loginService;

    WorkSpacePresenterImp() {

    }

    @Override
    protected void onCreate() {
        loginService = LoginService.getInstance();
    }

    @Override
    public void getWorkSpace(String code, String phoneNumber, String playerId) {
        if (getView().isConnected()) {
            getCompositeDisposable().add(loginService.loging(code, phoneNumber, playerId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(response -> getView().showProgressBar())
                    .doFinally(() -> getView().hideProgressBar())
                    .subscribe(loginResponse -> {
                        getView().showLoginResponse(loginResponse.getResult());

                        getCompositeDisposable().add(AppDatabase.getInstance()
                                .getLoginResultDao()
                                .deleteLogin()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnComplete(() -> getCompositeDisposable().add(AppDatabase.getInstance()
                                        .getLoginResultDao()
                                        .insertResults(loginResponse.getResult())
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(response -> {
                                                }, error -> getView().showFailureMessage(error.getMessage())
                                        )))

                                .subscribe());

                    }, error -> {
                        getView().showFailureMessage(error.getMessage());

                        getCompositeDisposable().add(AppDatabase.getInstance()
                                .getLoginResultDao()
                                .loadAllLoginResults()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(results -> getView().showLoginResponse(results)
                                        , errorMsg -> getView().showFailureMessage(errorMsg.getMessage())));
                    }));
        } else {
            getCompositeDisposable().add(AppDatabase.getInstance().getLoginResultDao().loadAllLoginResults()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doFinally(() -> getView().hideProgressBar())
                    .subscribe(response -> getView().showLoginResponse(response),
                            error -> getView().showFailureMessage(error.getMessage())));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
