package com.iscagroup.schools.global.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import com.hbb20.CountryCodePicker;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.login.LoginResponse;
import com.iscagroup.schools.global.verification.CodeVerificationActivity;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.utils.UserSharedPreferences;
import com.iscagroup.schools.utils.Validation;

public class LoginActivity extends BaseActivity implements LoginContract.LoginView {
    @SuppressWarnings("unused")
    private static final String TAG = "LoginActivity";

    private LoginPresenterImp loginPresenterImp;
    private CountryCodePicker countryCodePicker;
    private EditText etLoginPhoneNumber;

    // The final phoneWithoutPlus number string e.g +201012345555
    private String phoneNumberWithPlus;
    private String phoneWithoutPlus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_FullScreen);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        initVars();
    }

    private void initVars() {
        loginPresenterImp = new LoginPresenterImp();
        loginPresenterImp.attachLifecycle(getLifecycle());
        loginPresenterImp.attachView(this);
    }

    private void initViews() {
        countryCodePicker = findViewById(R.id.ccp_activity_login_country_code_picker);
        etLoginPhoneNumber = findViewById(R.id.et_activity_login_phone_number);
        Button btnLogin = findViewById(R.id.btn_activity_login);
        btnLogin.setOnClickListener(v -> login());
    }


    @Override
    public void onConnectivityChanged(boolean isConnected) {
        if (!isConnected) {
            showConnectionErrorMessage(findViewById(R.id.cl_activity_login_phone_container));
        } else {
            dismissConnectionErrorMessage();
        }
    }

    @Override
    public void showProgressBar() {
        showProgressDialog(getString(R.string.logining));
    }

    @Override
    public void hideProgressBar() {
        hideProgressDialog();
    }

    @Override
    public void login() {
        String phoneNumberText = etLoginPhoneNumber.getText().toString().trim();
        if (!TextUtils.isEmpty(phoneNumberText)) {
            if (Validation.validateNumberWithCode(this, countryCodePicker.getSelectedCountryNameCode(), phoneNumberText)) {

                phoneWithoutPlus = Validation.filterNumber(phoneNumberText);
                phoneNumberWithPlus = countryCodePicker.getSelectedCountryCodeWithPlus() + phoneWithoutPlus;
                loginPresenterImp.login(countryCodePicker.getSelectedCountryCode(), phoneWithoutPlus, UserSharedPreferences.getUserPlayerId(this));

            } else {
                showToast("Invalid Phone Number");
            }
        } else {
            etLoginPhoneNumber.setError(getString(R.string.required));
        }
    }

    @Override
    public void onSuccessfulLoginResponse(LoginResponse loginResponse) {
        if (loginResponse.getStatus()) {
            Intent intent = new Intent(this, CodeVerificationActivity.class);
            intent.putExtra(StringConstants.EXTRA_LOGIN_RESPONSE, loginResponse);
            intent.putExtra(StringConstants.EXTRA_PHONE_NUMBER, phoneNumberWithPlus);
            intent.putExtra(StringConstants.EXTRA_CODE_NUMBER, countryCodePicker.getSelectedCountryCode());
            intent.putExtra(StringConstants.EXTRA_USER_PHONE_NUMBER, phoneWithoutPlus);
            startActivity(intent);
        } else {
            showToast(loginResponse.getMsg());
        }
    }

    @Override
    public void showFailureMessage(String message) {
        super.showFailureMessage(message);
//        showToast(getString(R.string.connection_interruption));
    }
}
