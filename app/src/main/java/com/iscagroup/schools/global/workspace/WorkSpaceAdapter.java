package com.iscagroup.schools.global.workspace;

import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.AbstractViewHolder;
import com.iscagroup.schools.base.TypeFactory;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.login.Result;
import com.iscagroup.schools.global.main.MainActivity;
import com.iscagroup.schools.utils.UserSharedPreferences;

import java.util.List;

import static com.iscagroup.schools.utils.StringConstants.EXTRA_LOGIN_DATA;

/**
 * Created by Raed Saeed on 6/27/2019.
 */
public class WorkSpaceAdapter extends RecyclerView.Adapter<AbstractViewHolder<Result>> {
    @SuppressWarnings("unused")
    private static final String TAG = "WorkSpaceAdapter";

    private TypeFactory typeFactory = new TypeFactory() {
        @Override
        public int type(BaseData object) {
            return R.layout.item_workspace;
        }

        @Override
        public AbstractViewHolder createViewHolder(View view, int type) {
            return new WorkSpaceViewHolder(view);
        }
    };

    private List<Result> loginResults;

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public AbstractViewHolder<Result> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return typeFactory.createViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder<Result> holder, int position) {
        Result result = loginResults.get(position);
        if (result != null) {
            holder.bind(result);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return typeFactory.type(loginResults.get(position));
    }

    @Override
    public int getItemCount() {
        return (loginResults != null) ? loginResults.size() : 0;
    }

    void setLoginResults(List<Result> list) {
        this.loginResults = list;
        notifyDataSetChanged();
    }

    class WorkSpaceViewHolder extends AbstractViewHolder<Result> implements View.OnClickListener {

        private ImageView ivWorkspaceImage;
        private TextView tvWorkspaceTitle;
        private Image ivWorkspaceForward;
        private CardView cvWorkspaceCard;


        WorkSpaceViewHolder(@NonNull View itemView) {
            super(itemView);
            ivWorkspaceImage = itemView.findViewById(R.id.iv_item_workspace_image);
            tvWorkspaceTitle = itemView.findViewById(R.id.tv_item_workspace_title);
            cvWorkspaceCard = itemView.findViewById(R.id.cv_item_workspace_card);
            cvWorkspaceCard.setOnClickListener(this);
        }

        @Override
        public void bind(Result element) {
            tvWorkspaceTitle.setText(getTitleType(element.getUserType()));
            ivWorkspaceImage.setImageResource(getWorkImageType(element.getUserType()));
            if (loginResults.size() == 1) {
                cvWorkspaceCard.performClick();
            }
        }

        @Override
        public void onClick(View v) {
            if (getAdapterPosition() != -1 && getAdapterPosition() < loginResults.size()) {
                Result result = loginResults.get(getAdapterPosition());
                UserSharedPreferences.setUserName(v.getContext(), result.getUserName());
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                intent.putExtra(EXTRA_LOGIN_DATA, result);

                if (loginResults.size() == 1) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                }

                v.getContext().startActivity(intent);
            }
        }

        private String getTitleType(String type) {
            switch (type) {
                case "1":
                    return "Parent";
                case "2":
                    return "Teacher";
                default:
                    return "Management";
            }
        }

        private int getWorkImageType(String type) {
            switch (type) {
                case "1":
                    return R.drawable.ic_home_24dp;
                case "2":
                    return R.drawable.ic_man;
                default:
                    return R.drawable.ic_work_24dp;
            }
        }
    }
}
