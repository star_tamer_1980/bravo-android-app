package com.iscagroup.schools.global.workspace;

import com.iscagroup.schools.base.BaseContract;
import com.iscagroup.schools.data.login.Result;

import java.util.List;

/**
 * Created by Raed Saeed on 6/27/2019.
 */
public interface WorkSpaceContract {
    interface WorkSpaceView extends BaseContract.BaseView {
        void showLoginResponse(List<Result> list);
    }

    interface WorkSpacePresenter {
        void getWorkSpace(String code, String phoneNumber, String playerId);
    }
}
