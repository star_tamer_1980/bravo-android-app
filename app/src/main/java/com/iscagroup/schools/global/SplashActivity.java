package com.iscagroup.schools.global;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.global.login.LoginActivity;
import com.iscagroup.schools.global.workspace.WorkSpaceActivity;
import com.iscagroup.schools.utils.UserSharedPreferences;


public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_Splash);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (UserSharedPreferences.getUserLoginStatus(this)) {
            new Handler().postDelayed(this::startWorkSpaceActivity, 2000L);
        } else {
            new Handler().postDelayed(this::startLoginActivity, 2000L);
        }

    }

    private void startWorkSpaceActivity() {
        Intent intent = new Intent(this, WorkSpaceActivity.class);
        startActivity(intent);
        finish();
    }

    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
