package com.iscagroup.schools.global;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.gson.JsonObject;
import com.iscagroup.schools.base.BasePresenter;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.request.Attach;
import com.iscagroup.schools.model.remote.AttachmentService;

import java.io.File;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

/**
 * Created by Raed Saeed on 7/8/2019.
 */
public class AttachmentPresenter extends BasePresenter<AttachmentContract.AttachmentView> {
    private static final String TAG = "AttachmentPresenter";
    private AttachmentService attachmentService;


    @Override
    protected void onCreate() {
        attachmentService = AttachmentService.getInstance();
    }


    public void removeFiles(String ids) {
        getCompositeDisposable().add(attachmentService.removeFiles(ids).subscribeOn(io())
                .observeOn(mainThread())
                .subscribe());
    }

    public void attachFiles(String featureType, String featureId, String schoolId, List<BaseData> files) {
        attachFiles(featureType, featureId, schoolId, files, false);
    }

    @SuppressLint("CheckResult")
    public void attachFiles(String featureType, String featureId, String schoolId, List<BaseData> files, boolean isParent) {
        String userTypeIndicator = isParent ? "1" : "0";
        Observable.fromIterable(files)
                .filter(baseData -> {
                    Attach attach = (Attach) baseData;
                    return (attach.getId() == null);
                })
                .map(baseData -> {
                    Attach attach = (Attach) baseData;
                    File file = new File(attach.getFilePath());
                    RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
                    return MultipartBody.Part.createFormData("file", file.getName(), requestBody);
                })
                .flatMap(part -> {
                    RequestBody featureTypeBody = RequestBody.create(MediaType.parse("text/plain"), featureType);
                    RequestBody featureIdBody = RequestBody.create(MediaType.parse("text/plain"), featureId);
                    RequestBody schoolIdBody = RequestBody.create(MediaType.parse("text/plain"), schoolId);
                    RequestBody isParentBody = RequestBody.create(MediaType.parse("text/plain"), userTypeIndicator);
                    return uploadFile(featureTypeBody, featureIdBody, schoolIdBody, isParentBody, part);
                })
                .subscribe(new Observer<JsonObject>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        Log.w(TAG, "onNext: " + jsonObject.toString());
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().onUploadFailed();
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        getView().onUploadDone();
                    }
                });
    }

    private Observable<JsonObject> uploadFile(RequestBody type, RequestBody id, RequestBody schoolId, RequestBody isParent, MultipartBody.Part part) {
        return attachmentService.addAttachment(type, id, schoolId, isParent, part).subscribeOn(io())
                .observeOn(mainThread());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
