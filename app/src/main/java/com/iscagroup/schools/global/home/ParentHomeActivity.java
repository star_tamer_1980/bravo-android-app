package com.iscagroup.schools.global.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.iscagroup.schools.R;
import com.iscagroup.schools.data.Age;
import com.iscagroup.schools.data.login.HomeModel;
import com.iscagroup.schools.data.login.Result;
import com.iscagroup.schools.data.login.Student;
import com.iscagroup.schools.utils.AgeCalculatar;
import com.iscagroup.schools.utils.StringConstants;

import java.util.ArrayList;
import java.util.List;

public class ParentHomeActivity extends BaseNavActivity {
    private static final String TAG = "ParentHomeActivity";

    /**
     * Received student data
     */
    private Student student;

    /**
     * Received login response that contain all user data
     */
    private Result loginResult;

    private NavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = menuItem -> {
        switch (menuItem.getItemId()) {
            case R.id.action_settings:
                closeDrawer();
                return true;
            case R.id.action_menu_logout:
                closeDrawer();
                logout();
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout flContainer = findViewById(R.id.fl_activity_nav_drawer_container);
        View activityView = getLayoutInflater().inflate(R.layout.activity_home, flContainer);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.home);
        }


        Intent intent = getIntent();
        if (intent != null) {
            student = intent.getParcelableExtra(StringConstants.EXTRA_STUDENT_HOME);
            loginResult = intent.getParcelableExtra(StringConstants.EXTRA_LOGIN_RESPONSE);
        }

        setUpNavHeader();
        setUpNavMenu();

        HomeAdapter homeAdapter = new HomeAdapter();

        RecyclerView rvParentHomeList = activityView.findViewById(R.id.rv_activity_home_list);
        rvParentHomeList.setLayoutManager(new GridLayoutManager(this, 2));
        rvParentHomeList.setAdapter(homeAdapter);

        homeAdapter.setHomeList(setHomeModelViewType(student.getHomeList()));
    }

    private List<HomeModel> setHomeModelViewType(List<HomeModel> homes) {
        List<HomeModel> homeModels = new ArrayList<>();
        for (HomeModel model : homes) {
            model.setResourceId(1);
            homeModels.add(model);
        }
        return homeModels;
    }

    private void setUpNavHeader() {
        View headerView = getLayoutInflater().inflate(R.layout.nav_header_parent, null, false);

        ImageView ivKidImage = headerView.findViewById(R.id.civ_nav_header_photo);
        TextView tvSchoolName = headerView.findViewById(R.id.tv_nav_header_parent_school_name);
        TextView tvUserName = headerView.findViewById(R.id.tv_nav_header_parent_user_name);

        TextView tvYearsNumber = headerView.findViewById(R.id.tv_nav_header_parent_year_number);
        TextView tvMonthNumbr = headerView.findViewById(R.id.tv_nav_header_parent_month_number);
        TextView tvDayNumber = headerView.findViewById(R.id.tv_nav_header_parent_day_number);

        setNavigationViewHeader(headerView);

        if (student != null) {
            Age age = AgeCalculatar.calculateAge(student.getBirthdate());
            tvYearsNumber.setText(String.valueOf(age.getYears()));
            tvMonthNumbr.setText(String.valueOf(age.getMonths()));
            tvDayNumber.setText(String.valueOf(age.getDays()));
            Glide.with(headerView)
                    .load(student.getStudentPhoto())
                    .into(ivKidImage);


            tvSchoolName.setText(student.getSchoolName());
            tvUserName.setText(new StringBuilder().append(student.getFirstName()).append(" ").append(student.getLastName()));
        }
    }

    private void setUpNavMenu() {
        setNavigationViewMenu(R.menu.menu_parent_nav_drawer);
        setNavigationViewClickListener(navigationItemSelectedListener);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivityWithTransition();
    }
}
