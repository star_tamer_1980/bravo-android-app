package com.iscagroup.schools.global.verification;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.login.LoginResponse;
import com.iscagroup.schools.global.workspace.WorkSpaceActivity;
import com.iscagroup.schools.model.local.AppDatabase;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.utils.UserSharedPreferences;
import com.iscagroup.schools.widget.PinView;
import com.onesignal.OneSignal;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CodeVerificationActivity extends BaseActivity {
    @SuppressWarnings("unused")
    private static final String TAG = "CodeVerification";

    /**
     * The received phone number from {@link com.iscagroup.schools.global.login.LoginActivity}
     * To send it to {@link com.google.firebase.auth.FirebaseAuth} to Verify it.
     */
    private String phoneNumber;


    /**
     * Response from Login Api that hold all the user data
     */
    private LoginResponse loginResponse;

    /**
     * User Phone number without plus sign or country code
     */
    private String userPhoneNumber;

    /**
     * Country code number
     */
    private String countryCode;

    private PhoneAuthProvider.ForceResendingToken token;

    /**
     * It is the verification id that received from {@link PhoneAuthProvider}
     * To use it with the received code to get the <br>Sign In Credential</br>
     */
    private String mVerificationId;
    private CompositeDisposable compositeDisposable;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            Log.w(TAG, "onVerificationCompleted: ");
            showToast("Phone Number Verified");
            updateDatabase();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.w(TAG, "onVerificationFailed: " + e.getMessage());
            showToast(e.getMessage());

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                Log.w(TAG, "onVerificationFailed: credential is invalid ");
                // Invalid request
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                Log.w(TAG, "onVerificationFailed: sms quota for this project has been exceeded ");
                // ...
            }
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            Log.w(TAG, "onCodeSent: ");
            super.onCodeSent(s, forceResendingToken);
            mVerificationId = s;
            token = forceResendingToken;
        }
    };

    // Initialize Views
    private Button btnCodeVerificationVerify;
    private PinView pvVerificationCode;
    private TextView tvCodeVerificationResendCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_FullScreen);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_verification);

        Intent intent = getIntent();
        if (intent != null) {
            loginResponse = intent.getParcelableExtra(StringConstants.EXTRA_LOGIN_RESPONSE);
            phoneNumber = intent.getStringExtra(StringConstants.EXTRA_PHONE_NUMBER);
            countryCode = intent.getStringExtra(StringConstants.EXTRA_CODE_NUMBER);
            userPhoneNumber = intent.getStringExtra(StringConstants.EXTRA_USER_PHONE_NUMBER);
        }

        initViews();
        initVars();
        startTimeDown();
    }

    private void initViews() {
        pvVerificationCode = findViewById(R.id.pv_activity_code_verification_pin);
        pvVerificationCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() == 6) {
                    btnCodeVerificationVerify.setEnabled(true);
                } else {
                    btnCodeVerificationVerify.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnCodeVerificationVerify = findViewById(R.id.btn_activity_code_verification_verify);
        btnCodeVerificationVerify.setEnabled(false);

        btnCodeVerificationVerify.setOnClickListener(v -> {
            if (pvVerificationCode.getText() != null) {
                String code = pvVerificationCode.getText().toString().trim();
                verifyReceivedCode(code);
            }
        });

        tvCodeVerificationResendCode = findViewById(R.id.tv_activity_code_verification_resend_code);
        tvCodeVerificationResendCode.setEnabled(false);
        tvCodeVerificationResendCode.setOnClickListener(v -> resendVerificationCodeToPhone(phoneNumber, token));
    }

    private void initVars() {
        if (phoneNumber != null) {
            sendVerificationCodeToPhone(phoneNumber);
        }

        compositeDisposable = new CompositeDisposable();
    }

    /**
     * @param phoneNumber that is used by PhoneAuth to send verification code
     */
    private void sendVerificationCodeToPhone(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber,
                45,
                TimeUnit.SECONDS,
                this,
                callbacks);
    }


    /**
     * Resend verification code if it not received
     *
     * @param phoneNumber         that receive Phone Code
     * @param forceResendingToken the token of Phone Code
     */
    private void resendVerificationCodeToPhone(String phoneNumber, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber,
                45,
                TimeUnit.SECONDS,
                this,
                callbacks,
                forceResendingToken);

        startTimeDown();
    }

    /**
     * Create PhoneCredential from received code
     *
     * @param code the received code from Firebase
     */
    private void verifyReceivedCode(String code) {
        if (mVerificationId != null) {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
            signInWithPhoneCredential(credential);
        }
    }

    /**
     * Sign to FirebaseAuth using the credential if user signed successfully this means
     * The number is verified if else the number isn't verified
     *
     * @param phoneAuthCredential credential created from received code
     */
    private void signInWithPhoneCredential(PhoneAuthCredential phoneAuthCredential) {
        showProgressDialog("Verifying your number");
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.signInWithCredential(phoneAuthCredential).addOnCompleteListener(task -> {
            hideProgressDialog();
            if (task.isSuccessful()) {
                updateDatabase();

                FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

                if (firebaseUser != null) {
                    firebaseUser.delete().addOnCompleteListener(task1 -> {
                        if (task1.isSuccessful()) {
                            log("Phone number verified and inserted in database successfully");
                        } else {
                            log("Phone number verified but couldn't delete user from firebase");
                        }
                    });
                }
            } else {
                showToast("Invalid PIN");
            }
        });
    }

    private void startTimeDown() {
        disableView();
        TextView tvCodeVerificationTimeDown = findViewById(R.id.tv_activity_code_verification_time_down);

        new CountDownTimer(45000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvCodeVerificationTimeDown.setText(new StringBuilder().append(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))
                        .append(":").append(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)));
            }

            @Override
            public void onFinish() {
                tvCodeVerificationTimeDown.setText("0:0");
                enableView();
            }
        }.start();
    }

    /**
     * Update database every time user verify his phone number
     * with it's login response, Delete all the previous records and store new records
     */
    private void updateDatabase() {
        compositeDisposable.add(AppDatabase.getInstance().getLoginResultDao().deleteLogin()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> compositeDisposable.add(AppDatabase.getInstance().getLoginResultDao().insertResults(loginResponse.getResult())
                        .subscribeOn(Schedulers.io())

                        .observeOn(AndroidSchedulers.mainThread())

                        .doFinally(() -> {

                            UserSharedPreferences.setUserNationalPhoneNumber(this, userPhoneNumber);
                            UserSharedPreferences.setUserCountryCode(this, countryCode);
                            UserSharedPreferences.setUserLoginStatus(this, true);
                        })

                        .subscribe(response -> {
                            OneSignal.setSubscription(true);
                                    Intent intent = new Intent(this, WorkSpaceActivity.class);
                            intent.putExtra(StringConstants.EXTRA_LOGIN_RESPONSE, loginResponse);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }, error -> showFailureMessage(error.getMessage())
                        )))
                .subscribe());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    public void enableView() {
        tvCodeVerificationResendCode.setEnabled(false);
        tvCodeVerificationResendCode.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    public void disableView() {
        tvCodeVerificationResendCode.setEnabled(true);
        tvCodeVerificationResendCode.setTextColor(getResources().getColor(R.color.grey));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivityWithTransition();
    }
}
