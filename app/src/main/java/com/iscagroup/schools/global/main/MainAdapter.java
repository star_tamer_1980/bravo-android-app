package com.iscagroup.schools.global.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.AbstractViewHolder;
import com.iscagroup.schools.base.TypeFactory;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.login.School;
import com.iscagroup.schools.data.login.Student;
import com.iscagroup.schools.utils.CircleTransform;

import java.util.List;

/**
 * Created by Raed Saeed on 6/30/2019.
 */
public class MainAdapter extends RecyclerView.Adapter<AbstractViewHolder<BaseData>> {
    @SuppressWarnings("unused")
    private static final String TAG = "MainAdapter";

    private TypeFactory typeFactory = new TypeFactory() {
        @Override
        public int type(BaseData object) {
            return object.getResourceId();
        }

        @Override
        public AbstractViewHolder createViewHolder(View view, int type) {
            switch (type) {
                case R.layout.item_main_student:
                    return new MainStudentViewHolder(view);
                case R.layout.item_main_teacher:
                    return new MainSchoolViewHolder(view);
                default:
                    return new MainManagementViewHolder(view);
            }
        }
    };

    private List<BaseData> list;

    private OnStudentClickListener studentClickListener;
    private OnSchoolClickListener schoolClickListener;
    private OnManagementClickListener managementClickListener;

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public AbstractViewHolder<BaseData> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return typeFactory.createViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder<BaseData> holder, int position) {
        BaseData baseData = list.get(position);
        if (baseData != null) {
            holder.bind(baseData);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return typeFactory.type(list.get(position));
    }

    @Override
    public int getItemCount() {
        return (list != null) ? list.size() : 0;
    }

    public void setList(List<BaseData> list) {
        this.list = list;
    }

    void setSchoolClickListener(OnSchoolClickListener schoolClickListener) {
        this.schoolClickListener = schoolClickListener;
    }

    void setStudentClickListener(OnStudentClickListener studentClickListener) {
        this.studentClickListener = studentClickListener;
    }

    void setManagementClickListener(OnManagementClickListener managementClickListener) {
        this.managementClickListener = managementClickListener;
    }

    public interface OnStudentClickListener {
        void onStudentClick(int position, Student student);
    }

    public interface OnSchoolClickListener {
        void onSchoolClick(int position, School school);
    }

    public interface OnManagementClickListener {
        void onManagementClick(int position, School school);
    }

    class MainStudentViewHolder extends AbstractViewHolder<Student> implements View.OnClickListener {
        private CardView cvItemMainStudentCard;
        private TextView tvItemMainStudentSchoolName;
        private TextView tvItemMainStudentClassName;
        private TextView tvItemMainStudentName;
        private ImageView ivItemMainStudentAvatar;
        private Student student;

        MainStudentViewHolder(@NonNull View itemView) {
            super(itemView);
            cvItemMainStudentCard = itemView.findViewById(R.id.cv_item_main_student_card);
            cvItemMainStudentCard.setOnClickListener(this);

            tvItemMainStudentSchoolName = itemView.findViewById(R.id.tv_item_main_students_school_name);
            tvItemMainStudentClassName = itemView.findViewById(R.id.tv_item_main_student_class);
            tvItemMainStudentName = itemView.findViewById(R.id.tv_item_main_student_name);
            ivItemMainStudentAvatar = itemView.findViewById(R.id.iv_item_main_student_avatar);
        }

        @Override
        public void bind(Student element) {
            student = element;
            tvItemMainStudentSchoolName.setText(element.getSchoolName());
            tvItemMainStudentClassName.setText(element.getClassName());
            tvItemMainStudentName.setText(new StringBuilder().append(element.getFirstName()).append(" ").append(element.getLastName()));

            Glide.with(itemView)
                    .load(element.getStudentPhoto())
                    .transform(new CircleTransform(itemView.getContext()))
                    .apply(RequestOptions.centerCropTransform())
                    .into(ivItemMainStudentAvatar);
        }

        @Override
        public void onClick(View v) {
            if (studentClickListener != null) {
                studentClickListener.onStudentClick(getAdapterPosition(), student);
            }
        }
    }

    class MainSchoolViewHolder extends AbstractViewHolder<School> implements View.OnClickListener {
        private CardView cvItemMainSchoolCard;
        private TextView tvItemMainSchoolName;
        private ImageView ivItemMainSchoolLogo;
        private School school;

        MainSchoolViewHolder(@NonNull View itemView) {
            super(itemView);
            cvItemMainSchoolCard = itemView.findViewById(R.id.cv_item_main_teacher_card);
            cvItemMainSchoolCard.setOnClickListener(this);

            ivItemMainSchoolLogo = itemView.findViewById(R.id.iv_item_main_teacher_logo);

            tvItemMainSchoolName = itemView.findViewById(R.id.tv_item_main_teacher_name);
        }

        @Override
        public void bind(School element) {
            school = element;
            tvItemMainSchoolName.setText(element.getName());

            Glide.with(itemView)
                    .load(element.getLogo())
                    .into(ivItemMainSchoolLogo);
        }

        @Override
        public void onClick(View v) {
            if (schoolClickListener != null) {
                schoolClickListener.onSchoolClick(getAdapterPosition(), school);
            }
        }
    }

    class MainManagementViewHolder extends AbstractViewHolder<School> implements View.OnClickListener {
        private CardView cvItemMainSchoolCard;
        private TextView tvItemMainSchoolName;
        private ImageView ivItemMainSchoolLogo;
        private School school;

        MainManagementViewHolder(@NonNull View itemView) {
            super(itemView);
            cvItemMainSchoolCard = itemView.findViewById(R.id.cv_item_main_school_card);
            cvItemMainSchoolCard.setOnClickListener(this);

            ivItemMainSchoolLogo = itemView.findViewById(R.id.iv_item_main_school_logo);

            tvItemMainSchoolName = itemView.findViewById(R.id.tv_item_main_school_name);
        }

        @Override
        public void bind(School element) {
            school = element;
            tvItemMainSchoolName.setText(element.getName());

            Glide.with(itemView)
                    .load(element.getLogo())
                    .into(ivItemMainSchoolLogo);
        }

        @Override
        public void onClick(View v) {
            if (managementClickListener != null) {
                managementClickListener.onManagementClick(getAdapterPosition(), school);
            }
        }
    }
}
