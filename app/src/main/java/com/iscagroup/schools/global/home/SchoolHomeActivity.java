package com.iscagroup.schools.global.home;

import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;
import com.iscagroup.schools.R;
import com.iscagroup.schools.data.login.HomeModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 7/14/2019.
 */
public class SchoolHomeActivity extends TeacherHomeActivity {
    @SuppressWarnings("unused")
    private static final String TAG = "SchoolHomeActivity";
    private NavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = menuItem -> {
        switch (menuItem.getItemId()) {
            case R.id.action_settings:
                closeDrawer();
                return true;
            case R.id.action_menu_logout:
                closeDrawer();
                logout();
                return true;
        }
        return false;
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected List<HomeModel> setHomeModelViewType(List<HomeModel> homes) {
        List<HomeModel> homeModels = new ArrayList<>();
        for (HomeModel model : homes) {
            model.setResourceId(3);
            homeModels.add(model);
        }
        return homeModels;
    }

    protected void setUpNavMenu() {
        setNavigationViewMenu(R.menu.menu_parent_nav_drawer);
        setNavigationViewClickListener(navigationItemSelectedListener);
    }

}
