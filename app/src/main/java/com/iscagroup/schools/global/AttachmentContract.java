package com.iscagroup.schools.global;

import com.iscagroup.schools.base.BaseContract;

/**
 * Created by Raed Saeed on 7/9/2019.
 */
public interface AttachmentContract {
    interface AttachmentView extends BaseContract.BaseView {
        void onUploadDone();

        void onUploadFailed();
    }
}
