package com.iscagroup.schools.global.home;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.AbstractViewHolder;
import com.iscagroup.schools.base.TypeFactory;
import com.iscagroup.schools.dailyplan.ParentDailyPlanActivity;
import com.iscagroup.schools.dailyplan.SchoolPlanActivity;
import com.iscagroup.schools.dailyplan.TeacherDailyPlanActivity;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.login.HomeModel;
import com.iscagroup.schools.note.parent.ParentNoteActivity;
import com.iscagroup.schools.note.school.SchoolNoteActivity;
import com.iscagroup.schools.note.teacher.TeacherNoteActivity;
import com.iscagroup.schools.request.parent.ParentRequestListActivity;
import com.iscagroup.schools.request.school.SchoolRequestListActivity;
import com.iscagroup.schools.request.teacher.TeacherRequestListActivity;
import com.iscagroup.schools.school.bus.BusActivity;

import java.util.List;

/**
 * Created by Raed Saeed on 6/30/2019.
 */
public class HomeAdapter extends RecyclerView.Adapter<AbstractViewHolder<HomeModel>> {
    @SuppressWarnings("unused")
    private static final String TAG = "HomeAdapter";

    private TypeFactory typeFactory = new TypeFactory() {
        @Override
        public int type(BaseData object) {
            return object.getResourceId();
        }

        @Override
        public AbstractViewHolder createViewHolder(View view, int type) {
            switch (type) {
                case 1:
                    return new ParentHomeViewHolder(view);
                case 2:
                    return new TeacherHomeViewHolder(view);
                default:
                    return new ManagementHomeViewHolder(view);
            }
        }
    };

    private List<HomeModel> homeList;

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public AbstractViewHolder<HomeModel> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, parent, false);
        return typeFactory.createViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder<HomeModel> holder, int position) {
        HomeModel model = homeList.get(position);
        if (model != null) {
            holder.bind(model);
        }
    }

    @Override
    public int getItemCount() {
        return (homeList != null) ? homeList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return typeFactory.type(homeList.get(position));
    }

    void setHomeList(List<HomeModel> list) {
        this.homeList = list;
    }

    private int getFeatureResourceId(String id) {
        switch (id) {
            case "1":
                return R.drawable.ic_parent_request;
            case "2":
                return R.drawable.ic_note;
            case "3":
                return R.drawable.ic_newsletter;
            case "4":
                return R.drawable.ic_daily_plan;
            case "5":
                return R.drawable.ic_schedule;
        }
        return R.drawable.ic_home_24dp;
    }

    class ParentHomeViewHolder extends AbstractViewHolder<HomeModel> implements View.OnClickListener {
        private TextView tvItemHomeName;
        private CardView cvItemHomeCard;
        private ImageView ivItemHomeImage;

        ParentHomeViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemHomeName = itemView.findViewById(R.id.tv_item_home_name);
            cvItemHomeCard = itemView.findViewById(R.id.cv_item_home_card_view);
            ivItemHomeImage = itemView.findViewById(R.id.iv_item_home_image);
            cvItemHomeCard.setOnClickListener(this);
        }

        @Override
        public void bind(HomeModel element) {
            tvItemHomeName.setText(element.getFeature());
            ivItemHomeImage.setImageResource(getFeatureResourceId(element.getId()));
        }

        @Override
        public void onClick(View v) {
            HomeModel home = homeList.get(getAdapterPosition());
            Intent intent;

            switch (home.getId()) {
                case "1":
                    intent = new Intent(v.getContext(), ParentRequestListActivity.class);
                    v.getContext().startActivity(intent);
                    break;

                case "2":
                    intent = new Intent(v.getContext(), ParentNoteActivity.class);
                    v.getContext().startActivity(intent);
                    break;

                case "3":
                    break;

                case "4":
                    intent = new Intent(v.getContext(), ParentDailyPlanActivity.class);
                    v.getContext().startActivity(intent);
                    break;

                case "5":
                    break;

                case "6":
                    break;
            }
        }
    }

    class TeacherHomeViewHolder extends AbstractViewHolder<HomeModel> implements View.OnClickListener {
        private TextView tvItemHomeName;
        private CardView cvItemHomeCard;
        private ImageView ivItemHomeImage;

        TeacherHomeViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemHomeName = itemView.findViewById(R.id.tv_item_home_name);
            cvItemHomeCard = itemView.findViewById(R.id.cv_item_home_card_view);
            ivItemHomeImage = itemView.findViewById(R.id.iv_item_home_image);
            cvItemHomeCard.setOnClickListener(this);
        }

        @Override
        public void bind(HomeModel element) {
            tvItemHomeName.setText(element.getFeature());
            ivItemHomeImage.setImageResource(getFeatureResourceId(element.getId()));
        }

        @Override
        public void onClick(View v) {
            HomeModel home = homeList.get(getAdapterPosition());
            Intent intent;

            switch (home.getId()) {
                case "1":
                    intent = new Intent(v.getContext(), TeacherRequestListActivity.class);
                    v.getContext().startActivity(intent);
                    break;

                case "2":
                    intent = new Intent(v.getContext(), TeacherNoteActivity.class);
                    v.getContext().startActivity(intent);
                    break;

                case "3":
                    break;

                case "4":
                    intent = new Intent(v.getContext(), TeacherDailyPlanActivity.class);
                    v.getContext().startActivity(intent);
                    break;

                case "5":
                    break;

                case "6":
                    break;
            }
        }

    }

    class ManagementHomeViewHolder extends AbstractViewHolder<HomeModel> implements View.OnClickListener {
        private TextView tvItemHomeName;
        private CardView cvItemHomeCard;
        private ImageView ivItemHomeImage;

        ManagementHomeViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemHomeName = itemView.findViewById(R.id.tv_item_home_name);
            cvItemHomeCard = itemView.findViewById(R.id.cv_item_home_card_view);
            ivItemHomeImage = itemView.findViewById(R.id.iv_item_home_image);
            cvItemHomeCard.setOnClickListener(this);
        }

        @Override
        public void bind(HomeModel element) {
            tvItemHomeName.setText(element.getFeature());
            ivItemHomeImage.setImageResource(getFeatureResourceId(element.getId()));
        }

        @Override
        public void onClick(View v) {
            HomeModel home = homeList.get(getAdapterPosition());
            Intent intent;

            switch (home.getId()) {
                case "1":
                    intent = new Intent(v.getContext(), SchoolRequestListActivity.class);
                    v.getContext().startActivity(intent);
                    break;

                case "2":
                    intent = new Intent(v.getContext(), SchoolNoteActivity.class);
                    v.getContext().startActivity(intent);
                    break;

                case "3":
                    break;

                case "4":
                    intent = new Intent(v.getContext(), SchoolPlanActivity.class);
                    v.getContext().startActivity(intent);
                    break;

                case "5":
                    break;

                case "6":
                    intent = new Intent(v.getContext(), BusActivity.class);
                    v.getContext().startActivity(intent);
                    break;
            }
        }
    }
}
