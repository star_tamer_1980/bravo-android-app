package com.iscagroup.schools.global.login;

import com.iscagroup.schools.base.BaseContract;
import com.iscagroup.schools.data.login.LoginResponse;

/**
 * Created by Raed Saeed on 6/26/2019.
 */
public interface LoginContract {
    interface LoginView extends BaseContract.BaseView {
        void login();

        void onSuccessfulLoginResponse(LoginResponse loginResponse);
    }

    interface LoginPresenter {
        void login(String code, String phoneNumber, String playerId);
    }
}
