package com.iscagroup.schools.global.login;

import com.iscagroup.schools.base.BasePresenter;
import com.iscagroup.schools.model.remote.LoginService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Raed Saeed on 6/26/2019.
 */
@SuppressWarnings("unused")
public class LoginPresenterImp extends BasePresenter<LoginContract.LoginView> implements LoginContract.LoginPresenter {
    private static final String TAG = "LoginPresenterImp";

    private LoginService loginService;

    LoginPresenterImp() {

    }

    @Override
    protected void onCreate() {
        loginService = LoginService.getInstance();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void login(String code, String phoneNumber, String playerId) {
        getCompositeDisposable().add(loginService.loging(code, phoneNumber, playerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(response ->
                        getView().showProgressBar()
                )
                .doFinally(() -> getView().hideProgressBar())
                .subscribe(response ->
                                getView().onSuccessfulLoginResponse(response)
                        , error ->
                                getView().showFailureMessage(error.getMessage())
                ));
    }
}
