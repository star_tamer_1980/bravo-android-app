package com.iscagroup.schools.global.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.MenuRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.global.login.LoginActivity;
import com.iscagroup.schools.utils.UserSharedPreferences;
import com.onesignal.OneSignal;

/**
 * Created by Raed Saeed on 6/30/2019.
 */


/**
 * This is the basic Navigation Activity for every activity that want
 * a nav drawer, it's content the base design and completely dynamic
 * with Nav drawer header and menu
 */
public class BaseNavActivity extends BaseActivity {

    // Base Drawer Layout
    private DrawerLayout drawerLayout;

    // Base NavigationView that get the header and menu from child activity
    private NavigationView mNavigationView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer);

        Toolbar toolbar = findViewById(R.id.tb_activity_nav_drawer);
        setSupportActionBar(toolbar);


        mNavigationView = findViewById(R.id.nv_activity_nav_drawer_navigation_view);

        drawerLayout = findViewById(R.id.dl_activity_drawer);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
    }

    protected void setNavigationViewHeader(View headerView) {
        mNavigationView.addHeaderView(headerView);
    }

    protected void setNavigationViewMenu(@MenuRes int menuResourceId) {
        mNavigationView.inflateMenu(menuResourceId);
    }

    protected void setNavigationViewClickListener(NavigationView.OnNavigationItemSelectedListener clickListener) {
        mNavigationView.setNavigationItemSelectedListener(clickListener);
    }

    protected void closeDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    protected void logout() {
        UserSharedPreferences.clear(this);
        OneSignal.setSubscription(false);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
