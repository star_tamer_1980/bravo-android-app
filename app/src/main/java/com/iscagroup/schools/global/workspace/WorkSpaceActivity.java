package com.iscagroup.schools.global.workspace;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.login.LoginResponse;
import com.iscagroup.schools.data.login.Result;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.utils.UserSharedPreferences;

import java.util.List;

public class WorkSpaceActivity extends BaseActivity implements WorkSpaceContract.WorkSpaceView {
    @SuppressWarnings("unused")
    private static final String TAG = "WorkSpaceActivity";

    private ProgressBar pbWorkSpaceLoading;
    private WorkSpacePresenterImp workSpacePresenterImp;
    private WorkSpaceAdapter workSpaceAdapter;

    // Received Login Response from Verification Code
    private LoginResponse receivedLoginResponse;

    // Cache object to check if there is data or not
    private List<Result> cacheObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_space);

        Intent intent = getIntent();
        if (intent != null) {
            receivedLoginResponse = intent.getParcelableExtra(StringConstants.EXTRA_LOGIN_RESPONSE);
        }

        initViews();
        initVars();
    }

    private void initViews() {
        Toolbar toolbar = findViewById(R.id.tb_activity_work_space_tool_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.workspace));
        }

        pbWorkSpaceLoading = findViewById(R.id.pb_activity_work_space_loading);
    }

    private void initVars() {
        workSpacePresenterImp = new WorkSpacePresenterImp();
        workSpacePresenterImp.attachView(this);
        workSpacePresenterImp.attachLifecycle(getLifecycle());

        workSpaceAdapter = new WorkSpaceAdapter();
        RecyclerView rvWorkspaceList = findViewById(R.id.rv_activity_work_space_list);
        rvWorkspaceList.setLayoutManager(new LinearLayoutManager(this));
        rvWorkspaceList.setAdapter(workSpaceAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpWorkSpace();
    }


    private void setUpWorkSpace() {
        if (receivedLoginResponse == null && cacheObject == null) {
            getWorkSpace();
        } else {
            hideProgressBar();

            if (receivedLoginResponse != null) {
                showLoginResponse(receivedLoginResponse.getResult());
            }

            if (cacheObject != null) {
                showLoginResponse(cacheObject);
            }
        }
    }

    private void getWorkSpace() {
        String code = UserSharedPreferences.getUserCountryCode(this);
        String phoneNumber = UserSharedPreferences.getUserNationalPhoneNumber(this);
        if (code.length() > 0 && phoneNumber.length() > 0) {
            workSpacePresenterImp.getWorkSpace(code, phoneNumber, UserSharedPreferences.getUserPlayerId(this));
        }
    }

    @Override
    public void showProgressBar() {
        pbWorkSpaceLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        pbWorkSpaceLoading.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showLoginResponse(List<Result> list) {
        cacheObject = list;
        workSpaceAdapter.setLoginResults(list);
    }
}
