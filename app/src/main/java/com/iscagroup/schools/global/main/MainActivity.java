package com.iscagroup.schools.global.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.login.Result;
import com.iscagroup.schools.data.login.School;
import com.iscagroup.schools.data.login.Student;
import com.iscagroup.schools.global.home.ParentHomeActivity;
import com.iscagroup.schools.global.home.SchoolHomeActivity;
import com.iscagroup.schools.global.home.TeacherHomeActivity;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.utils.UserSharedPreferences;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements MainAdapter.OnStudentClickListener, MainAdapter.OnSchoolClickListener, MainAdapter.OnManagementClickListener {

    private Result loginResult;
    private MainAdapter mainAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.tb_activity_main_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.main));
        }


        initViews();
        initVars();

        Intent intent = getIntent();
        if (intent != null) {
            loginResult = intent.getParcelableExtra(StringConstants.EXTRA_LOGIN_DATA);
            setUpItemView();
        }
    }

    private void initViews() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initVars() {
        mainAdapter = new MainAdapter();
        mainAdapter.setSchoolClickListener(this);
        mainAdapter.setStudentClickListener(this);
        mainAdapter.setManagementClickListener(this);

        RecyclerView rvMainList = findViewById(R.id.rv_activity_main_list);
        rvMainList.setLayoutManager(new LinearLayoutManager(this));
        rvMainList.setAdapter(mainAdapter);

    }

    private void setUpItemView() {
        if (loginResult != null) {
            String userType = loginResult.getUserType();
            if (userType.equalsIgnoreCase("1")) {
                List<BaseData> newStudentList = new ArrayList<>();
                for (Student student : loginResult.getStudents()) {
                    student.setResourceId(R.layout.item_main_student);
                    student.setHomeList(loginResult.getHome());
                    newStudentList.add(student);
                }
                mainAdapter.setList(newStudentList);
            } else if (userType.equalsIgnoreCase("2")) {
                List<BaseData> newSchoolList = new ArrayList<>();
                for (School school : loginResult.getSchools()) {
                    school.setResourceId(R.layout.item_main_teacher);
                    school.setHomeList(loginResult.getHome());
                    newSchoolList.add(school);
                }
                mainAdapter.setList(newSchoolList);
            } else {
                List<BaseData> newSchoolList = new ArrayList<>();
                for (School school : loginResult.getSchools()) {
                    school.setResourceId(R.layout.item_main_school);
                    school.setHomeList(loginResult.getHome());
                    newSchoolList.add(school);
                }
                mainAdapter.setList(newSchoolList);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivityWithTransition();
    }

    @Override
    public void onStudentClick(int position, Student student) {
        setUserData(student);
        Intent intent = new Intent(this, ParentHomeActivity.class);
        intent.putExtra(StringConstants.EXTRA_STUDENT_HOME, student);
        intent.putExtra(StringConstants.EXTRA_LOGIN_RESPONSE, loginResult);
        startActivity(intent);
    }

    @Override
    public void onSchoolClick(int position, School school) {
        setUserData(school);
        Intent intent = new Intent(this, TeacherHomeActivity.class);
        intent.putExtra(StringConstants.EXTRA_SCHOOL_HOME, school);
        intent.putExtra(StringConstants.EXTRA_LOGIN_RESPONSE, loginResult);
        startActivity(intent);
    }

    @Override
    public void onManagementClick(int position, School school) {
        setUserData(school);
        Intent intent = new Intent(this, SchoolHomeActivity.class);
        intent.putExtra(StringConstants.EXTRA_SCHOOL_HOME, school);
        intent.putExtra(StringConstants.EXTRA_LOGIN_RESPONSE, loginResult);
        startActivity(intent);
    }

    private void setUserData(Student student) {
        UserSharedPreferences.setUserName(this, student.getFirstName() + " " + student.getLastName());
        UserSharedPreferences.setUserPhoto(this, student.getStudentPhoto());
        UserSharedPreferences.setUserClassName(this, student.getClassName());
        UserSharedPreferences.setUserStudentId(this, student.getId());
        UserSharedPreferences.setUserClassId(this, student.getClassId());
        UserSharedPreferences.setUserSchoolId(this, student.getSchoolId());
        UserSharedPreferences.setUserSchoolLogo(this, student.getSchoolLogo());
    }

    private void setUserData(School school) {
        UserSharedPreferences.setSchoolName(this, school.getName());
        UserSharedPreferences.setUserPhoto(this, school.getLogo());
        UserSharedPreferences.setUserSchoolId(this, school.getId());
        UserSharedPreferences.setUserId(this, school.getUserId());
        UserSharedPreferences.setUserSchoolLogo(this, school.getLogo());
    }
}
