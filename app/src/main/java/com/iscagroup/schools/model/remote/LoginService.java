package com.iscagroup.schools.model.remote;

import com.iscagroup.schools.data.login.LoginResponse;

import io.reactivex.Single;

/**
 * Created by Raed Saeed on 6/26/2019.
 */
public class LoginService extends BaseRetrofit {
    @SuppressWarnings("unused")
    private static final String TAG = "LoginService";

    private LoginService() {
        super();
    }

    public static LoginService getInstance() {
        return new LoginService();
    }

    public Single<LoginResponse> loging(String code, String phoneNumber, String playerId) {
        return getApiServices().login(code, phoneNumber, playerId);
    }
}
