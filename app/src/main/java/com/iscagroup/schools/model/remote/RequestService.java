package com.iscagroup.schools.model.remote;

import com.google.gson.JsonObject;
import com.iscagroup.schools.data.request.RequestResponse;
import com.iscagroup.schools.data.teacher.TeacherResponse;

import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Created by Raed Saeed on 7/4/2019.
 */
public class RequestService extends BaseRetrofit {


    public static RequestService getInstance() {
        return new RequestService();
    }

    public Flowable<RequestResponse> getParentRequest(String studentId, String status, String itemCount, String limit) {
        return getApiServices().getRequest(studentId, status, itemCount, limit);
    }

    public Flowable<TeacherResponse> getTeachers(String classId) {
        return getApiServices().getTeachers(classId);
    }

    public Observable<JsonObject> addRequest(String studentId, String classId, String teacherUserId, String schoolId, String requestFor, String requestBody) {
        return getApiServices().addRequest(studentId, classId, teacherUserId, schoolId, requestFor, requestBody);
    }

    public Flowable<RequestResponse> getTeacherRequest(String userId, String schoolId, String status, String itemCount, String limit) {
        return getApiServices().getTeacherRequest(userId, schoolId, status, itemCount, limit);
    }

    public Flowable<RequestResponse> getSchoolRequest(String schoolId, String status, String itemCount, String limit) {
        return getApiServices().getSchoolRequest(schoolId, status, itemCount, limit);
    }

    public Observable<JsonObject> replyRequestTeacher(String requestId, String replyDescription) {
        return getApiServices().replyRequestTeacher(requestId, replyDescription);
    }

    public Observable<JsonObject> replyRequestSchool(String requestId, String replyDescription) {
        return getApiServices().replyRequestSchool(requestId, replyDescription);
    }
}
