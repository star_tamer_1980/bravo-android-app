package com.iscagroup.schools.model.local;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.iscagroup.schools.data.login.LoginResponseTypConverter;
import com.iscagroup.schools.data.login.Result;
import com.iscagroup.schools.data.note.Note;
import com.iscagroup.schools.data.plan.Plan;
import com.iscagroup.schools.data.request.Request;

/**
 * Created by Raed Saeed on 6/27/2019.
 */
@Database(entities = {Result.class, Request.class, Note.class, Plan.class}, version = 11, exportSchema = false)
@TypeConverters(LoginResponseTypConverter.class)
public abstract class AppDatabase extends RoomDatabase {
    private static final String TAG = "AppDatabase";

    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "schools_database.db";
    private static AppDatabase instance;

    public static AppDatabase getInstance() {
        return instance;
    }

    public static void initializeDataBase(Context context) {
        if (instance == null) {
            synchronized (LOCK) {
                instance = Room.databaseBuilder(context,
                        AppDatabase.class,
                        DATABASE_NAME)
                        .fallbackToDestructiveMigration()
                        .build();
            }
        }
    }

    public abstract LoginResultDao getLoginResultDao();

    public abstract RequestDao getRequestDao();

    public abstract NoteDao getNoteDao();

    public abstract DailyPlanDao getPlanDao();
}
