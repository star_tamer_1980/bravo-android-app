package com.iscagroup.schools.model.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.iscagroup.schools.data.plan.Plan;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;

/**
 * Created by Raed Saeed on 7/31/2019.
 */
@Dao
public interface DailyPlanDao extends BaseDao<Plan> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Maybe<List<Long>> saveDailyPlans(List<Plan> plans);

    @Query("select * from `plan` where teacherUserId = :teacherId and planDate = :planDate")
    Maybe<List<Plan>> getTeacherPlans(String teacherId, String planDate);

    @Query("select * from `plan` where classId =:classId and planDate =:planDate")
    Maybe<List<Plan>> getParentPlans(String classId, String planDate);

    @Query("select * from `plan` where schoolId =:schoolId and planDate =:planDate")
    Maybe<List<Plan>> getSchoolPlans(String schoolId, String planDate);

    @Delete
    Completable deletePlan(Plan plan);

    @Query("delete from `plan` where planId =:planId")
    Completable deletePlan(String planId);

    @Query("delete from `plan` where classId =:classId and planDate =:planDate")
    Completable deletePlan(String classId, String planDate);

    @Query("delete from `plan`")
    Completable deletePlans();
}
