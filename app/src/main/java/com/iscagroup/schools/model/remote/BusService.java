package com.iscagroup.schools.model.remote;

import com.iscagroup.schools.data.driver.DriverResponse;

import io.reactivex.Flowable;

/**
 * Created by Raed Saeed on 8/21/2019.
 */

public class BusService extends BaseRetrofit {

//    @Inject
//    public BusService(Retrofit retrofit) {
//        apiService = retrofit.create(ApiService.class);
//    }

    public static BusService getInstance() {
        return new BusService();
    }

    public Flowable<DriverResponse> getDrivers(String schoolId) {
        return getApiServices().getDrivers(schoolId);
    }
}
