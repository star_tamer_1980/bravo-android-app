package com.iscagroup.schools.model.remote;

import com.google.gson.JsonObject;
import com.iscagroup.schools.data.note.CategoryResponse;
import com.iscagroup.schools.data.note.KidsResponse;
import com.iscagroup.schools.data.note.NoteClassResponse;
import com.iscagroup.schools.data.note.NoteResponse;
import com.iscagroup.schools.data.note.SchoolResponse;

import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Created by Raed Saeed on 7/18/2019.
 */
public class NoteService extends BaseRetrofit {
    private NoteService() {

    }

    public static NoteService getInstance() {
        return new NoteService();
    }

    public Flowable<CategoryResponse> getCategories() {
        return getApiServices().getCategories();
    }

    public Flowable<SchoolResponse> getSchoolResponse(String schoolId) {
        return getApiServices().getSchool(schoolId);
    }

    public Flowable<NoteResponse> getParentNotes(String studentId, String noteType, String itemCount, String limit) {
        return getApiServices().getParentNotes(studentId, noteType, itemCount, limit);
    }

    public Flowable<NoteResponse> getTeacherNotes(String schoolId, String teacherId, String itemCount, String limit) {
        return getApiServices().getTeacherNotes(schoolId, teacherId, itemCount, limit);
    }

    public Flowable<NoteResponse> getSchoolNotes(String schoolId, String itemCount, String limit) {
        return getApiServices().getSchoolNotes(schoolId, itemCount, limit);
    }

    public Flowable<KidsResponse> getKidsResponse(String classId, String schoolId) {
        return getApiServices().getKids(classId, schoolId);
    }

    public Observable<JsonObject> addNote(String note, String addedBy, String categoryId, String schoolId, String stageId, String gradeId, String classId, String kidId) {
        return getApiServices().addNote(note, addedBy, categoryId, schoolId,
                stageId, gradeId, classId, kidId);
    }

    public Flowable<NoteClassResponse> getTeacherClass(String schoolId, String userId) {
        return getApiServices().getTeacherNoteClass(schoolId, userId);
    }

    public Observable<JsonObject> addNote(String note, String addedBy, String categoryId, String schoolId, String classId, String kidId) {
        return getApiServices().addTeacherNote(note, addedBy, categoryId, schoolId, classId, kidId);
    }
}
