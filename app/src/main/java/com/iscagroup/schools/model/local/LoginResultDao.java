package com.iscagroup.schools.model.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.iscagroup.schools.data.login.Result;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;

/**
 * Created by Raed Saeed on 6/27/2019.
 */

@Dao
public interface LoginResultDao extends BaseDao<Result> {

    @Insert
    Maybe<List<Long>> insertResults(List<Result> results);

    @Query("SELECT * FROM login_result")
    Maybe<List<Result>> loadAllLoginResults();

    @Delete
    Maybe<Integer> deletLoginResult(Result result);

    @Delete
    void deleteAllLoginResults(List<Result> list);

    @Query("DELETE FROM login_result")
    Completable deleteLogin();
}
