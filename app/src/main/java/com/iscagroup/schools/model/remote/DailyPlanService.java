package com.iscagroup.schools.model.remote;

import com.google.gson.JsonObject;
import com.iscagroup.schools.data.plan.PlanResponse;

import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Created by Raed Saeed on 7/31/2019.
 */
public class DailyPlanService extends BaseRetrofit {
    @SuppressWarnings("unused")
    private static final String TAG = "DailyPlanService";

    private DailyPlanService() {

    }

    public static DailyPlanService getInstance() {
        return new DailyPlanService();
    }

    public Flowable<PlanResponse> getTeacherPlans(String schoolId, String userId, String date) {
        return getApiServices().getTeacherPlan(schoolId, userId, date);
    }

    public Observable<JsonObject> addPlan(String teacherId, String scheduleId, String plan, String planDate) {
        return getApiServices().addPlan(teacherId, scheduleId, plan, planDate);
    }

    public Observable<JsonObject> updatePlan(String planId, String plan) {
        return getApiServices().updatePlan(planId, plan);
    }

    public Observable<JsonObject> delete(String planId) {
        return getApiServices().deletePlan(planId);
    }

    public Flowable<PlanResponse> getParentPlan(String schoolId, String classId, String date) {
        return getApiServices().getParentPlans(schoolId, classId, date);
    }

    public Flowable<PlanResponse> getSchoolPlan(String schoolId, String date) {
        return getApiServices().getSchoolPlan(schoolId, date);
    }
}
