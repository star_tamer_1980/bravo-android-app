package com.iscagroup.schools.model.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.iscagroup.schools.data.request.Request;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;

/**
 * Created by Raed Saeed on 7/4/2019.
 */
@SuppressWarnings("unused")
@Dao
public interface RequestDao extends BaseDao<Request> {
    /**
     * Insert the returned requests from server into database, if the inserted row
     * exist it will be replaced
     *
     * @param results list of {@link Request}
     * @return list of number of inserted rows
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Maybe<List<Long>> saveRequests(List<Request> results);


    /**
     * Select 10 rows of {@link Request} each time the query run according to studentId
     *
     * @param studentId     of request
     * @param requestStatus the status of requests <br>1</br> for closed, <br>0</br> for open
     * @param offset        the limit of returned rows
     * @return list of 10 elements of {@link Request}
     */

    @Query("SELECT * from requests_table WHERE studentId = :studentId AND reqStatus = :requestStatus LIMIT 10 OFFSET :offset")
    Maybe<List<Request>> getRequests(String studentId, String requestStatus, int offset);

    @Query("SELECT * from requests_table WHERE teacherUserId = :userId AND schoolId = :schoolId AND reqStatus =:requestStatus LIMIT 10 OFFSET :offset")
    Maybe<List<Request>> getTeacherRequest(String userId, String schoolId, String requestStatus, int offset);

    @Query("SELECT * FROM requests_table WHERE schoolId =:schoolId AND reqStatus = :status AND requestFrom = '2' LIMIT 10 OFFSET :offset")
    Maybe<List<Request>> getSchoolRequest(String schoolId, String status, Integer offset);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Maybe<Long> insertRequest(Request request);

    @Delete
    Maybe<Integer> deleteRequest(Request result);

    @Delete
    void deleteRequestList(List<Request> list);

    @Query("DELETE FROM requests_table")
    Completable deleteRequests();

    @Query("DELETE FROM requests_table WHERE id= :requestId")
    Completable deleteRequest(String requestId);

}
