package com.iscagroup.schools.model.local;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;

import io.reactivex.Completable;
import io.reactivex.Maybe;

/**
 * Created by Raed Saeed on 6/27/2019.
 */
@Dao
public interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Maybe<Long> insert(T object);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    Completable update(T object);
}
