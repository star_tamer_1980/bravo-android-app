package com.iscagroup.schools.model.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.iscagroup.schools.data.note.Note;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;

/**
 * Created by Raed Saeed on 7/23/2019.
 */
@Dao
public interface NoteDao extends BaseDao<Note> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Maybe<List<Long>> saveNotes(List<Note> noteList);

    @Query("select * from notes where schoolId =:schoolId and studentId =:studentId and noteType = :noteType limit 10 offset :offset")
    Maybe<List<Note>> getPublicNote(String schoolId, String studentId, String noteType, int offset);

    @Query("select * from notes where schoolId =:schoolId and classId =:classId and studentId =''limit 10 offset :offset")
    Maybe<List<Note>> getClassNotes(String schoolId, String classId, int offset);

    @Query("select * from notes where studentId =:studentId and noteType =:noteType limit 10 offset :offset")
    Maybe<List<Note>> getPrivateNotes(String studentId, String noteType, int offset);

    @Query("select * from notes where schoolId =:schoolId and teacherId =:teacherId limit 10 offset  :offset")
    Maybe<List<Note>> getTeacherNote(String schoolId, String teacherId, int offset);

    @Query("select * from notes where schoolId =:schoolId limit 10 offset :offset")
    Maybe<List<Note>> getSchoolNotes(String schoolId, int offset);

    @Query("delete from notes")
    Completable deleteNotes();

    @Query("delete from notes where id = :id")
    Completable deleteNote(String id);

    @Delete
    Completable deleteNote(Note note);
}
