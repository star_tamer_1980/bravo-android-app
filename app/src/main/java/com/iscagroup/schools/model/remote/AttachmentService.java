package com.iscagroup.schools.model.remote;

import com.google.gson.JsonObject;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Raed Saeed on 7/8/2019.
 */
public class AttachmentService extends BaseRetrofit {

    private AttachmentService() {

    }

    public static AttachmentService getInstance() {
        return new AttachmentService();
    }

    public Observable<JsonObject> addAttachment(RequestBody featureType, RequestBody featureId, RequestBody schoolId, RequestBody isParent, MultipartBody.Part part) {
        return getApiServices().attachFile(featureType, featureId, schoolId, isParent, part);
    }

    public Observable<JsonObject> removeFiles(String ids) {
        return getApiServices().removeFiles(ids);
    }
}
