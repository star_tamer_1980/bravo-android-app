package com.iscagroup.schools.model.remote;

import com.google.gson.JsonObject;
import com.iscagroup.schools.data.driver.DriverResponse;
import com.iscagroup.schools.data.login.LoginResponse;
import com.iscagroup.schools.data.note.CategoryResponse;
import com.iscagroup.schools.data.note.KidsResponse;
import com.iscagroup.schools.data.note.NoteClassResponse;
import com.iscagroup.schools.data.note.NoteResponse;
import com.iscagroup.schools.data.note.SchoolResponse;
import com.iscagroup.schools.data.plan.PlanResponse;
import com.iscagroup.schools.data.request.RequestResponse;
import com.iscagroup.schools.data.teacher.TeacherResponse;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Raed Saeed on 6/26/2019.
 */
interface ApiService {
    @FormUrlEncoded
    @POST("login.php")
    Single<LoginResponse> login(@Field("c_code") String code,
                                @Field("mobile") String phone,
                                @Field("player_id") String playerId);

    /************** Requests APIs **************************************************************/

    @FormUrlEncoded
    @POST("parent/parent-request/parent_request_list.php")
    Flowable<RequestResponse> getRequest(@Field("student_id") String studentId,
                                         @Field("status") String status,
                                         @Field("item_count") String itemCount,
                                         @Field("limit") String limit);


    @FormUrlEncoded
    @POST("teacher/parent-request/parent_request_list.php")
    Flowable<RequestResponse> getTeacherRequest(@Field("user_id") String userId,
                                                @Field("school_id") String schoolId,
                                                @Field("status") String status,
                                                @Field("item_count") String itemCount,
                                                @Field("limit") String limit);

    @FormUrlEncoded
    @POST("parent/parent-request/teachers_list.php")
    Flowable<TeacherResponse> getTeachers(@Field("class_id") String classId);

    @FormUrlEncoded
    @POST("parent/parent-request/add.php")
    Observable<JsonObject> addRequest(@Field("student_id") String studentId,
                                      @Field("class_id") String classId,
                                      @Field("teacher_user_id") String teacherUserId,
                                      @Field("school_id") String schoolId,
                                      @Field("request_from") String requestFor,
                                      @Field("request") String requestBody);


    @Multipart
    @POST("attach_file.php")
    Observable<JsonObject> attachFile(@Part("feature_type") RequestBody featureType,
                                      @Part("feature_id") RequestBody featureId,
                                      @Part("school_id") RequestBody schoolId,
                                      @Part("parent_request") RequestBody isParent,
                                      @Part MultipartBody.Part part);

    @FormUrlEncoded
    @POST("teacher/parent-request/reply.php")
    Observable<JsonObject> replyRequestTeacher(@Field("id") String requestId,
                                               @Field("reply") String replyDescription);

    @FormUrlEncoded
    @POST("sm/parent-request/parent_request_list.php")
    Flowable<RequestResponse> getSchoolRequest(@Field("school_id") String schoolId,
                                               @Field("status") String status,
                                               @Field("item_count") String itemCount,
                                               @Field("limit") String limit);

    @FormUrlEncoded
    @POST("sm/parent-request/reply.php")
    Observable<JsonObject> replyRequestSchool(@Field("id") String requestId,
                                              @Field("reply") String replyDescription);

    /********************************************** End of Request APIs *********************************************/

    @GET("notes/category.php")
    Flowable<CategoryResponse> getCategories();

    @FormUrlEncoded
    @POST("notes/classes.php")
    Flowable<SchoolResponse> getSchool(@Field("school_id") String schoolId);

    @FormUrlEncoded
    @POST("notes/parent/notes_list.php")
    Flowable<NoteResponse> getParentNotes(@Field("student_id") String studentId,
                                          @Field("note_type") String noteType,
                                          @Field("item_count") String itemCount,
                                          @Field("limit") String limit);

    @FormUrlEncoded
    @POST("notes/management/notes_list.php")
    Flowable<NoteResponse> getSchoolNotes(@Field("school_id") String schoolId,
                                          @Field("item_count") String itemCount,
                                          @Field("limit") String limit);

    @FormUrlEncoded
    @POST("notes/teacher/notes_list.php")
    Flowable<NoteResponse> getTeacherNotes(@Field("school_id") String schoolId,
                                           @Field("user_id") String teacherId,
                                           @Field("item_count") String itemCount,
                                           @Field("limit") String limit);

    @FormUrlEncoded
    @POST("notes/class_students.php")
    Flowable<KidsResponse> getKids(@Field("class_id") String classId,
                                   @Field("school_id") String schoolId);

    @FormUrlEncoded
    @POST("notes/management/add.php")
    Observable<JsonObject> addNote(@Field("note") String note,
                                   @Field("added_by") String addedBy,
                                   @Field("category_id") String categoryId,
                                   @Field("school_id") String schoolId,
                                   @Field("stage_id") String stageId,
                                   @Field("grade_id") String gradeId,
                                   @Field("class_id") String classId,
                                   @Field("student_id") String kidId);

    @FormUrlEncoded
    @POST("notes/teacher/teacher_classes.php")
    Flowable<NoteClassResponse> getTeacherNoteClass(@Field("school_id") String schoolId,
                                                    @Field("user_id") String userId);

    @FormUrlEncoded
    @POST("notes/teacher/add.php")
    Observable<JsonObject> addTeacherNote(@Field("note") String note,
                                          @Field("added_by") String addedBy,
                                          @Field("category_id") String categoryId,
                                          @Field("school_id") String schoolId,
                                          @Field("class_id") String classId,
                                          @Field("student_id") String kidId);

    /************************************** End of Notes Api and Start of Plan API ****************************************/

    @FormUrlEncoded
    @POST("plan/teacher/list.php")
    Flowable<PlanResponse> getTeacherPlan(@Field("school_id") String schoolId,
                                          @Field("teacher_id") String userId,
                                          @Field("plan_date") String date);

    @FormUrlEncoded
    @POST("plan/teacher/add.php")
    Observable<JsonObject> addPlan(@Field("teacher_id") String teacherId,
                                   @Field("schedule_id") String scheduleId,
                                   @Field("plan") String plan,
                                   @Field("plan_date") String planDate);

    @FormUrlEncoded
    @POST("plan/teacher/edit.php")
    Observable<JsonObject> updatePlan(@Field("plan_id") String planId,
                                      @Field("plan") String plan);

    @FormUrlEncoded
    @POST("delete-attach.php")
    Observable<JsonObject> removeFiles(@Field("id") String ids);

    @FormUrlEncoded
    @POST("plan/teacher/delete.php")
    Observable<JsonObject> deletePlan(@Field("plan_id") String planId);

    @FormUrlEncoded
    @POST("plan/parent/list.php")
    Flowable<PlanResponse> getParentPlans(@Field("school_id") String schoolId,
                                          @Field("class_id") String classId,
                                          @Field("plan_date") String date);

    @FormUrlEncoded
    @POST("plan/management/list.php")
    Flowable<PlanResponse> getSchoolPlan(@Field("school_id") String schoolId,
                                         @Field("plan_date") String date);

    @FormUrlEncoded
    @POST("bus-tracker/list-buses.php")
    Flowable<DriverResponse> getDrivers(@Field("school_id") String schoolId);
}
