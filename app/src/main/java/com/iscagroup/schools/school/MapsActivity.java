package com.iscagroup.schools.school;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.iscagroup.schools.BuildConfig;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.driver.Driver;
import com.iscagroup.schools.data.location.LocationResponse;
import com.iscagroup.schools.utils.StringConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback {
    private static final String TAG = "MapsActivity";

    private GoogleMap mMap;

    // default map zoom ratio
    private float currentZoom = 17;

    // marker holder
    private HashMap<String, Marker> mMarkers = new HashMap<>();

    // requested driver
    private Driver driver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Intent intent = getIntent();

        if (intent != null) {
            driver = intent.getParcelableExtra(StringConstants.EXTRA_DRIVER);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMaxZoomPreference(24);
        mMap.setIndoorEnabled(true);
        mMap.setTrafficEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setOnCameraMoveListener(() -> {
            CameraPosition cameraPosition = mMap.getCameraPosition();
            currentZoom = cameraPosition.zoom;
        });
        setFirebaseOptions();
    }

    private void setFirebaseOptions() {
        boolean trackerAppHasBeenInitialized = false;
        FirebaseOptions firebaseOptions = new FirebaseOptions.Builder()
                .setApplicationId(BuildConfig.TRACKER_APP_ID)
                .setApiKey(BuildConfig.API_KEY)
                .setDatabaseUrl(BuildConfig.DATABASE_URL)
                .build();

        List<FirebaseApp> myApps = FirebaseApp.getApps(this);
        for (FirebaseApp app : myApps) {
            if (app.getName().equals("secondary")) {
                trackerAppHasBeenInitialized = true;
            }
        }

        if (!trackerAppHasBeenInitialized) {
            FirebaseApp.initializeApp(this, firebaseOptions, "secondary");
        }

        FirebaseDatabase trackerDatabase = FirebaseDatabase.getInstance(FirebaseApp.getInstance("secondary"));

        String trackerPath = "Locations/" + driver.getFirebaseId();
        DatabaseReference trackerDatabaseRefrence = trackerDatabase.getReference(trackerPath);

        trackerDatabaseRefrence.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                setMarker(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w(TAG, "onCancelled: " + databaseError.getMessage());
            }
        });
    }

    private void setMarker(DataSnapshot dataSnapshot) {
        // When a location update is received, put or update
        // its value in mMarkers, which contains all the markers
        // for locations received, so that we can build the
        // boundaries required to show them all on the map at once
        String key = dataSnapshot.getKey();
        LocationResponse locationObject = dataSnapshot.getValue(LocationResponse.class);

        if (locationObject != null) {
            double lat = locationObject.getLatitude();
            double lng = locationObject.getLongitude();

            LatLng latLngLocation = new LatLng(lat, lng);
            if (!mMarkers.containsKey(key)) {
                mMarkers.put(key, mMap.addMarker(new MarkerOptions()
                        .title(key)
                        .position(latLngLocation)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_bus))));
            } else {
                Objects.requireNonNull(mMarkers.get(key)).setPosition(latLngLocation);
            }

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : mMarkers.values()) {
                builder.include(marker.getPosition());
            }

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngLocation, currentZoom));
        }

    }
}
