package com.iscagroup.schools.school.bus;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.AbstractViewHolder;
import com.iscagroup.schools.base.TypeFactory;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.driver.Driver;
import com.iscagroup.schools.school.MapsActivity;
import com.iscagroup.schools.utils.StringConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 8/21/2019.
 */
public class DriverAdapter extends RecyclerView.Adapter<AbstractViewHolder<BaseData>> {

    private List<BaseData> items;

    private TypeFactory typeFactory = new TypeFactory() {
        @Override
        public int type(BaseData object) {
            return R.layout.item_driver;
        }

        @Override
        public AbstractViewHolder createViewHolder(View view, int viewType) {
            return new DriverViewHolder(view);
        }
    };

    public DriverAdapter() {

    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public AbstractViewHolder<BaseData> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return typeFactory.createViewHolder(view, viewType);

    }

    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder<BaseData> holder, int position) {
        BaseData item = items.get(position);
        if (item != null) {
            holder.bind(item);
        }
    }

    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return typeFactory.type(items.get(position));
    }

    public void load(List<BaseData> dataList) {
        this.items = dataList;
        if (items != null) {
            this.notifyDataSetChanged();
        }
    }

    public void setItems(List<Driver> driverList) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.clear();
        items.addAll(driverList);
        notifyDataSetChanged();
    }

    class DriverViewHolder extends AbstractViewHolder<Driver> {

        private CardView cvDriverCard;
        private TextView tvDriverName;
        private TextView tvDriverBusNumber;

        DriverViewHolder(View itemView) {
            super(itemView);
            cvDriverCard = itemView.findViewById(R.id.cv_item_driver_card);
            cvDriverCard.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), MapsActivity.class);
                intent.putExtra(StringConstants.EXTRA_DRIVER, items.get(getAdapterPosition()));
                v.getContext().startActivity(intent);
            });
            tvDriverName = itemView.findViewById(R.id.tv_item_driver_name);
            tvDriverBusNumber = itemView.findViewById(R.id.tv_item_driver_bus_number);
        }

        @Override
        public void bind(Driver element) {
            tvDriverName.setText(element.getDriverName());
            tvDriverBusNumber.setText(element.getBusNo());
        }
    }
}