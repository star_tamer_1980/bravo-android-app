package com.iscagroup.schools.school.bus;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.driver.DriverResponse;
import com.iscagroup.schools.utils.UserSharedPreferences;

import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

public class BusActivity extends BaseActivity {
    private BusViewModel busViewModel;
    private DriverAdapter driverAdapter;
    private ProgressBar pbBusActivityLoading;
    private SwipeRefreshLayout srlBusActivityRefreshLayout;
    private boolean hasData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus);

        Toolbar tbActivityBus = findViewById(R.id.tb_activity_bus_toolbar);
        setSupportActionBar(tbActivityBus);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(R.string.drivers);
        }

        busViewModel = ViewModelProviders.of(this).get(BusViewModel.class);
        initViews();
        getDriverBusList();
    }

    private void initViews() {
        driverAdapter = new DriverAdapter();

        RecyclerView rvDriverList = findViewById(R.id.rv_activity_driver_list);
        rvDriverList.setLayoutManager(new LinearLayoutManager(this));
        rvDriverList.setAdapter(driverAdapter);

        pbBusActivityLoading = findViewById(R.id.pb_activity_bus_loading);
        srlBusActivityRefreshLayout = findViewById(R.id.srl_activity_bus_refresh_layout);
        srlBusActivityRefreshLayout.setOnRefreshListener(this::getDriverBusList);
    }

    @Override
    public void hideProgressBar() {
        pbBusActivityLoading.setVisibility(View.GONE);
    }

    @Override
    public void showProgressBar() {
        if (!hasData) pbBusActivityLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void startRefreshing() {
        if (hasData) srlBusActivityRefreshLayout.setRefreshing(true);
    }

    @Override
    public void stopRefreshing() {
        srlBusActivityRefreshLayout.setRefreshing(false);
    }

    @SuppressLint("CheckResult")
    private void getDriverBusList() {
        busViewModel.getDrivers(UserSharedPreferences.getUserSchoolId(this))
                .subscribeOn(io())
                .observeOn(mainThread())
                .doOnSubscribe(subscription -> {
                    showProgressBar();
                    startRefreshing();
                })
                .doFinally(() -> {
                    hideProgressBar();
                    stopRefreshing();
                })
                .flatMapIterable(DriverResponse::getDrivers)
                .map(driver -> {
                    driver.setResourceId(R.layout.item_driver);
                    return driver;
                })
                .toList()
                .subscribe(driverList -> {
                    hasData = driverList.size() != 0;
                    driverAdapter.setItems(driverList);
                }, error -> {
                    showFailureMessage(error.toString());
                    hideProgressBar();
                });
    }
}
