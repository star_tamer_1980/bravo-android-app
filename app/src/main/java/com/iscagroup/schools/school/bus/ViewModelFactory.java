package com.iscagroup.schools.school.bus;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * Created by Raed Saeed on 8/21/2019.
 */
public class ViewModelFactory<T extends ViewModel> extends ViewModelProvider.NewInstanceFactory {

    private T viewModel;

    ViewModelFactory(T viewModel) {
        this.viewModel = viewModel;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <V extends ViewModel> V create(@NonNull Class<V> modelClass) {
        return (V) viewModel;
    }
}
