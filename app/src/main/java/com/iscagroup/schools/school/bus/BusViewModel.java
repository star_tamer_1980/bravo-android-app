package com.iscagroup.schools.school.bus;

import androidx.lifecycle.ViewModel;

import com.iscagroup.schools.data.driver.DriverResponse;
import com.iscagroup.schools.model.remote.BusService;

import io.reactivex.Flowable;

/**
 * Created by Raed Saeed on 8/21/2019.
 */
public class BusViewModel extends ViewModel {
    private BusService busService;

    public BusViewModel() {
        busService = BusService.getInstance();
    }

    Flowable<DriverResponse> getDrivers(String schoolId) {
        return busService.getDrivers(schoolId);
    }
}
