package com.iscagroup.schools.base;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleRegistry;

import com.esafirm.imagepicker.features.ImagePicker;
import com.iscagroup.schools.R;

import static com.iscagroup.schools.dialog.BottomFilePickerDialog.DOCUMENT_REQUEST_CODE;

/**
 * Created by Raed Saeed on 6/19/2019.
 */
public abstract class BaseDialogFragment extends AppCompatDialogFragment implements BaseContract.BaseView {
    @SuppressWarnings("unused")
    private static final String TAG = "BaseDialogFragment";
    private LifecycleRegistry lifecycleRegistry;

    private Context context;

    //Dialog Click listener to respond to dialog clicks
    private DialogClickListener clickListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        if (lifecycleRegistry == null) {
            lifecycleRegistry = new LifecycleRegistry(this);
            return lifecycleRegistry;
        }

        return lifecycleRegistry;
    }

    public LifecycleRegistry getLifecycleRegistry() {
        if (lifecycleRegistry == null) {
            lifecycleRegistry = new LifecycleRegistry(this);
            return lifecycleRegistry;
        }

        return lifecycleRegistry;
    }

    @Override
    public void startRefreshing() {

    }

    @Override
    public void stopRefreshing() {

    }

    @Override
    public void hideProgressBar() {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void showFailureMessage(String message) {

    }

    @Override
    public void showNoData() {

    }

    @Override
    public boolean isConnected() {
        return false;
    }

    protected DialogClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(DialogClickListener clickListener) {
        this.clickListener = clickListener;
    }

    /**
     * Show a toast message
     */
    protected void showToast(String message) {
        if (message != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * finish the activity from dialog
     */
    protected void finish() {
        if (getActivity() != null) {
            this.dismiss();
            getActivity().finish();
        }
    }

    protected void startDocumentPicker() {
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType("application/pdf");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(intent, DOCUMENT_REQUEST_CODE);
        } else {
            intent = new Intent(Intent.ACTION_PICK);
            intent.setType("application/*");
            startActivityForResult(Intent.createChooser(intent, "Select File"), DOCUMENT_REQUEST_CODE);
        }

    }

    protected void startImagePicker(int size) {
        ImagePicker.create(this)
                .limit(size)
                .start();
    }

    private boolean shouldHandleDocument(int requestCode, int resultCode, Intent data) {
        return resultCode == Activity.RESULT_OK
                && requestCode == DOCUMENT_REQUEST_CODE
                && data != null;
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    protected void revealShow(View dialogView, boolean b, final Dialog dialog) {

        final View view = dialogView.findViewById(R.id.cl_dialog_file_picker);

        int w = view.getWidth();
        int h = view.getHeight();

        int endRadius = (int) Math.hypot(w, h);


// get the center for the clipping circle
        int cx = (view.getLeft() + view.getRight()) / 2;
        int cy = (view.getTop() + view.getBottom()) / 2;

// get the final radius for the clipping circle
        int finalRadius = Math.max(view.getWidth(), view.getHeight());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (b) {
                Animator revealAnimator = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, endRadius);

                view.setVisibility(View.VISIBLE);
                revealAnimator.setDuration(300);
                revealAnimator.start();

            } else {
                Animator anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, endRadius, 0);
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        dialog.dismiss();
                        view.setVisibility(View.INVISIBLE);

                    }
                });
                anim.setDuration(300);
                anim.start();
            }
        }

    }

    public interface DialogClickListener {
        void onDialogClick(View view);
    }
}
