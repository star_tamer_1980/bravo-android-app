package com.iscagroup.schools.base;

import android.view.View;
import android.view.ViewGroup;

import com.iscagroup.schools.data.BaseData;

/**
 * Created by Raed Saeed on 7/21/2019.
 */
public interface SpinnerTypeFactory {
    int type(BaseData baseData);

    View createView(int position, View convertView, ViewGroup parent);
}
