package com.iscagroup.schools.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleRegistry;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.io.Files;
import com.iscagroup.schools.R;
import com.iscagroup.schools.adapter.FileAdapter;
import com.iscagroup.schools.data.request.Attach;
import com.iscagroup.schools.di.ActivityComponent;
import com.iscagroup.schools.di.DaggerActivityComponent;
import com.iscagroup.schools.di.RetrofitModule;
import com.iscagroup.schools.dialog.LoadingDialog;
import com.iscagroup.schools.global.AttachmentContract;
import com.iscagroup.schools.global.AttachmentPresenter;
import com.iscagroup.schools.receiver.ConnectivityChangeReceiver;
import com.iscagroup.schools.utils.CompressionUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

import static com.iscagroup.schools.dialog.BottomFilePickerDialog.DOCUMENT_REQUEST_CODE;
import static com.iscagroup.schools.utils.RealPathUtil.getFilePathFromURI;

/**
 * Created by Raed Saeed on 6/18/2019.
 */
@SuppressWarnings("unused")
public abstract class BaseActivity extends AppCompatActivity implements BaseContract.BaseView, ConnectivityChangeReceiver.OnConnectivityChangedListener, AttachmentContract.AttachmentView {
    @SuppressWarnings("unused")
    private static final String TAG = "BaseActivity";

    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;

    // handler to handle hide and show runnable threads
    private final Handler mHideHandler = new Handler();
    // This is a compositeDisposable to enable working with MVVM Architecture pattern
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();
    /**
     * Basic Tool bar to use as Toolbar view holder to set it's @{Toolbar.setVisibility(VISIBLE|GONE)}that can be assigned from
     * setSupportActionBar() method.
     */
    private Toolbar toolbar;
    /**
     * runnable thread to show the toolbar
     */
    private final Runnable mShowPart2Runnable = () -> {
        // Delayed display of UI elements
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
        }

        if (toolbar != null) {
            toolbar.setVisibility(View.VISIBLE);
        }
    };
    // the view that should be in full screen mode
    private View mContentView;
    // runnable thread to hide toolbar
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            if (mContentView != null) {
                mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
            }
        }
    };
    // boolean variable to check whether toolbar visible or not
    private boolean mVisible;
    // thread to hide toolbar
    private final Runnable mHideRunnable = this::hide;
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    @SuppressLint("ClickableViewAccessibility")
    private final View.OnTouchListener mDelayHideTouchListener = (view, motionEvent) -> {
        if (AUTO_HIDE) {
            delayedHide();
        }
        return false;
    };
    private LifecycleRegistry lifecycleRegistry;
    // Custom loading dialog
    private LoadingDialog loadingDialog;
    // Connectivity BroadCastReceiver to check the connections
    private ConnectivityChangeReceiver connectivityChangeReceiver;
    // Snack bar for connection error
    private Snackbar connectionSnackBar;
    //Snack bar for Failure response
    private Snackbar snackbar;
    // File Adapter to select files and upload it
    private FileAdapter fileAdapter = new FileAdapter();
    // attachmentPresenter to upload files
    private AttachmentPresenter attachmentPresenter;

    // I was planing to use dagger :)
    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // start connectivity broad cast receiver
        initDagger();
        startConnectivityReceiver();
        initVars();
    }

    public ActivityComponent getDaggerFragmentComponent() {
        return activityComponent;
    }

    protected void initDagger() {
        activityComponent = DaggerActivityComponent
                .builder()
                .retrofitModule(new RetrofitModule())
                .build();
    }

    /**
     * Set action bar
     * <p>
     * The toolbar is assigned to another tool in case if we want to use a full screen activity
     *
     * @param toolbar received toolbar from child activities.
     */

    @Override
    public void setSupportActionBar(@Nullable Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
        this.toolbar = toolbar;

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    /**
     * This method is the super method for children
     *
     * @param item menu item to check id
     * @return if the menu item consumed in this activity or not
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finishActivityWithTransition();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * The setter of mContent View of child activities
     *
     * @param mContentView the view that should be handled to fit all the screen.
     */
    protected void setTempContentView(View mContentView) {
        this.mContentView = mContentView;
    }

    /**
     * Initialize Attachment Presenter to upload files
     */
    private void initVars() {
        attachmentPresenter = new AttachmentPresenter();
        attachmentPresenter.attachView(this);
        attachmentPresenter.attachLifecycle(getLifecycle());
    }

    // Lifecycle that is used by every presenter if we use MVP architecture pattern
    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        if (lifecycleRegistry == null) {
            lifecycleRegistry = new LifecycleRegistry(this);
            return lifecycleRegistry;
        }

        return lifecycleRegistry;
    }

    public LifecycleRegistry getLifecycleRegistry() {
        if (lifecycleRegistry == null) {
            lifecycleRegistry = new LifecycleRegistry(this);
            return lifecycleRegistry;
        }

        return lifecycleRegistry;
    }

    // return file adapter that contain all files to upload
    protected FileAdapter getFileAdapter() {
        return fileAdapter;
    }

    // return attachmentPresenter to upload files
    public AttachmentPresenter getAttachmentPresenter() {
        return attachmentPresenter;
    }

    /**
     * Show a progress dialog
     *
     * @param message that showing in loading dialog
     */
    protected void showProgressDialog(String message) {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialog.getInstance(message);
        }
        loadingDialog.show(getSupportFragmentManager(), null);
    }

    protected void showProgressDialog(@StringRes int resourceId) {
        if (resourceId != 0) {
            showProgressDialog(getString(resourceId));
        }
    }

    /**
     * dismiss loading dialog
     */
    protected void hideProgressDialog() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }

    /**
     * Connection error snack bar
     *
     * @param view the parent view for snack bar
     */
    public void showConnectionErrorMessage(View view) {
        String message = "Connection lost, Please check your connection.";
        if (connectionSnackBar == null) {
            connectionSnackBar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
            connectionSnackBar.show();
        }
    }


    /**
     * dismiss snack bar if it showing
     */
    public void dismissConnectionErrorMessage() {
        if (connectionSnackBar != null && connectionSnackBar.isShown()) {
            connectionSnackBar.dismiss();
        }
    }


    /**
     * Show a Toast message
     */
    protected void showToast(String message) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    protected void showToast(@StringRes int resource) {
        showToast(getString(resource));
    }

    /**
     * show a warning log message
     */
    protected void log(String message) {
        Log.w(TAG, "log: " + message);
    }

//    /**
//     * if the softKeyBoard is visible this method should hide it
//     * when the user touch any part of the screen
//     *
//     * @param event the touch event
//     * @return the super method
//     */
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent event) {
//        if (event.getAction() == MotionEvent.ACTION_DOWN) {
//            View v = getCurrentFocus();
//            if (v instanceof EditText) {
//                Rect outRect = new Rect();
//                v.getGlobalVisibleRect(outRect);
//                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
//                    v.clearFocus();
//                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                }
//            }
//        }
//        return super.dispatchTouchEvent(event);
//    }

    private void startConnectivityReceiver() {
        connectivityChangeReceiver = new ConnectivityChangeReceiver(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(connectivityChangeReceiver, intentFilter);
    }

    /**
     * This method is the implementation of {@link com.iscagroup.schools.base.BaseContract.BaseView}
     * we call this method every time we try to fetch a network request
     *
     * @return true if is connected, false if not
     */
    @SuppressWarnings("Deprecation")
    @Override
    public boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if (Build.VERSION.SDK_INT < 23) {
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo != null) {
                    return (networkInfo.isConnected() && networkInfo.getType() == ConnectivityManager.TYPE_MOBILE
                            || networkInfo.getType() == ConnectivityManager.TYPE_WIFI);
                }
            } else {
                Network network = connectivityManager.getActiveNetwork();
                if (network != null) {
                    NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
                    return (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI));
                }
            }
        }
        return false;
    }

    // To override inside children activities
    @Override
    public void startRefreshing() {

    }

    // To override inside children activities
    @Override
    public void stopRefreshing() {

    }

    // Called from presenter onFailure method
    @Override
    public void showFailureMessage(String message) {
        Log.w(TAG, "showFailureMessage: " + message);
        showToast(getString(R.string.connection_interruption));
    }

    // To override inside children activities
    @Override
    public void hideProgressBar() {

    }

    // To override inside children activities
    @Override
    public void showProgressBar() {

    }

    // To override inside children activities
    @Override
    public void showNoData() {

    }

    /**
     * To override inside children activities and check if the connection available or not
     * this method is triggered each time the connection change whether the device is connected or not
     *
     * @param isConnected true if devices connected to internet, false if device is not connected
     */
    @Override
    public void onConnectivityChanged(boolean isConnected) {

    }

    /**
     * Build and Alert Dialog to notify user for something happened and needs it's interface
     * @param title alert dialog title
     * @param message alert dialog message
     * @param action alert dialog action ok
     * @return AlertDialog Builder
     */
    private AlertDialog.Builder buildAlertDialog(String title, String message, DialogInterface.OnClickListener action) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.alertDialogTheme);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", action);
        return builder;
    }

    public void showAlertDialog(String title, String message, DialogInterface.OnClickListener actionOk) {
        buildAlertDialog(title, message, actionOk).show();
    }

    public void showAlertDialog(String title, String message, DialogInterface.OnClickListener actionOk, DialogInterface.OnClickListener actionCancel) {
        buildAlertDialog(title, message, actionOk)
                .setNegativeButton("Cancel", actionCancel)
                .show();
    }

    public void showAlertDialog(String title, String message, DialogInterface.OnClickListener actionOk, DialogInterface.OnClickListener actionNeutral, DialogInterface.OnClickListener actionCancel) {
        buildAlertDialog(title, message, actionOk)
                .setNegativeButton("Cancel", actionCancel)
                .setNeutralButton("Discard", actionNeutral)
                .show();
    }

    // start document picker e.g (pdf files)
    protected void startDocumentPicker() {
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType("application/pdf");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(intent, DOCUMENT_REQUEST_CODE);
        } else {
            intent = new Intent(Intent.ACTION_PICK);
            intent.setType("application/*");
            startActivityForResult(Intent.createChooser(intent, "Select File"), DOCUMENT_REQUEST_CODE);
        }

    }

    // start image and camera photo picker
    protected void startImagePicker(int size) {
        ImagePicker.create(this)
                .limit(size)
                .start();
    }

    private boolean shouldHandleDocument(int requestCode, int resultCode, Intent data) {
        return resultCode == Activity.RESULT_OK
                && requestCode == DOCUMENT_REQUEST_CODE
                && data != null;
    }

    // called as a result of FilePicker or ImagePicker
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (fileAdapter == null) {
            fileAdapter = new FileAdapter();
        }

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            List<Image> list = ImagePicker.getImages(data);
            for (int i = 0; i < list.size(); i++) {
                final String path = list.get(i).getPath();                                               // get the file path
                String compressedPath = new CompressionUtil(this).compressImage(path);           // compress the file
                File file = new File(compressedPath);                                                    // create new file of compressed path
                Attach uploadModel = new Attach(file.getName(), path, true);                     // create attach object contains file name and path
                uploadModel.setResourceId(R.layout.item_file);                                           // add file layout to the attach object
                uploadModel.setExtension(Files.getFileExtension(file.getName()));                        // add file extension to attach object
                fileAdapter.loadItem(uploadModel);                                                       // insert the attach object to file adapter and update list
            }

        } else if (shouldHandleDocument(requestCode, resultCode, data)) {
            // Set the attachment
            if (data != null) {
                String path = getFilePathFromURI(this, data.getData());
                File file = new File(path);
                Attach uploadModel = new Attach(file.getName(), path, false);
                uploadModel.setResourceId(R.layout.item_file);
                uploadModel.setExtension(Files.getFileExtension(file.getName()));
                fileAdapter.loadItem(uploadModel);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right_to_left, R.anim.slide_in_left);
    }

    protected void finishActivityWithTransition() {
        finish();
        overridePendingTransition(R.anim.slide_out_left, R.anim.slide_from_left_to_right);
    }

    protected void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        toolbar.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    protected void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    private void delayedHide() {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, BaseActivity.AUTO_HIDE_DELAY_MILLIS);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(connectivityChangeReceiver);
        compositeDisposable.clear();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivityWithTransition();
    }

    @Override
    public void onUploadDone() {

    }

    @Override
    public void onUploadFailed() {

    }

    protected List<Attach> filterDownFiles(List<Attach> list) {
        List<Attach> attaches = new ArrayList<>();
        if (list != null) {
            for (Attach attach : list) {
                attach.setResourceId(R.layout.item_downloadable_file);
                attaches.add(attach);
            }
        }
        return attaches;
    }

    protected List<Attach> filterModifyFiles(List<Attach> list) {
        List<Attach> attaches = new ArrayList<>();
        if (list != null) {
            for (Attach attach : list) {
                attach.setResourceId(R.layout.item_file);
                attaches.add(attach);
            }
        }
        return attaches;
    }
}

