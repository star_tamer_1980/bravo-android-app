package com.iscagroup.schools.base;

import androidx.lifecycle.Lifecycle;

/**
 * Created by Raed Saeed on 6/18/2019.
 */
public interface BaseContract {
    interface BaseView {
        void startRefreshing();

        void stopRefreshing();

        void hideProgressBar();

        void showProgressBar();

        void showFailureMessage(String message);

        void showNoData();

        boolean isConnected();
    }

    interface BasePresenter<V extends BaseView> {
        void attachLifecycle(Lifecycle lifecycle);

        void detachLifecycle(Lifecycle lifecycle);

        void attachView(V view);

        void detachView();

        V getView();
    }
}
