package com.iscagroup.schools.base;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleRegistry;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.snackbar.Snackbar;
import com.iscagroup.schools.R;
import com.iscagroup.schools.adapter.FileAdapter;
import com.iscagroup.schools.data.request.Attach;
import com.iscagroup.schools.dialog.LoadingDialog;
import com.iscagroup.schools.receiver.ConnectivityChangeReceiver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static com.iscagroup.schools.dialog.BottomFilePickerDialog.DOCUMENT_REQUEST_CODE;
import static com.iscagroup.schools.utils.RealPathUtil.getFilePathFromURI;

/**
 * Created by Raed Saeed on 7/1/2019.
 */
@SuppressWarnings("unused")
public abstract class BaseFragment extends Fragment implements BaseContract.BaseView {
    private static final String TAG = "BaseFragment";

    private LifecycleRegistry lifecycleRegistry;
    private LoadingDialog loadingDialog;

    // Connectivity BroadCastReceiver to check the connections
    private ConnectivityChangeReceiver connectivityChangeReceiver;

    // Snack bar for connection error
    private Snackbar connectionSnackBar;

    //Snack bar for Failure response
    private Snackbar snackbar;

    // File Adapter to select files and upload it
    private FileAdapter fileAdapter;

    // List of files to upload
    private List<Attach> filesToUpload;

    private Context context;

    // Name of fragment if it within a viewpager
    private String name;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

//    /**
//     * This method is the super method for children
//     *
//     * @param item menu item to check id
//     * @return if the menu item consumed in this activity or not
//     */
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == android.R.id.home) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        if (lifecycleRegistry == null) {
            lifecycleRegistry = new LifecycleRegistry(this);
            return lifecycleRegistry;
        }

        return lifecycleRegistry;
    }

    public LifecycleRegistry getLifecycleRegistry() {
        if (lifecycleRegistry == null) {
            lifecycleRegistry = new LifecycleRegistry(this);
            return lifecycleRegistry;
        }

        return lifecycleRegistry;
    }

    public Context getContext() {
        return context;
    }

    /**
     * Get fragment name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the fragment name in order to use it from viewPager
     *
     * @param name fragment name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Show a progress dialog
     *
     * @param message that showing in loading dialog
     */
    protected void showProgressDialog(String message) {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialog.getInstance(message);
            loadingDialog.show(getChildFragmentManager(), null);
        }
    }

    /**
     * dismiss loading dialog
     */
    protected void hideProgressDialog() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }

    /**
     * Connection error snack bar
     *
     * @param view the parent view for snack bar
     */
    public void showConnectionErrorMessage(View view) {
        String message = "Connection lost, Please check your connection.";
        if (connectionSnackBar == null) {
            connectionSnackBar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
            connectionSnackBar.show();
        }
    }


    /**
     * dismiss snack bar if it showing
     */
    public void dismissConnectionErrorMessage() {
        if (connectionSnackBar != null && connectionSnackBar.isShown()) {
            connectionSnackBar.dismiss();
        }
    }


    /**
     * Show a Toast message
     */
    protected void showToast(String message) {
        if (message != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * show a warning log message
     */
    protected void log(String message) {
        Log.w(TAG, "log: " + message);
    }

//    /**
//     * if the softKeyBoard is visible this method should hide it
//     * when the user touch any part of the screen
//     *
//     * @param event the touch event
//     * @return the super method
//     */
//    public boolean dispatchTouchEvent(MotionEvent event) {
//        if (event.getAction() == MotionEvent.ACTION_DOWN) {
//            View v = getConte.getCurrentFocus();
//            if (v instanceof EditText) {
//                Rect outRect = new Rect();
//                v.getGlobalVisibleRect(outRect);
//                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
//                    v.clearFocus();
//                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                }
//            }
//        }
//        return super.dispatchTouchEvent(event);
//    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if (Build.VERSION.SDK_INT < 23) {
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo != null) {
                    return (networkInfo.isConnected() && networkInfo.getType() == ConnectivityManager.TYPE_MOBILE
                            || networkInfo.getType() == ConnectivityManager.TYPE_WIFI);
                }
            } else {
                Network network = connectivityManager.getActiveNetwork();
                if (network != null) {
                    NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
                    return (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI));
                }
            }
        }
        return false;
    }

    @Override
    public void startRefreshing() {

    }

    @Override
    public void stopRefreshing() {

    }

    @Override
    public void showFailureMessage(String message) {
        Log.w(TAG, "showFailureMessage: " + message);
        showToast(getString(R.string.connection_interruption));
    }

    @Override
    public void hideProgressBar() {

    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void showNoData() {

    }

    private AlertDialog.Builder buildAlertDialog(String title, String message, DialogInterface.OnClickListener action) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.alertDialogTheme);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", action);
        return builder;
    }

    public void showAlertDialog(String title, String message, DialogInterface.OnClickListener actionOk) {
        buildAlertDialog(title, message, actionOk).show();
    }

    public void showAlertDialog(String title, String message, DialogInterface.OnClickListener actionOk, DialogInterface.OnClickListener actionCancel) {
        buildAlertDialog(title, message, actionOk)
                .setNegativeButton("Cancel", actionCancel)
                .show();
    }

    public void showAlertDialog(String title, String message, DialogInterface.OnClickListener actionOk, DialogInterface.OnClickListener actionNeutral, DialogInterface.OnClickListener actionCancel) {
        buildAlertDialog(title, message, actionOk)
                .setNegativeButton("Cancel", actionCancel)
                .setNeutralButton("Discard", actionNeutral)
                .show();
    }

    protected void startAttachPicker() {
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType("application/pdf");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(intent, DOCUMENT_REQUEST_CODE);
        } else {
            intent = new Intent(Intent.ACTION_PICK);
            intent.setType("application/*");
            startActivityForResult(Intent.createChooser(intent, "Select File"), DOCUMENT_REQUEST_CODE);
        }

    }

    protected void startImagePicker(int size) {
        ImagePicker.create(this)
                .limit(size)
                .start();
    }

    private boolean shouldHandleAttach(int requestCode, int resultCode, Intent data) {
        return resultCode == Activity.RESULT_OK
                && requestCode == DOCUMENT_REQUEST_CODE
                && data != null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (fileAdapter == null) {
            fileAdapter = new FileAdapter();
        }

        if (filesToUpload == null) {
            filesToUpload = new ArrayList<>();
        }

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            List<Image> list = ImagePicker.getImages(data);
            for (int i = 0; i < list.size(); i++) {
                final String path = list.get(i).getPath();
                File file = new File(path);
                Attach uploadModel = new Attach(file.getName(), path, true);
                filesToUpload.add(uploadModel);
                fileAdapter.notifyItemInserted(filesToUpload.size() - 1);
            }

        } else if (shouldHandleAttach(requestCode, resultCode, data)) {
            // Set the attachment
            if (data != null) {
                String path = getFilePathFromURI(context, data.getData());
                File file = new File(path);
                filesToUpload.add(new Attach(file.getName(), path, false));
                fileAdapter.notifyItemInserted(filesToUpload.size() - 1);
            }
        }

        Log.w(TAG, "onActivityResult: " + fileAdapter.getItemCount());
        super.onActivityResult(requestCode, resultCode, data);
    }
}
