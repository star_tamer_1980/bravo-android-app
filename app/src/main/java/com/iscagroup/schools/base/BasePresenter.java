package com.iscagroup.schools.base;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import io.reactivex.disposables.CompositeDisposable;


/**
 * Created by Raed Saeed on 3/25/2019.
 */
public abstract class BasePresenter<V extends BaseContract.BaseView> implements LifecycleObserver, BaseContract.BasePresenter<V> {
    @SuppressWarnings("unused")
    private static final String TAG = "BasePresenter";

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private V view;

    @Override
    public void attachLifecycle(Lifecycle lifecycle) {
        lifecycle.addObserver(this);
    }

    @Override
    public void detachLifecycle(Lifecycle lifecycle) {
        lifecycle.removeObserver(this);
    }

    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }

    @Override
    public V getView() {
        return view;
    }

    protected CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    protected abstract void onCreate();

    @SuppressWarnings("unused")
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    protected void onResume() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    protected void onDestroy() {
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
        detachView();
    }

}
