package com.iscagroup.schools.base;

import androidx.multidex.MultiDexApplication;

import com.iscagroup.schools.model.local.AppDatabase;
import com.iscagroup.schools.utils.UserSharedPreferences;
import com.onesignal.OneSignal;

/**
 * Created by Raed Saeed on 6/26/2019.
 */
public class BaseApplication extends MultiDexApplication {
    private static final String TAG = "BaseApplication";

    @Override
    public void onCreate() {
        super.onCreate();

        AppDatabase.initializeDataBase(this);

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();

        OneSignal.idsAvailable((userId, registrationId) -> UserSharedPreferences.setUserPlayerId(this, userId));
    }
}
