package com.iscagroup.schools.base;

import android.view.View;

import com.iscagroup.schools.data.BaseData;

/**
 * Created by Raed Saeed on 6/27/2019.
 */
public interface TypeFactory {

    int type(BaseData object);

    AbstractViewHolder createViewHolder(View view, int type);
}
