package com.iscagroup.schools.adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.iscagroup.schools.base.BaseFragment;

import java.util.List;

/**
 * Created by Raed Saeed on 7/1/2019.
 */
public class FragmentAdapter extends FragmentPagerAdapter {

    private List<BaseFragment> fragments;

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return (fragments != null) ? fragments.size() : 0;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragments.get(position).getName();
    }

    public void setFragments(List<BaseFragment> fragments) {
        this.fragments = fragments;
        notifyDataSetChanged();
    }
}
