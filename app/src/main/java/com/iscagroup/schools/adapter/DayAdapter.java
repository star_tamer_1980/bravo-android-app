package com.iscagroup.schools.adapter;

import android.app.AlarmManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.AbstractViewHolder;
import com.iscagroup.schools.base.TypeFactory;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.Day;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 7/30/2019.
 */
public class DayAdapter extends RecyclerView.Adapter<AbstractViewHolder<BaseData>> {
    private static final long DAY_MILLIS = AlarmManager.INTERVAL_DAY;
    private List<BaseData> items = new ArrayList<>();
    private int lastPosition;
    private Day today;

    private OnDayClickListener onDayClickListener;

    private TypeFactory typeFactory = new TypeFactory() {
        @Override
        public int type(BaseData object) {
            return object.getResourceId();
        }

        @Override
        public AbstractViewHolder createViewHolder(View view, int viewType) {
            if (viewType == R.layout.item_day) {
                return new DayViewHolder(view);
            }
            return new TodayViewHolder(view);
        }
    };

    public DayAdapter() {
        generateDays(new DateTime().minusDays(30).getMillis());
    }

    private void generateDays(long initialDate) {
        int i = 0;
        while (i < 60) {
            DateTime actualDate = new DateTime(initialDate + (DAY_MILLIS * i++));
            Day day = new Day(actualDate);
            day.setResourceId(R.layout.item_day);
            if (day.isToday()) {
                day.setSelected(true);
                day.setResourceId(R.layout.item_today);
                today = day;
            }
            items.add(day);
        }
        notifyDataSetChanged();
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public AbstractViewHolder<BaseData> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return typeFactory.createViewHolder(view, viewType);

    }

    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder<BaseData> holder, int position) {
        BaseData item = items.get(position);
        if (item != null) {
            holder.bind(item);
        }
    }

    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return typeFactory.type(items.get(position));
    }

    public void load(List<BaseData> dataList) {
        this.items = dataList;
        if (items != null) {
            this.notifyDataSetChanged();
        }
    }

    public int getTodayIndex() {
        if (items != null) {
            return items.indexOf(today);
        }
        return 0;
    }

    public Day getSelectedDay() {
        return (Day) items.get(lastPosition);
    }

    private void selectDay(int position, boolean isSelected) {
        BaseData baseData = items.get(position);
        if (isSelected) {
            baseData.setResourceId(R.layout.item_today);
        } else {
            baseData.setResourceId(R.layout.item_day);
        }
        items.set(position, baseData);
        notifyItemChanged(position);
    }

    public void setOnDayClickListener(OnDayClickListener onDayClickListener) {
        this.onDayClickListener = onDayClickListener;
    }

    public interface OnDayClickListener {
        void onDaySelected(int position, Day day);
    }

    class DayViewHolder extends AbstractViewHolder<Day> implements View.OnClickListener {
        private CardView cvDayCard;
        private TextView tvDayName;
        private TextView tvDayMonth;

        DayViewHolder(View itemView) {
            super(itemView);
            cvDayCard = itemView.findViewById(R.id.cv_item_day_card);
            cvDayCard.setOnClickListener(this);
            tvDayName = itemView.findViewById(R.id.tv_item_day_name);
            tvDayMonth = itemView.findViewById(R.id.tv_item_day_month);
        }

        @Override
        public void bind(Day element) {
            tvDayName.setText(element.getDay());
            tvDayMonth.setText(element.getMonth());
        }

        @Override
        public void onClick(View v) {
            if (lastPosition != -1 && lastPosition != getAdapterPosition()) {
                selectDay(lastPosition, false);
                selectDay(getAdapterPosition(), true);
                lastPosition = getAdapterPosition();

                Day day = (Day) items.get(getAdapterPosition());
                if (day.isSelected()) {
                    onDayClickListener.onDaySelected(lastPosition, day);
                }
            }
        }
    }

    class TodayViewHolder extends AbstractViewHolder<Day> implements View.OnClickListener {
        private CardView cvTodayCard;
        private TextView tvDayName;
        private TextView tvDayMonth;

        TodayViewHolder(View itemView) {
            super(itemView);
            cvTodayCard = itemView.findViewById(R.id.cv_item_today_card);
            cvTodayCard.setOnClickListener(this);

            tvDayName = itemView.findViewById(R.id.tv_item_today_name);
            tvDayMonth = itemView.findViewById(R.id.tv_item_today_month);
        }

        @Override
        public void bind(Day element) {
            lastPosition = getAdapterPosition();
            tvDayName.setText(element.getDay());
            tvDayName.setTextColor(itemView.getContext().getResources().getColor(R.color.colorAccent));
            tvDayMonth.setText(element.getMonth());
            tvDayMonth.setTextColor(itemView.getContext().getResources().getColor(R.color.colorAccent));
            onDayClickListener.onDaySelected(lastPosition, element);

            if (element.isToday()) {
                onClick(itemView);
            }
        }

        @Override
        public void onClick(View v) {
            if (lastPosition != -1 && lastPosition != getAdapterPosition()) {
                selectDay(lastPosition, false);
                selectDay(getAdapterPosition(), true);
                lastPosition = getAdapterPosition();

                Day day = (Day) items.get(getAdapterPosition());
                if (day.isSelected()) {
                    onDayClickListener.onDaySelected(lastPosition, day);
                }
            }
        }
    }
}