package com.iscagroup.schools.adapter;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.AbstractViewHolder;
import com.iscagroup.schools.base.TypeFactory;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.request.Attach;
import com.iscagroup.schools.global.ImageFullScreenActivity;
import com.iscagroup.schools.utils.CheckPermissions;
import com.iscagroup.schools.utils.DownloadUtil;
import com.iscagroup.schools.utils.StringConstants;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 2/13/2019.
 */
public class FileAdapter extends RecyclerView.Adapter<AbstractViewHolder<BaseData>> {
    @SuppressWarnings("unused")
    private static final String TAG = "FileAdapter";

    // list items of attach files
    private List<BaseData> items = new ArrayList<>();

    // list of deleted files names
    private List<String> deletedFiles = new ArrayList<>();

    // type factory that return the resource id (xml file) to choose
    // to get the quivalent view holder as we following the SOLID principles
    private TypeFactory typeFactory = new TypeFactory() {
        @Override
        public int type(BaseData object) {
            return object.getResourceId();
        }

        @Override
        public AbstractViewHolder createViewHolder(View view, int type) {
            if (type == R.layout.item_file) {
                return new FileAdapterViewHolder(view);
            }
            return new DownloadableFileAdapterViewHolder(view);
        }
    };

    public FileAdapter() {
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public AbstractViewHolder<BaseData> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return typeFactory.createViewHolder(view, viewType);
    }


    // bind the view holder with data, according to open for extension closed for modification principles (SOLID)
    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder<BaseData> holder, int position) {
        BaseData document = items.get(position);
        if (document != null) {
            holder.bind(document);
        }
    }


    // return the view type of object
    // every object should has it's layout (xml file)
    @Override
    public int getItemViewType(int position) {
        return typeFactory.type(items.get(position));
    }

    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }

    public void load(List<BaseData> dataList) {
        this.items = dataList;
        if (items != null) {
            this.notifyDataSetChanged();
        }
    }

    public void loadAttach(List<Attach> attachList) {
        if (items != null) {
            items.addAll(attachList);
            notifyDataSetChanged();
        }
    }

    public void loadItem(Attach document) {
        if (items != null) {
            items.add(document);
            int position = items.size() - 1;
            if (position > -1) {
                notifyItemInserted(position);
            }
        }
    }

    public List<BaseData> getList() {
        return items;
    }

    // convert list of BaseData to list of Attach and return it
    public List<Attach> getAttachList() {
        List<Attach> attaches = new ArrayList<>();
        for (BaseData data : items) {
            Attach attach = (Attach) data;
            attaches.add(attach);
        }
        return attaches;
    }

    // get deleted files names to delete it from the Server
    public List<String> getDeletedFiles() {
        return deletedFiles;
    }

    /**
     * this method to clear all compressed files from the device
     * still working on.
     */
    public void clearCompressedFiles() {
        for (BaseData data : items) {
            Attach attach = (Attach) data;
            if (attach.getId() == null) {
                File file = new File(attach.getFilePath());
                file.delete();
                if (file.exists()) {
                    try {
                        file.getCanonicalFile().delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    // FileViewHolder for files that could be deleted
    class FileAdapterViewHolder extends AbstractViewHolder<Attach> implements View.OnClickListener {
        ImageView ivFileImage;
        ImageView ivFileDelete;
        TextView tvFileName;
        ProgressBar pbFileUploadingProgress;

        // Progress body call back
        FileAdapterViewHolder(View itemView) {
            super(itemView);
            tvFileName = itemView.findViewById(R.id.tv_item_file_name);
            tvFileName.setOnClickListener(this);

            ivFileImage = itemView.findViewById(R.id.iv_item_file_image);
            ivFileImage.setOnClickListener(this);

            ivFileDelete = itemView.findViewById(R.id.iv_item_file_delete);
            ivFileDelete.setOnClickListener(this);

            pbFileUploadingProgress = itemView.findViewById(R.id.pb_item_file_loading);

        }

        @Override
        public void bind(Attach element) {
            if (element.isImage() || element.getFileName().contains("jpg") || element.getFileName().contains("png") || element.getFileName().contains("jpeg")) {
                Glide.with(itemView).load(element.getFilePath())
                        .apply(RequestOptions.bitmapTransform(new RoundedCorners(24)))
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                pbFileUploadingProgress.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                pbFileUploadingProgress.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(ivFileImage);
            } else {
                Glide.with(itemView).load(R.drawable.pdf_image).into(ivFileImage);
            }
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Attach document = (Attach) items.get(position);
            switch (v.getId()) {
                case R.id.iv_item_file_delete:
                    if (items.contains(document)) {
                        if (document.getId() != null) {
                            deletedFiles.add(document.getId());
                        }
                        items.remove(position);
                    }
                    notifyItemRemoved(position);
                    break;

                case R.id.iv_item_file_image:
                case R.id.tv_item_file_name:
                    if (document.isImage()) {
                        Intent intent = new Intent(v.getContext(), ImageFullScreenActivity.class);
                        intent.putParcelableArrayListExtra(StringConstants.EXTRA_PHOTO_LIST, (ArrayList<? extends Parcelable>) items);
                        intent.putExtra(StringConstants.EXTRA_PAGER_POSITION, getAdapterPosition());
                        v.getContext().startActivity(intent);
                    }
                    break;
            }
        }
    }

    // FileViewHolder for files that could be downloaded
    class DownloadableFileAdapterViewHolder extends AbstractViewHolder<Attach> implements View.OnClickListener {
        private ImageView ivItemFile;
        private ProgressBar pbItemLoading;

        DownloadableFileAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ivItemFile = itemView.findViewById(R.id.iv_item_downloadable_file_download);
            ivItemFile.setOnClickListener(this);

            pbItemLoading = itemView.findViewById(R.id.pb_item_downloadable_file_loading);
        }

        @Override
        public void bind(Attach element) {
            if (element.getExtension().equalsIgnoreCase("jpg") ||
                    element.getExtension().equalsIgnoreCase("jpeg") ||
                    element.getExtension().equalsIgnoreCase("png")) {
                element.setImage(true);
                items.set(getAdapterPosition(), element);
                Glide.with(itemView)
                        .load(element.getFilePath())
                        .apply(RequestOptions.bitmapTransform(new RoundedCorners(24)))
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                pbItemLoading.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                pbItemLoading.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(ivItemFile);

            } else {
                pbItemLoading.setVisibility(View.GONE);
                Glide.with(itemView)
                        .load(R.drawable.pdf_image)
                        .into(ivItemFile);
            }
        }

        @Override
        public void onClick(View v) {
            Attach document = (Attach) items.get(getAdapterPosition());
            if (v.getId() == R.id.iv_item_downloadable_file_download) {
                if (document.isImage()) {
                    Intent intent = new Intent(v.getContext(), ImageFullScreenActivity.class);
                    intent.putParcelableArrayListExtra(StringConstants.EXTRA_PHOTO_LIST, (ArrayList<? extends Parcelable>) items);
                    intent.putExtra(StringConstants.EXTRA_PAGER_POSITION, getAdapterPosition());
                    v.getContext().startActivity(intent);
                } else {
                    if (CheckPermissions.checkStoragePermissions((AppCompatActivity) v.getContext())) {
                        DownloadUtil.downloadFile(v.getContext(), document.getFilePath());
                    }

                }
            }
        }
    }
}