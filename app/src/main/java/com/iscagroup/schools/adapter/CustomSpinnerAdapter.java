package com.iscagroup.schools.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.data.teacher.Teacher;

import java.util.List;

/**
 * Created by Raed Saeed on 7/2/2019.
 */
public class CustomSpinnerAdapter extends BaseAdapter {
    @SuppressWarnings("unused")
    private static final String TAG = "CustomSpinnerAdapter";

    private List<Teacher> names;

    @Override
    public int getCount() {
        return (names != null) ? names.size() + 1 : 1;
    }

    @Override
    public Object getItem(int position) {
        return names.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void setTeachers(List<Teacher> strings) {
        this.names = strings;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getAdapterView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    private View getAdapterView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spinner_teacher, parent, false);
        }
        TextView textView = convertView.findViewById(R.id.tv_item_spinner_teacher_name);


        if (position == 0) {
            textView.setText("Select Teacher");
            textView.setTextColor(convertView.getContext().getResources().getColor(R.color.grey));
        } else {
            position = position - 1;
            textView.setText(names.get(position).getName());
            textView.setTextColor(convertView.getContext().getResources().getColor(R.color.colorPrimary));
        }
        return convertView;
    }

}
