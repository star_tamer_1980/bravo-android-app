package com.iscagroup.schools.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.iscagroup.schools.R;
import com.iscagroup.schools.data.request.Attach;

import java.util.List;

/**
 * Created by Raed Saeed on 6/24/2019.
 */
public class ImagePagerAdapter extends PagerAdapter {
    @SuppressWarnings("unused")
    private static final String TAG = "ImagePagerAdapter";

    private List<Attach> photos;
    private ImageTouchListener imageTouchListener;

    /**
     * boolean value to check if the controllers in full screen activity
     * visible or not, the default value is not visible
     */
    private boolean mVisible = false;


    public ImagePagerAdapter(List<Attach> list) {
        photos = list;
    }

    @Override
    public int getCount() {
        return (photos != null) ? photos.size() : 0;
    }

    public void loadList(List<Attach> list) {
        photos = list;
        notifyDataSetChanged();
    }

    public void setImageTouchListener(ImageTouchListener imageTouchListener) {
        this.imageTouchListener = imageTouchListener;
    }

    public Attach getCurrentItem(int position) {
        if (photos != null && photos.size() > position) {
            return photos.get(position);
        }
        return null;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_image_pager, container, false);
        ImageView ivItemImage = view.findViewById(R.id.tiv_item_image_pager);

        ProgressBar pbItemImageLoading = view.findViewById(R.id.pb_item_pager_loading);

        Attach attach = photos.get(position);

        if (attach != null) {
            Glide.with(view)
//                   add listener to set progress visible or hidden
                    .load(attach.getFilePath())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            pbItemImageLoading.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            pbItemImageLoading.setVisibility(View.GONE);
                            return false;
                        }
                    })

//                 Touch Image View has some problems with addList into (ImageView) method
//                 so that it will be easier to use Target<Drawable> instead
//                    .into(new GlideCustomTarget<Drawable>() {
//                        @Override
//                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                            ivItemImage.setImageDrawable(resource);
//                        }
//
//                        //
//                        @Override
//                        public void onLoadCleared(@Nullable Drawable placeholder) {
//
//                        }
//                    });
                    .into(ivItemImage);
        }

        ivItemImage.setOnClickListener(v -> {
            imageTouchListener.onImageTouch(mVisible);
            mVisible = !mVisible;
        });

        container.addView(view, ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    public interface ImageTouchListener {
        void onImageTouch(boolean visible);
    }
}
