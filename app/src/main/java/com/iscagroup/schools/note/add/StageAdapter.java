package com.iscagroup.schools.note.add;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.note.Stage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 7/21/2019.
 */
public class StageAdapter extends BaseAdapter {

    private List<BaseData> items;

    @Override
    public int getCount() {
        return (items != null) ? items.size() + 2 : 2;
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    void loadItems(List<Stage> list) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.clear();
        items.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getStageView(position, convertView, parent);
    }

    private View getStageView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spinner_teacher, parent, false);
        }
        TextView textView = convertView.findViewById(R.id.tv_item_spinner_teacher_name);

        if (position == 0) {
            textView.setText(convertView.getContext().getResources().getString(R.string.please_select));
            textView.setTextColor(convertView.getContext().getResources().getColor(R.color.grey));
        } else if (position == 1) {
            textView.setText(convertView.getContext().getResources().getString(R.string.all_school));
        } else {
            Stage stage = (Stage) items.get(position - 2);
            textView.setText(stage.getStage());
            textView.setTextColor(convertView.getContext().getResources().getColor(R.color.colorPrimary));
        }
        return convertView;
    }
}
