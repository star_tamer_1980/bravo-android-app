package com.iscagroup.schools.note.school;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.android.material.appbar.AppBarLayout;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseFragment;
import com.iscagroup.schools.data.note.Note;
import com.iscagroup.schools.note.BaseNoteActivity;
import com.iscagroup.schools.note.add.AddNoteActivity;
import com.iscagroup.schools.utils.StringConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 7/23/2019.
 */
public class SchoolNoteActivity extends BaseNoteActivity {
    private static final int CREATE_NOTE_REQUEST = 771;
    private SchoolNoteFragment schoolNoteFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getTabLayout().setVisibility(View.GONE);

        // this line of code to disable the
        AppBarLayout.LayoutParams layoutParams = (AppBarLayout.LayoutParams) getToolbar().getLayoutParams();
        layoutParams.setScrollFlags(0);
        getFloatingActionButton().setOnClickListener(v -> {
            Intent intent = new Intent(this, AddNoteActivity.class);
            startActivityForResult(intent, CREATE_NOTE_REQUEST);
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        });
    }

    @Override
    protected List<BaseFragment> setFragments() {
        List<BaseFragment> fragments = new ArrayList<>();
        schoolNoteFragment = new SchoolNoteFragment();
        fragments.add(schoolNoteFragment);
        return fragments;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            if (data != null) {
                List<Note> notes = data.getParcelableArrayListExtra(StringConstants.EXTRA_CREATED_NOTE);
                schoolNoteFragment.updateAdapter(notes);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
