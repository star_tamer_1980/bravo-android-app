package com.iscagroup.schools.note.add;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.note.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 7/18/2019.
 */
public class CategorySpinnerAdapter extends BaseAdapter {
    private List<BaseData> categories;

    public CategorySpinnerAdapter() {
    }

    @Override
    public int getCount() {
        return (categories != null) ? categories.size() + 1 : 1;
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void setCategories(List<Category> list) {
        if (list != null) {
            if (categories == null) {
                categories = new ArrayList<>();
            }
            categories.addAll(list);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getAdapterView(position, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    private View getAdapterView(int position, ViewGroup parent) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spinner_teacher, parent, false);
        TextView textView = convertView.findViewById(R.id.tv_item_spinner_teacher_name);
        textView.setTextColor(convertView.getContext().getResources().getColor(R.color.black));


        if (position == 0) {
            textView.setText(convertView.getContext().getResources().getString(R.string.select_category));
        } else {
            position = position - 1;
            Category category = (Category) categories.get(position);
            textView.setText(category.getCategory());
        }
        return convertView;
    }
}
