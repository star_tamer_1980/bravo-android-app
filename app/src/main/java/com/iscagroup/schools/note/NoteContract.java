package com.iscagroup.schools.note;

import com.iscagroup.schools.base.BaseContract;
import com.iscagroup.schools.data.note.Note;

import java.util.List;

/**
 * Created by Raed Saeed on 7/23/2019.
 */
public interface NoteContract {
    interface NoteView extends BaseContract.BaseView {
        void getNotes();

        void showNotesList(List<Note> noteList);
    }

    interface NotePresenter {
        void getParentNotes(String schoolId, String classId, String studentId, String noteType, String itemCount, String limit);

        void getTeacherNotes(String schoolId, String teacherId, String itemCount, String limit);

        void getSchoolNotes(String schoolId, String itemCount, String limit);
    }
}
