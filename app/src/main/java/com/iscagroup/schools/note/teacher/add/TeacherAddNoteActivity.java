package com.iscagroup.schools.note.teacher.add;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.note.Category;
import com.iscagroup.schools.data.note.Kid;
import com.iscagroup.schools.data.note.Note;
import com.iscagroup.schools.data.note.NoteTeacherClass;
import com.iscagroup.schools.dialog.BottomFilePickerDialog;
import com.iscagroup.schools.note.NoteViewModel;
import com.iscagroup.schools.note.add.CategorySpinnerAdapter;
import com.iscagroup.schools.note.add.KidsAdapter;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.utils.UserSharedPreferences;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

public class TeacherAddNoteActivity extends BaseActivity {
    private static final String featureType = "2";

    /**
     * NoteViewModel in this activity i will use MVVM instead of MVP :P
     */
    private NoteViewModel noteViewModel;

    /**
     * Class List, every grade has a list of class that is used by classAdapter
     * to create a class dropdown menu
     */
    private List<NoteTeacherClass> classList;

    private TeacherClassAdapter classAdapter;
    private KidsAdapter kidsAdapter;

    /**
     * last selected element from stage adapter that is used to inform the user of last selection
     * when he open the bottomDialog
     */
    private int previousClassSpinnerSelection = -1;
    private int previousKidSpinnerSelection = -1;


    /**
     * This is the last selected text from spinners of Stages, Grades and Classes
     */
    private String lastSelectedElementName;

    private Kid selectedKid;     // selected kid from spinner

    private NoteTeacherClass selectedClassModel;  // selected class from spinner

    private String selectedCategory;        // selected category name

    private String kidId = "0";  // default kid id

    private String classId = "0";   // default class id

    private String categoryId = "";         // default category id

    private String userId;

    private CategorySpinnerAdapter categorySpinnerAdapter;

    private BottomSheetDialog noteConfigureDialog;

    private Button btnAddNoteNext;

    private EditText etAddNoteText;

    private List<Note> createdNotesList = new ArrayList<>();

    private String schoolId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        Toolbar toolbar = findViewById(R.id.tb_activity_add_note_category);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(R.string.create_note);
        }

        initVars();
        initCategorySpinner();

        // This text view to show the bottom dialog of note configurations
        TextView tvAddNoteActivitySendTo = findViewById(R.id.tv_activity_add_note_send_to);
        tvAddNoteActivitySendTo.setOnClickListener(v -> {
            noteConfigureDialog = new BottomSheetDialog(this);
            noteConfigureDialog.setContentView(createNoteConfigureView());
            noteConfigureDialog.setOnDismissListener(dialog -> {
                // return last selected text when the dialog dismissed
                // if the user doesn't select any thing the last selected text would be All stages

                if (getLastSelectedDropDownElement()) {
                    if (previousClassSpinnerSelection == 0) {
                        tvAddNoteActivitySendTo.setText(new StringBuilder().append(lastSelectedElementName));
                    } else {
                        tvAddNoteActivitySendTo.setText(new StringBuilder().append(getString(R.string.to)).append(" ").append(lastSelectedElementName));
                    }
                }

            });
            noteConfigureDialog.show();
        });

        // note text
        etAddNoteText = findViewById(R.id.et_activity_add_note_text);
        etAddNoteText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    btnAddNoteNext.setEnabled(true);
                } else {
                    btnAddNoteNext.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnAddNoteNext = findViewById(R.id.btn_activity_add_note_next);
        btnAddNoteNext.setOnClickListener(v -> {

            // if category id not equal 0 this means the user selected category
            if (!categoryId.equalsIgnoreCase("0")) {
                if (previousClassSpinnerSelection > 0) {
                    addNote(etAddNoteText.getText().toString().trim());
                    btnAddNoteNext.setEnabled(false);
                } else {
                    showToast(getString(R.string.please_select_receiver));
                }
            } else {
                showToast(getString(R.string.please_select_category));
            }
        });

        ImageView attachFile = findViewById(R.id.iv_activity_add_note_attach_file);
        attachFile.setOnClickListener(v -> {
            BottomFilePickerDialog filePickerDialog = new BottomFilePickerDialog();
            filePickerDialog.setClickListener(view -> {
                switch (view.getId()) {
                    case R.id.iv_dialog_file_picker_pick_image:
                        startImagePicker(7);
                        break;

                    case R.id.iv_dialog_file_picker_pick_document:
                        startDocumentPicker();
                        break;
                }
            });
            filePickerDialog.show(getSupportFragmentManager(), null);
        });

        RecyclerView rvNoteFileList = findViewById(R.id.rv_activity_add_note_file_list);
        rvNoteFileList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvNoteFileList.setAdapter(getFileAdapter());

    }


    /**
     * This method for getting the last selected element from dropdown menus of Stages, Grades, Classes and Kids.
     * The last element name depend on previous spinner selection
     * if spinner selection of class is greater than 0 that means the last selected element from class dropdown
     * otherwise we select the name from the previous menu which is Grade list
     */
    private boolean getLastSelectedDropDownElement() {
        if (previousKidSpinnerSelection > 0) {
            lastSelectedElementName = selectedKid.getStudentName();
            return true;
        } else if (previousClassSpinnerSelection >= 0) {
            if (previousClassSpinnerSelection > 0) {
                lastSelectedElementName = selectedClassModel.getClassName();
                return true;
            } else {
                lastSelectedElementName = getString(R.string.please_select);
                return true;
            }
        }
        return false;
    }

    private void initCategorySpinner() {
        AppCompatSpinner categorySpinner = findViewById(R.id.spinner_activity_add_note_category);
        categorySpinnerAdapter = new CategorySpinnerAdapter();
        categorySpinner.setAdapter(categorySpinnerAdapter);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    categoryId = ((Category) categorySpinnerAdapter.getItem(position - 1)).getId();
                    selectedCategory = ((Category) categorySpinnerAdapter.getItem(position - 1)).getCategory();
                } else {
                    categoryId = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        getCategoryList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getClassesList();
    }

    private void initVars() {
        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);

        classAdapter = new TeacherClassAdapter();
        kidsAdapter = new KidsAdapter();

        userId = UserSharedPreferences.getUserId(this);
        schoolId = UserSharedPreferences.getUserSchoolId(this);
    }

    private void getCategoryList() {
        compositeDisposable.add(noteViewModel.getResponse().subscribeOn(io())
                .observeOn(mainThread())
                .subscribe(response -> {
                    log(response.toString());
                    categorySpinnerAdapter.setCategories(response.getResult());
                }, error -> {
                    error.printStackTrace();
                    showToast(R.string.connection_interruption);
                }));
    }

    private void getClassesList() {
        compositeDisposable.add(noteViewModel.getTeacherClass(schoolId, userId)
                .subscribeOn(io())
                .observeOn(mainThread())
                .subscribe(response -> {
                    classAdapter.loadItems(response.getClasses());
                    log(response.toString());
                }, error -> {
                    error.printStackTrace();
                    showToast(R.string.connection_interruption);
                }));
    }

    /**
     * Create the view of bottom Dialog
     *
     * @return the created view
     */
    private View createNoteConfigureView() {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dialog_note_configure, null, false);

        AppCompatSpinner stageSpinner = view.findViewById(R.id.spinner_note_config_stage);
        stageSpinner.setVisibility(View.GONE);


        AppCompatSpinner gradeSpinner = view.findViewById(R.id.spinner_note_config_grade);
        gradeSpinner.setVisibility(View.GONE);


        AppCompatSpinner classSpinner = view.findViewById(R.id.spinner_note_config_class);
        classSpinner.setAdapter(classAdapter);
        classSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    selectedClassModel = (NoteTeacherClass) classAdapter.getItem(position - 1);
                    getKidsList(selectedClassModel.getClassId(), schoolId);
                    classId = selectedClassModel.getClassId();
                } else {
                    kidsAdapter.clear();
                    classId = "0";
                }

                previousClassSpinnerSelection = position;
                previousKidSpinnerSelection = -1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (previousClassSpinnerSelection != -1) {
            classSpinner.setSelection(previousClassSpinnerSelection);
        }


        AppCompatSpinner kidSpinner = view.findViewById(R.id.spinner_note_config_kid);
        kidSpinner.setAdapter(kidsAdapter);
        kidSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    selectedKid = (Kid) kidsAdapter.getItem(position - 1);
                    kidId = selectedKid.getId();
                } else {
                    kidId = "0";
                }
                previousKidSpinnerSelection = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (previousKidSpinnerSelection != -1) {
            kidSpinner.setSelection(previousKidSpinnerSelection);
        }

        TextView tvBottomNoteConfigurationDialogDone = view.findViewById(R.id.tv_dialog_note_configuration_done);
        tvBottomNoteConfigurationDialogDone.setOnClickListener(v -> noteConfigureDialog.dismiss());
        return view;
    }

    /**
     * get kids list for note
     *
     * @param classId  selected class from spinner adapter
     * @param schoolId selected school id
     */
    private void getKidsList(String classId, String schoolId) {
        compositeDisposable.add(noteViewModel.getKids(classId, schoolId)
                .subscribeOn(io())
                .observeOn(mainThread())
                .subscribe(kidsResponse ->
                        kidsAdapter.loadItems(kidsResponse.getStudents()), error ->
                        showToast(R.string.connection_interruption)));
    }

    /**
     * Add note method
     *
     * @param note note body
     */
    private void addNote(String note) {
        compositeDisposable.add(noteViewModel.addNote(note, userId, categoryId, schoolId, classId, kidId)
                .subscribeOn(io())
                .observeOn(mainThread())
                .doOnSubscribe(subscription -> showProgressDialog(getString(R.string.sending_note)))
                .subscribe(this::sendFiles, error -> {
                    hideProgressDialog();
                    showFailureMessage(error.getMessage());
                    error.printStackTrace();
                }));
    }

    private void sendFiles(JsonObject jsonObject) {
        JsonElement jsonElement = jsonObject.get("id");
        getAttachmentPresenter().attachFiles(featureType,
                String.valueOf(jsonElement.getAsInt()),
                schoolId,
                getFileAdapter().getList());
    }

    @Override
    public void onUploadDone() {
        hideProgressDialog();
        buildNotes();
        showAlertDialog(getString(R.string.send_other), getString(R.string.do_you_want_send_another_note),
                (dialog, which) -> dialog.dismiss(),
                (dialog, which) -> closeActivity());
        btnAddNoteNext.setEnabled(true);
    }

    private void buildNotes() {
        Note note = new Note();
        note.setId("0");
        note.setStudentId(kidId);
        note.setClassId(classId);
        note.setCategoryId(categoryId);
        note.setSchoolId(schoolId);
        note.setAddedBy(UserSharedPreferences.getUserName(this));
        note.setNote(etAddNoteText.getText().toString().trim());
        note.setCategoryName(selectedCategory);
        note.setNoteTo(lastSelectedElementName);
        note.setResourceId(R.layout.item_parent_note);
        note.setAttach(getFileAdapter().getAttachList());
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
        note.setAddedOn(dateFormat.format(date));

        log(note.toString());
        createdNotesList.add(note);
    }

    private void closeActivity() {
//        getFileAdapter().clearCompressedFiles();
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(StringConstants.EXTRA_CREATED_NOTE, (ArrayList<? extends Parcelable>) createdNotesList);
        setResult(RESULT_OK, intent);
        finish();
    }
}
