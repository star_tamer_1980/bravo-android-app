package com.iscagroup.schools.note.school;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.iscagroup.schools.note.BaseNoteFragment;
import com.iscagroup.schools.utils.UserSharedPreferences;

/**
 * Created by Raed Saeed on 7/23/2019.
 */
public class SchoolNoteFragment extends BaseNoteFragment {

    private String schoolId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        schoolId = UserSharedPreferences.getUserSchoolId(getContext());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void loadMoreItems() {
        loadMore = true;
        notePresenter.getSchoolNotes(schoolId, String.valueOf(noteAdapter.getItemCount()), "10");
    }

    @Override
    public void getNotes() {
        // set isRefreshed to true to enable setRefreshing method
        isRefreshed = true;

        // set isLastPage to false to enable loadMoreItems method
        isLastPage = false;
        notePresenter.getSchoolNotes(schoolId, "0", "10");
    }
}
