package com.iscagroup.schools.note.parent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.iscagroup.schools.note.BaseNoteFragment;
import com.iscagroup.schools.utils.UserSharedPreferences;

/**
 * Created by Raed Saeed on 7/23/2019.
 */
public class ParentPrivateNoteFragment extends BaseNoteFragment {
    private static final String NOTE_TYPE = "3"; // For private Notes
    private String schoolId;
    private String classId;
    private String studentId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        schoolId = UserSharedPreferences.getUserSchoolId(getContext());
        classId = UserSharedPreferences.getUserClassId(getContext());
        studentId = UserSharedPreferences.getUserStudentId(getContext());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void loadMoreItems() {
        loadMore = true;
        notePresenter.getParentNotes(schoolId, classId, studentId, NOTE_TYPE, String.valueOf(noteAdapter.getItemCount()), "10");
    }

    @Override
    public void getNotes() {
        // set isRefreshed to true to enable setRefreshing method
        isRefreshed = true;

        // set isLastPage to false to enable loadMoreItems method
        isLastPage = false;
        notePresenter.getParentNotes(schoolId, classId, studentId, NOTE_TYPE, "0", "10");
    }
}
