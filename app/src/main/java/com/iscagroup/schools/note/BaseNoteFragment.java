package com.iscagroup.schools.note;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseFragment;
import com.iscagroup.schools.data.note.Note;

import java.util.List;

/**
 * Created by Raed Saeed on 7/23/2019.
 */
public abstract class BaseNoteFragment extends BaseFragment implements NoteContract.NoteView {
    @SuppressWarnings("unused")
    private static final String TAG = "BaseNoteFragment";
    protected NotePresenterImp notePresenter;
    protected NoteAdapter noteAdapter;
    /**
     * isLastPage boolean variable to check if the page has more items or not
     * if true we don't call loadMoreItems method
     * if false we call loadMoreItems method
     */
    protected boolean isLastPage = false;
    /**
     * isRefreshed boolean variable to check if the getRequest called or not for first time
     * getRequest called from onResume and onRefresh which is used to clear the adapter
     */
    protected boolean isRefreshed = false;
    /**
     * loadMore boolean variable to check loadMore or not
     * if loadMore is true the progress bar should be showing
     * if loadMore is false the progress bar should be not showing
     */
    protected boolean loadMore = false;
    private Context context;
    private SwipeRefreshLayout refreshLayout;
    private ProgressBar pbNoteLoading;
    /**
     * loaded variable to check if the request loaded or not, means on successful response
     * wether data exist or not the loaded variable will be true to use in setRefreshing
     * if true setRefreshing will be called
     * if false setRefreshing won't be called
     */
    private boolean loaded = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note, container, false);
        initViews(view);
        initVars();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!loaded) {
            getNotes();
        }
    }

    private void initViews(View view) {
        noteAdapter = new NoteAdapter();
        RecyclerView rvRequestList = view.findViewById(R.id.rv_fragment_note_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvRequestList.setLayoutManager(layoutManager);
        rvRequestList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = layoutManager.getItemCount();
                int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                boolean endIsReached = lastVisibleItemPosition + 3 > totalItemCount;
                if (!isLastPage && !loadMore && endIsReached && totalItemCount > 0) {
                    loadMoreItems();
                }
            }
        });

        rvRequestList.setAdapter(noteAdapter);


        refreshLayout = view.findViewById(R.id.srl_fragment_note_refresh_layout);
        refreshLayout.setOnRefreshListener(this::getNotes);

        pbNoteLoading = view.findViewById(R.id.pb_fragment_note_loading);
    }

    @Override
    public void startRefreshing() {
        if (loaded && !loadMore) refreshLayout.setRefreshing(true);
    }

    @Override
    public void stopRefreshing() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void hideProgressBar() {
        pbNoteLoading.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showProgressBar() {
        pbNoteLoading.setVisibility(View.VISIBLE);
    }

    private void initVars() {
        notePresenter = new NotePresenterImp();
        notePresenter.attachView(this);
        notePresenter.attachLifecycle(getLifecycle());
        getLifecycleRegistry().handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
    }

    protected abstract void loadMoreItems();

    @Override
    public void showNotesList(List<Note> noteList) {
        Log.w(TAG, "showNotesList: " + noteList.size());
        loaded = true;
        loadMore = false;

        if (isRefreshed) {
            noteAdapter.clearList();
            isRefreshed = false;
        }

        if (noteList.size() < 10) {
            isLastPage = true;
            hideProgressBar();
        }
        noteAdapter.addNoteList(noteList);
    }

    public void updateAdapter(List<Note> notes) {
        if (notes != null && notes.size() > 0) {
            for (Note note : notes) {
                noteAdapter.addNote(note);
            }
        }
    }
}
