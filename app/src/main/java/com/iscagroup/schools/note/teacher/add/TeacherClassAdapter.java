package com.iscagroup.schools.note.teacher.add;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.note.NoteTeacherClass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 7/24/2019.
 */
public class TeacherClassAdapter extends BaseAdapter {
    private List<BaseData> items;

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getCount() {
        return (items != null) ? items.size() + 1 : 1;
    }

    void loadItems(List<NoteTeacherClass> list) {
        if (items == null) {
            items = new ArrayList<>();
        }

        items.clear();
        items.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getClassView(position, convertView, parent);
    }

    private View getClassView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spinner_teacher, parent, false);
        }
        TextView textView = convertView.findViewById(R.id.tv_item_spinner_teacher_name);

        if (position == 0) {
            textView.setText(convertView.getContext().getResources().getString(R.string.please_select));
        } else {
            NoteTeacherClass teacherClass = (NoteTeacherClass) items.get(position - 1);
            textView.setText(teacherClass.getClassName());
            textView.setTextColor(convertView.getContext().getResources().getColor(R.color.colorPrimary));
        }
        return convertView;
    }

    public void clear() {
        if (items != null) {
            items.clear();
            notifyDataSetChanged();
        }
    }
}
