package com.iscagroup.schools.note;

import android.util.Log;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BasePresenter;
import com.iscagroup.schools.data.note.NoteResponse;
import com.iscagroup.schools.model.local.AppDatabase;
import com.iscagroup.schools.model.local.NoteDao;
import com.iscagroup.schools.model.remote.NoteService;

import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

/**
 * Created by Raed Saeed on 7/23/2019.
 */
public class NotePresenterImp extends BasePresenter<NoteContract.NoteView> implements NoteContract.NotePresenter {
    @SuppressWarnings("unused")
    private static final String TAG = "NotePresenterImp";

    private NoteService noteService;
    private NoteDao noteDao;

    @Override
    protected void onCreate() {
        noteService = NoteService.getInstance();
        noteDao = AppDatabase.getInstance().getNoteDao();
    }

    @Override
    public void getParentNotes(String schoolId, String classId, String studentId, String noteType, String itemCount, String limit) {
        if (getView().isConnected()) {
            getCompositeDisposable().add(noteService.getParentNotes(studentId, noteType, itemCount, limit)
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .flatMapIterable(NoteResponse::getNotes)
                    .map(note -> {
                        note.setResourceId(R.layout.item_parent_note);
                        if (noteType.equalsIgnoreCase("1")) {
                            note.setStudentId(studentId);
                        }
                        return note;
                    })
                    .doOnSubscribe(subscription -> {
                        getView().startRefreshing();
                        getView().showProgressBar();
                    })
                    .toList()
                    .subscribe(noteResponse -> {
                        getView().showNotesList(noteResponse);
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getCompositeDisposable().add(noteDao.saveNotes(noteResponse)
                                .subscribeOn(io())
                                .observeOn(mainThread())
                                .subscribe(successResponse -> Log.w(TAG, "getParentNotes: data saved "), error -> {
                                    getView().hideProgressBar();
                                    getView().stopRefreshing();
                                    getView().showFailureMessage(error.getMessage());
                                    error.printStackTrace();
                                }));
                    }, error -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                        error.printStackTrace();
                    }));
        } else {
            switch (noteType) {
                case "1":
                    getPublicNote(schoolId, studentId, noteType, itemCount);
                    break;
                case "2":
                    getClassNotes(schoolId, classId, itemCount);
                    break;

                case "3":
                    getPrivateNotes(studentId, noteType, itemCount);
                    break;

            }
        }
    }

    private void getPrivateNotes(String studentId, String noteType, String itemCount) {
        getCompositeDisposable().add(noteDao.getPrivateNotes(studentId, noteType, Integer.valueOf(itemCount))
                .subscribeOn(io())
                .observeOn(mainThread())
                .doOnSubscribe(subscription -> getView().showProgressBar())
                .toFlowable()
                .flatMapIterable(noteList -> noteList)
                .map(note -> {
                    note.setResourceId(R.layout.item_parent_note);
                    return note;
                })
                .toList()
                .subscribe(noteList -> {
                            getView().stopRefreshing();
                            getView().hideProgressBar();
                            getView().showNotesList(noteList);
                        },
                        error -> {
                            getView().stopRefreshing();
                            getView().hideProgressBar();
                            getView().showFailureMessage(error.getMessage());
                        }));
    }

    private void getClassNotes(String schoolId, String classId, String itemCount) {
        getCompositeDisposable().add(noteDao.getClassNotes(schoolId, classId, Integer.valueOf(itemCount))
                .subscribeOn(io())
                .observeOn(mainThread())
                .doOnSubscribe(subscription -> getView().showProgressBar())
                .toFlowable()
                .flatMapIterable(noteList -> noteList)
                .map(note -> {
                    note.setResourceId(R.layout.item_parent_note);
                    return note;
                })
                .toList()
                .subscribe(noteList -> {
                            getView().stopRefreshing();
                            getView().hideProgressBar();
                            getView().showNotesList(noteList);
                        },
                        error -> {
                            getView().stopRefreshing();
                            getView().hideProgressBar();
                            getView().showFailureMessage(error.getMessage());
                        }));
    }

    private void getPublicNote(String schoolId, String studentId, String noteType, String itemCount) {
        getCompositeDisposable().add(noteDao.getPublicNote(schoolId, studentId, noteType, Integer.valueOf(itemCount))
                .subscribeOn(io())
                .observeOn(mainThread())
                .doOnSubscribe(subscription -> getView().showProgressBar())
                .toFlowable()
                .flatMapIterable(noteList -> noteList)
                .map(note -> {
                    note.setResourceId(R.layout.item_parent_note);
                    return note;
                })
                .toList()
                .subscribe(noteList -> {
                            getView().stopRefreshing();
                            getView().hideProgressBar();
                            getView().showNotesList(noteList);
                        },
                        error -> {
                            getView().stopRefreshing();
                            getView().hideProgressBar();
                            getView().showFailureMessage(error.getMessage());
                        }));
    }

    @Override
    public void getTeacherNotes(String schoolId, String teacherId, String itemCount, String limit) {
        if (getView().isConnected()) {
            getCompositeDisposable().add(noteService.getTeacherNotes(schoolId, teacherId, itemCount, limit)
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .flatMapIterable(NoteResponse::getNotes)
                    .map(note -> {
                        note.setResourceId(R.layout.item_parent_note);
                        note.setTeacherId(teacherId);
                        return note;
                    })
                    .doOnSubscribe(subscription -> {
                        getView().startRefreshing();
                        getView().showProgressBar();
                    })
                    .toList()
                    .subscribe(noteResponse -> {
                        getView().showNotesList(noteResponse);
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getCompositeDisposable().add(noteDao.saveNotes(noteResponse)
                                .subscribeOn(io())
                                .observeOn(mainThread())
                                .subscribe(successResponse -> Log.w(TAG, "getTeacherNotes : data saved "), error -> {
                                    getView().hideProgressBar();
                                    getView().stopRefreshing();
                                    getView().showFailureMessage(error.getMessage());
                                    error.printStackTrace();
                                }));
                    }, error -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                        error.printStackTrace();
                    }));
        } else {
            getCompositeDisposable().add(noteDao.getTeacherNote(schoolId, teacherId, Integer.valueOf(itemCount))
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .doOnSubscribe(subscription -> getView().showProgressBar())
                    .toFlowable()
                    .flatMapIterable(noteList -> noteList)
                    .map(note -> {
                        note.setResourceId(R.layout.item_parent_note);
                        return note;
                    })
                    .toList()
                    .subscribe(noteList -> {
                                getView().stopRefreshing();
                                getView().hideProgressBar();
                                getView().showNotesList(noteList);
                            },
                            error -> {
                                getView().stopRefreshing();
                                getView().hideProgressBar();
                                getView().showFailureMessage(error.getMessage());
                            }));
        }
    }

    @Override
    public void getSchoolNotes(String schoolId, String itemCount, String limit) {
        if (getView().isConnected()) {
            getCompositeDisposable().add(noteService.getSchoolNotes(schoolId, itemCount, limit)
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .flatMapIterable(NoteResponse::getNotes)
                    .map(note -> {
                        note.setResourceId(R.layout.item_parent_note);
                        return note;
                    })
                    .doOnSubscribe(subscription -> {
                        getView().startRefreshing();
                        getView().showProgressBar();
                    })
                    .toList()
                    .subscribe(noteResponse -> {
                        getView().showNotesList(noteResponse);
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getCompositeDisposable().add(noteDao.saveNotes(noteResponse)
                                .subscribeOn(io())
                                .observeOn(mainThread())
                                .subscribe(successResponse -> Log.w(TAG, "getSchoolNotes : data saved "), error -> {
                                    getView().hideProgressBar();
                                    getView().stopRefreshing();
                                    getView().showFailureMessage(error.getMessage());
                                    error.printStackTrace();
                                }));
                    }, error -> {
                        getView().hideProgressBar();
                        getView().stopRefreshing();
                        getView().showFailureMessage(error.getMessage());
                        error.printStackTrace();
                    }));
        } else {
            getCompositeDisposable().add(noteDao.getSchoolNotes(schoolId, Integer.valueOf(itemCount))
                    .subscribeOn(io())
                    .observeOn(mainThread())
                    .doOnSubscribe(subscription -> getView().showProgressBar())
                    .toFlowable()
                    .flatMapIterable(noteList -> noteList)
                    .map(note -> {
                        note.setResourceId(R.layout.item_parent_note);
                        return note;
                    })
                    .toList()
                    .subscribe(noteList -> {
                                getView().stopRefreshing();
                                getView().hideProgressBar();
                                getView().showNotesList(noteList);
                            },
                            error -> {
                                getView().stopRefreshing();
                                getView().hideProgressBar();
                                getView().showFailureMessage(error.getMessage());
                            }));
        }
    }
}
