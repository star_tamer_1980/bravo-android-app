package com.iscagroup.schools.note.teacher;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.android.material.appbar.AppBarLayout;
import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseFragment;
import com.iscagroup.schools.data.note.Note;
import com.iscagroup.schools.note.BaseNoteActivity;
import com.iscagroup.schools.note.teacher.add.TeacherAddNoteActivity;

import java.util.ArrayList;
import java.util.List;

import static com.iscagroup.schools.utils.StringConstants.EXTRA_CREATED_NOTE;

/**
 * Created by Raed Saeed on 7/23/2019.
 */
public class TeacherNoteActivity extends BaseNoteActivity {

    private static final int CREATE_NOTE_REQUEST = 957;
    private TeacherNoteFragment teacherNoteFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getTabLayout().setVisibility(View.GONE);
        AppBarLayout.LayoutParams layoutParams = (AppBarLayout.LayoutParams) getToolbar().getLayoutParams();
        layoutParams.setScrollFlags(0);

        getFloatingActionButton().setOnClickListener(v -> {
            Intent intent = new Intent(this, TeacherAddNoteActivity.class);
            startActivityForResult(intent, CREATE_NOTE_REQUEST);
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        });
    }

    @Override
    protected List<BaseFragment> setFragments() {
        List<BaseFragment> fragments = new ArrayList<>();
        teacherNoteFragment = new TeacherNoteFragment();
        teacherNoteFragment.setName(getString(R.string.public_));
        fragments.add(teacherNoteFragment);
        return fragments;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            List<Note> noteList = data.getParcelableArrayListExtra(EXTRA_CREATED_NOTE);
            teacherNoteFragment.updateAdapter(noteList);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
