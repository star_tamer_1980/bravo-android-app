package com.iscagroup.schools.note;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.adapter.FileAdapter;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.data.note.Note;
import com.iscagroup.schools.data.request.Attach;
import com.iscagroup.schools.utils.StringConstants;

import java.util.ArrayList;
import java.util.List;

public class NoteDetailActivity extends BaseActivity {
    @SuppressWarnings("unused")
    private static final String TAG = "NoteDetailActivity";

    private Note note;
    private TextView tvNoteBody;
    private FileAdapter fileAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);

        Toolbar toolbar = findViewById(R.id.tb_activity_note_detail_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        initView();
        Intent intent = getIntent();
        if (intent != null) {
            note = intent.getParcelableExtra(StringConstants.EXTRA_NOTE);
            if (note != null) {
                setupView(note);
                initToolbarView(toolbar);
            }
        }
    }

    private void initToolbarView(Toolbar toolbar) {
        TextView tvFrom = toolbar.findViewById(R.id.tv_layout_secondary_toolbar_primary_name);
        tvFrom.setText(note.getAddedBy());

        TextView tvTo = toolbar.findViewById(R.id.tv_layout_secondary_toolbar_name);
        tvTo.setText(note.getNoteTo());
    }

    private void initView() {
        tvNoteBody = findViewById(R.id.tv_activity_note_detail_body);

        fileAdapter = new FileAdapter();
        RecyclerView rvNoteFiles = findViewById(R.id.rv_activity_note_detail_file_list);
        rvNoteFiles.setLayoutManager(new LinearLayoutManager(this));
        rvNoteFiles.setAdapter(fileAdapter);
    }

    private void setupView(Note note) {
        tvNoteBody.setText(note.getNote());
        fileAdapter.loadAttach(getFiles());

        TextView tvNoteDetailCategory = findViewById(R.id.tv_activity_note_detail_category);
        tvNoteDetailCategory.setText(note.getCategoryName());
    }

    private List<Attach> getFiles() {
        List<Attach> requestFiles = new ArrayList<>();
        if (note.getAttach() != null && note.getAttach().size() != 0) {
            for (Attach attach : note.getAttach()) {
                attach.setResourceId(R.layout.item_downloadable_file);
                requestFiles.add(attach);
            }
        }
        return requestFiles;
    }
}
