package com.iscagroup.schools.note;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.google.gson.JsonObject;
import com.iscagroup.schools.data.note.CategoryResponse;
import com.iscagroup.schools.data.note.KidsResponse;
import com.iscagroup.schools.data.note.NoteClassResponse;
import com.iscagroup.schools.data.note.SchoolResponse;
import com.iscagroup.schools.model.remote.NoteService;

import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Created by Raed Saeed on 7/18/2019.
 */
public class NoteViewModel extends AndroidViewModel {
    private NoteService noteService;


    public NoteViewModel(@NonNull Application application) {
        super(application);
        noteService = NoteService.getInstance();
    }

    public Flowable<CategoryResponse> getResponse() {
        return noteService.getCategories();
    }

    public Flowable<SchoolResponse> getBody(String schoolId) {
        return noteService.getSchoolResponse(schoolId);
    }

    public Flowable<KidsResponse> getKids(String classId, String schoolId) {
        return noteService.getKidsResponse(classId, schoolId);
    }

    public Flowable<NoteClassResponse> getTeacherClass(String schoolId, String userId) {
        return noteService.getTeacherClass(schoolId, userId);
    }


    public Observable<JsonObject> addNote(String note, String addedBy, String categoryId, String schoolId, String stageId,
                                   String gradeId, String classId, String kidId) {
        return noteService.addNote(note, addedBy, categoryId, schoolId, stageId,
                gradeId, classId, kidId);
    }

    public Observable<JsonObject> addNote(String note, String addedBy, String categoryId, String schoolId, String classId, String kidId) {
        return noteService.addNote(note, addedBy, categoryId, schoolId, classId, kidId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        noteService = null;
    }
}
