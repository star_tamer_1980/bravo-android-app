package com.iscagroup.schools.note;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.iscagroup.schools.R;
import com.iscagroup.schools.adapter.FragmentAdapter;
import com.iscagroup.schools.base.BaseActivity;
import com.iscagroup.schools.base.BaseFragment;

import java.util.List;

public abstract class BaseNoteActivity extends BaseActivity {


    private Toolbar toolbar;
    private FloatingActionButton floatingActionButton;
    private ViewPager vpNotePager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_note);
        toolbar = findViewById(R.id.tb_activity_base_note_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(R.string.note);
        }


        vpNotePager = findViewById(R.id.vp_activity_base_note_view_pager);
        FragmentAdapter fragmentAdapter = new FragmentAdapter(getSupportFragmentManager());
        vpNotePager.setAdapter(fragmentAdapter);
        fragmentAdapter.setFragments(setFragments());

        tabLayout = findViewById(R.id.tl_activity_base_note_tab);
        tabLayout.setupWithViewPager(vpNotePager);


        floatingActionButton = findViewById(R.id.fab_base_note_activity);
    }

    protected abstract List<BaseFragment> setFragments();

    public ViewPager getViewPager() {
        return vpNotePager;
    }

    public FloatingActionButton getFloatingActionButton() {
        return floatingActionButton;
    }

    public TabLayout getTabLayout() {
        return tabLayout;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    protected void hideFab() {
        floatingActionButton.hide();
    }

    protected void showFab() {
        floatingActionButton.show();
    }
}
