package com.iscagroup.schools.note;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.AbstractViewHolder;
import com.iscagroup.schools.base.TypeFactory;
import com.iscagroup.schools.data.BaseData;
import com.iscagroup.schools.data.note.Note;
import com.iscagroup.schools.utils.DateTimeUtil;
import com.iscagroup.schools.utils.StringConstants;
import com.iscagroup.schools.widget.ExpandableTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 7/23/2019.
 */
public class NoteAdapter extends RecyclerView.Adapter<AbstractViewHolder<BaseData>> {

    private List<BaseData> items;

    private TypeFactory typeFactory = new TypeFactory() {
        @Override
        public int type(BaseData object) {
            return object.getResourceId();
        }

        @Override
        public AbstractViewHolder createViewHolder(View view, int viewType) {
            return new ParentNoteViewHolder(view);
        }
    };

    NoteAdapter() {

    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public AbstractViewHolder<BaseData> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return typeFactory.createViewHolder(view, viewType);

    }

    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder<BaseData> holder, int position) {
        BaseData item = items.get(position);
        if (item != null) {
            holder.bind(item);
        }
    }

    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return typeFactory.type(items.get(position));
    }

    public void load(List<BaseData> dataList) {
        this.items = dataList;
        if (items != null) {
            this.notifyDataSetChanged();
        }
    }

    void addNoteList(List<Note> noteList) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.addAll(noteList);
        notifyDataSetChanged();
    }

    public void addNote(Note note) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.add(0, note);
        notifyItemInserted(0);
    }

    public void removeNote(Note note) {
        if (items != null) {
            int index = items.indexOf(note);
            items.remove(note);
            notifyItemRemoved(index);
        }
    }

    public void removeNote(int position) {
        if (items != null) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    void clearList() {
        if (items != null) {
            items.clear();
            notifyDataSetChanged();
        }
    }

    class ParentNoteViewHolder extends AbstractViewHolder<Note> {
        private CardView cvNoteItemCard;
        private TextView tvNoteFrom;
        private TextView tvNoteTo;
        private ExpandableTextView tvNoteDescription;
        private TextView tvCreateDate;
        private TextView tvAttachNumber;

        ParentNoteViewHolder(View itemView) {
            super(itemView);
            cvNoteItemCard = itemView.findViewById(R.id.cv_item_note_card);
            cvNoteItemCard.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), NoteDetailActivity.class);
                intent.putExtra(StringConstants.EXTRA_NOTE, items.get(getAdapterPosition()));
                v.getContext().startActivity(intent);
            });
            tvNoteFrom = itemView.findViewById(R.id.tv_item_note_from);
            tvNoteTo = itemView.findViewById(R.id.tv_item_note_to);
            tvNoteDescription = itemView.findViewById(R.id.tv_item_parent_note_description);
            tvCreateDate = itemView.findViewById(R.id.tv_item_parent_note_description_create_date);
            tvAttachNumber = itemView.findViewById(R.id.tv_item_parent_note_description_attach_number);
        }

        @Override
        public void bind(Note element) {
            tvNoteFrom.setText(element.getAddedBy());
            tvNoteTo.setText(new StringBuilder().append(itemView.getContext().getResources().getString(R.string.to))
                    .append(" ").append(element.getNoteTo()));
            tvNoteDescription.setText(element.getNote());
            tvCreateDate.setText(DateTimeUtil.getDateTimeString(element.getAddedOn()));
            tvAttachNumber.setText(String.valueOf(element.getAttach().size()));
        }
    }
}