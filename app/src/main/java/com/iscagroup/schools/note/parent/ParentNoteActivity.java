package com.iscagroup.schools.note.parent;

import android.os.Bundle;

import com.iscagroup.schools.R;
import com.iscagroup.schools.base.BaseFragment;
import com.iscagroup.schools.note.BaseNoteActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raed Saeed on 7/23/2019.
 */
public class ParentNoteActivity extends BaseNoteActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideFab();
        getViewPager().setOffscreenPageLimit(2);
    }

    @Override
    protected List<BaseFragment> setFragments() {
        List<BaseFragment> fragments = new ArrayList<>();
        ParentPublicNoteFragment publicNoteFragment = new ParentPublicNoteFragment();
        publicNoteFragment.setName(getString(R.string.public_));
        fragments.add(publicNoteFragment);

        ParentClassNoteFragment classNoteFragment = new ParentClassNoteFragment();
        classNoteFragment.setName(getString(R.string.class_label));
        fragments.add(classNoteFragment);

        ParentPrivateNoteFragment privateNoteFragment = new ParentPrivateNoteFragment();
        privateNoteFragment.setName(getString(R.string.private_label));
        fragments.add(privateNoteFragment);
        return fragments;
    }
}
